import logging
import os
import sys

import tomllib_python311
from rslm._default_hw_config import HARDWARE_CONFIG_SECTION

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

sys.path.append(os.getcwd())
try:
    from rslm import (
        CmdExecutor,
        GenericCommand,
        GenericHardwareSimulator,
        LedControl,
        create_dummy_command_from_string,
        hw_response_simulator,
    )
except ImportError as ie:
    print("import error")
    raise ie

SIMULATE_ONLY: bool = True
SIM_CMD_STR: str = "you see, this string is executed"
CMD_STR: str = "help"


def main(executor: CmdExecutor):
    executor.start_processing_thread()
    ledControl = LedControl(cmd_executor=executor)
    if SIMULATE_ONLY:
        cmd_str: str = SIM_CMD_STR
    else:
        cmd_str: str = CMD_STR
    cmd: GenericCommand = create_dummy_command_from_string(cmd_str)
    ledControl.execute_pre_constructed_cmd(cmd)
    executor.stop_processing_thread()
    return


if __name__ == "__main__":
    executor = CmdExecutor(
        hw_interface=GenericHardwareSimulator(
            config_dict=tomllib_python311.load(open("./hw_config.toml", "rb"))[  # type: ignore
                HARDWARE_CONFIG_SECTION
            ]
        ) if SIMULATE_ONLY else None,
        hw_response_callable=hw_response_simulator if SIMULATE_ONLY else None,
        config_file="./hw_config.toml",
    )
    try:
        main(executor=executor)
        executor.stop_processing_thread()
    except KeyboardInterrupt:
        print("Received KeyboardInterrupt. Now shutting down.")
        executor.stop_processing_thread()
        sys.exit(-1)
    except Exception as e:
        executor.stop_processing_thread()
        sys.exit(-1)
        raise e
