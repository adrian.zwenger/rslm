import logging
import os
import sys
import time
from typing import Iterable, List, Type

import tomllib_python311
from rslm._default_hw_config import HARDWARE_CONFIG_SECTION

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

sys.path.append(os.getcwd())
try:
    from rslm import (
        CmdExecutor,
        CommandBuilder,
        GenericCommand,
        GenericHardwareSimulator,
        LedControl,
        hw_response_simulator,
    )
    from rslm.led_command_lib.fake_test_lib import FakeCommand
except ImportError as ie:
    print("import error")
    raise ie

SIMULATE_ONLY: bool = not True
SIMCLASS_A: Type[GenericCommand] = FakeCommand
SIMCLASS_A_params: Iterable = [12]
TOTAL_CMDS: int = 100


def main(executor: CmdExecutor):
    executor.start_processing_thread()

    ledControl = LedControl(cmd_executor=executor)

    if not SIMULATE_ONLY:
        cmd: List[GenericCommand] = [
            CommandBuilder.tec_status() for _ in range(TOTAL_CMDS)
        ]
    else:
        cmd: List[GenericCommand] = [
            SIMCLASS_A(*SIMCLASS_A_params) for _ in range(TOTAL_CMDS)
        ]

    # manual command execution by direct hardware access
    try:
        cmd_str: str = "info"
        LOGGER.info("Now executing '%s' as a string manually", cmd_str)
        return_value = executor.hw.execute(cmd_str)
        LOGGER.info("return value: %s", return_value)
    except Exception as e:
        executor.stop_processing_thread()
        raise e

    # regular command execution via the API
    a = time.perf_counter()
    ledControl.execute_pre_constructed_cmd(cmd)
    a = time.perf_counter() - a

    borked_counter = 0

    for _c in cmd:
        try:
            assert _c.was_executed(), "1"
            assert _c.execution_successful(), "2"
            assert _c.get_cmd_result_as_dict(), "3"
            assert _c.get_cmd_result_as_str(), "4"
        except AssertionError as e:
            print(e)
            borked_counter += 1
        except Exception as e:
            executor.stop_processing_thread()
            raise e

    executor.stop_processing_thread()

    print(f"\n\n\nReceived Results after {a} seconds.")
    if SIMULATE_ONLY:
        print(
            f"Time per command: {a / TOTAL_CMDS}s / {200 / 115200}s (avg. / upper speed limit)"
        )
    else:
        print(f"Time per command: {a / TOTAL_CMDS}s")
    print(f"{TOTAL_CMDS - borked_counter}/{TOTAL_CMDS} successful")
    return


if __name__ == "__main__":
    executor = CmdExecutor(
        hw_interface=GenericHardwareSimulator(
            config_dict=tomllib_python311.load(open("./hw_config.toml", "rb"))[  # type: ignore
                HARDWARE_CONFIG_SECTION
            ]
        ) if SIMULATE_ONLY else None,
        hw_response_callable=hw_response_simulator if SIMULATE_ONLY else None,
        config_file="./hw_config.toml",
    )
    try:
        main(executor=executor)
        executor.stop_processing_thread()
    except KeyboardInterrupt:
        print("Received KeyboardInterrupt. Now shutting down.")
        executor.stop_processing_thread()
        sys.exit(-1)
    except Exception as e:
        executor.stop_processing_thread()
        sys.exit(-1)
        raise e
