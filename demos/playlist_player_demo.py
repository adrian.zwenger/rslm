
import logging
import os
import sys
from time import sleep
from typing import Iterable, List, Type

import tomllib_python311
from rslm._default_hw_config import HARDWARE_CONFIG_SECTION

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.basicConfig(level=logging.INFO)


sys.path.append(os.getcwd())
try:
    from rslm import (
        CmdExecutor,
        CommandBuilder,
        CommandPlayer,
        GenericCommand,
        GenericHardwareSimulator,
        hw_response_simulator,
    )
    from rslm.led_command_lib.fake_test_lib import FakeCommand
except ImportError as ie:
    print("import error")
    raise ie

SIMULATE_ONLY: bool = False
SIMCLASS_A: Type[GenericCommand] = FakeCommand
SIMCLASS_A_params: Iterable = [12]
TOTAL_CMDS: int = 10
CMD_DELAY: int = 1


def get_playlist():
    if not SIMULATE_ONLY:
        cmd: List[GenericCommand] = [
            CommandBuilder.info(delay_seconds=CMD_DELAY) for i in range(TOTAL_CMDS)
        ]
    else:

        def _create_cmd() -> GenericCommand:
            cmd: GenericCommand = SIMCLASS_A(*SIMCLASS_A_params)
            cmd.delay_seconds = CMD_DELAY
            return cmd

        cmd: List[GenericCommand] = [_create_cmd() for _ in range(TOTAL_CMDS)]

    return cmd


def main(executor: CmdExecutor):
    playlist: List[GenericCommand] = get_playlist()
    player: CommandPlayer = CommandPlayer(
        cmds=playlist, executor=executor, repeat=False, retries=2
    )
    executor.start_processing_thread()
    player.play()
    # it is important to note, that the player does not keep the thread alive.
    while player.is_playing():
        sleep(1)
    return


if __name__ == "__main__":
    executor = CmdExecutor(
        hw_interface=GenericHardwareSimulator(
            config_dict=tomllib_python311.load(open("./hw_config.toml", "rb"))[  # type: ignore
                HARDWARE_CONFIG_SECTION
            ]
        ) if SIMULATE_ONLY else None,
        hw_response_callable=hw_response_simulator if SIMULATE_ONLY else None,
        config_file="./hw_config.toml",
    )
    try:
        main(executor=executor)
        executor.stop_processing_thread()
    except KeyboardInterrupt:
        print("Received KeyboardInterrupt. Now shutting down.")
        executor.stop_processing_thread()
        sys.exit(-1)
