import logging
import os
import sys

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.basicConfig(level=logging.INFO)

sys.path.append(os.getcwd())
try:
    from rslm import start_gui_as_standalone
except ImportError as ie:
    print("import error")
    raise ie

if __name__ == "__main__":
    start_gui_as_standalone()
