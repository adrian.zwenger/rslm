# **RSLM** - **R**efrigerator **S**ized **L**ED **M**anager

The RSLM library provides the user with various possibilities to communicate with and control the accompanying lighting equipment through its `rslm` module.

RSLM enables you to create [`generic commands`](https://git.rwth-aachen.de/adrian.zwenger/rslm/-/blob/main/rslm/led_command_lib/_generic_command.py) which are then executed on the lighting equipment. A command execution consists of translating the users input into a mashine readable format, sending this input to the lighting equipment via a serial connection and then processing the hardware response. This process is taken care for you. You, as a user of RSLM, just have to create commands using the included utilities and and pass them to RSLM. Creating nested lists of commands to be executed in sequence is intended as well as supported.

RSLM includes a command-line utility that allows you to run the RSLM program in different modes and configurations. This utility provides options for setting the logging level, specifying the hardware configuration file, choosing between "webapp" and "native" modes, enabling hardware simulation, creating a default configuration file, and specifying the port when running in webapp mode.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Options](#options)
- [Examples](#examples)
- [Library Usage](#library-usage)
- [Todo](#todo)


## Installation

1. Install Python 3.8 (or newer) and add it to PATH. To check which Python version is installed execute the following in a command-line prompt:
    ```bash
    python --version
    ```

1. Clone the RSLM repository to your current working directory:

   ```bash
   git clone https://git.rwth-aachen.de/adrian.zwenger/rslm
   cd rslm
   ```

1. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

If on Linux and the following error occurs:
```sh
   flet-0.21.2/flet/flet: error while loading shared libraries: libmpv.so.1: cannot open shared object file: No such file or directory
```
make sure that libmpv is installed and symlink the executable:
```sh
   sudo apt install libmpv2 libmpv-dev
   sudo ln -s /usr/lib/x86_64-linux-gnu/libmpv.so.2 /usr/lib/x86_64-linux-gnu/libmpv.so.1
```

## Usage

To use the RSLM Command-Line Utility, run the `rslm` module with the desired command-line options.

```bash
python rslm [options]
```

## Options

The following command-line options are available for the RSLM Command-Line Utility:

- `--debug`: Set the logging level. Choose from `"INFO"`, `"DEBUG"`, `"WARNING"`, `"ERROR"`, or `"CRITICAL"`. Default is `"INFO"`.

- `--configfile`: Specify the hardware configuration file to use or create. Default is `"./config.toml"`.

- `--mode`: Specify the mode in which to run RSLM. Options are `"webapp"` or `"native"`.

- `--simulation`: If this flag is set, RSLM simulates the hardware and does not use it.

- `--createconfig`: If this flag is set, a default configuration file is created in the current working directory. If `--configfile` is not provided, the default file `"./config.toml"` will be created or overwritten.

- `--port`: Specify the port to be used if RSLM is running in webapp mode. Default is `8081`.

## Examples

1. Run RSLM in "webapp" mode with debugging enabled:

   ```bash
   python rslm --mode webapp --debug DEBUG
   ```

2. Run RSLM in "native" mode with hardware simulation:

   ```bash
   python rslm --mode native --simulation
   ```

3. Create a default configuration file in the current working directory:

   ```bash
   python rslm --createconfig
   ```

4. Specify a custom configuration file and port:

   ```bash
   python rslm --configfile custom_config.toml --port 8000
   ```

5. Start RSLM as a webapp on port 8088 using a custom configuration file with hardware simulation.
    ```bash
    python rslm --mode webapp --port 8088 --simulation --configfile ./hw_config.toml
    ```

## Library Usage
The `rlsm` module provides Python programmers a programmatic interface to the lighting hardware. For usage examples please take a look at the available `demos``.

Available demos:
| File | Description | |
| --- | --- | --- |
| [gui_demo.py](https://git.rwth-aachen.de/adrian.zwenger/rslm/-/blob/main/demos/gui_demo.py) | Starts RSLM as a native application | ```python ./demos/gui_demo.py``` |
| [webserver_demo.py](https://git.rwth-aachen.de/adrian.zwenger/rslm/-/blob/main/demos/webserver_demo.py) | Starts RSLM as a webapp | ```python ./demos/webserver_demo.py``` |
| [manual_playlist_creation.ipynb](https://git.rwth-aachen.de/adrian.zwenger/rslm/-/blob/main/demos/manual_playlist_creation.ipynb) | Shows how to create commands | Intended to be used with Jupyter. |
| [script_demo.py](https://git.rwth-aachen.de/adrian.zwenger/rslm/-/blob/main/demos/script_demo.py) | Demonstrates `rslm` usage as a Python API without a GUI. | ```python ./demos/script_demo.py``` |

## Todo

1) Add a metrics application to enable hardware status monitoring
1) complete REST API to enable creation of custom frontends
1) complete inline documentation