"""
    Source code of tomllib Cpython 311 is used here for backwards.
    compatibility with older python 3 versions which did not implement tomllib.
    renaming to tomllib_python311 to prevent namespace oveerride issues.

    https://github.com/python/cpython/tree/46cae02085311481dc8b1ea9a5110969d9325bc7/Lib/tomllib
"""
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Taneli Hukkinen
# Licensed to PSF under a Contributor Agreement.

__all__ = ("loads", "load", "TOMLDecodeError")

from ._parser import TOMLDecodeError, load, loads

# Pretend this exception was created here.
TOMLDecodeError.__module__ = __name__
