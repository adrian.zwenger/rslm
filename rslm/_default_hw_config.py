HARDWARE_CONFIG_SECTION: str = "HARDWARE_INFO"
"""this constant is the section name in which the hardware configuration is stored
take a look at the default toml file for more information
"""

API_CONFIG_SECTION_ID: str = "API"
API_CONFIG_VALID_VALUES_SECTION_ID: str = "valid_values"
API_CONFIG_PARAM_TYPE_ID: str = "type"
API_CONFIG_CUSTOM_COMMANDS_SECTION_ID: str = "custom_commands"

API_CONFIG_PARAM_TYPE_STRING_ID: str = "string"
API_CONFIG_PARAM_TYPE_INT_ID: str = "integer"
API_CONFIG_PARAM_TYPE_FLOAT_ID: str = "float"
API_CONFIG_PARAM_TYPE_BOOL_ID: str = "bool"


DEFAULT_HW_CONFIG_TOML: str = r"""# author: Adrian Zwenger
# organisation: TU Darmstadt, ALSV
# minicom --device /dev/ttyACM0 --baudrate 115200
# This file serves as a hardware configuration sample.
# When editing please adhere to the TOML specification
#   https://toml.io/en/
#
#
# a quick TOML to Python type equivalences:
# TOML:       | Example:                        | Python:
# string      | "a string"                      | str
# integer     | 314                             | int
# float       | 3.14                            | float
# boolean     | true, false                     | bool
# array       | ["elements", "in here", 3.13]   | list
#
#
# The following sections need to be present
#     - HARDWARE_INFO
#         contains hardware options
#     - API
#         contains API configuration flags
#         * COMMON
#             configuration flags for all "common" commands
#         * LED
#             configuration flags for all "led" commands
#         * POWER
#             configuration flags for all "power" commands
#         * TEC
#             configuration flags for all "tec" commands
#
#
# if a command needs non standard configuration flag definitions, add the command name to the
# "custom_commands" array of the corresponding api section. e.g.:
#
#     led command <flag1> <flag2>
#     where flag1 & flag2 share the same name as other commands but require different values
#
#     add command to custom_commands array to indicate need for custom definitions.
#     It is important that you add definitions for !ALL! command options!!!!!!
#     add a subsection in which the configurations are stored.
#
#
#     '''
#         [API]
#         ...
#         custom_commands = ["led command"]
#         ...
#             [API."led command"]
#                 [API."led command".flag1]
#                 type = int
#                 valid_values = [0, 100]
#
#                 [API."led command".flag2]
#                 type = str
#                 valid_values = ["mode 1", "mode 2", "mode 3"]
#     '''


[HARDWARE_INFO]
    # This sections contains hardware descriptions
    # Please DO NOT EDIT if you do not know what you are doing
    port = "COM3"              # "/dev/ttyACM0"      # the USB port which is connected with the device
    baudrate = 115200          # integer baudrate of the serial device
    bytesize = 8               # byte size of messages
    parity = "N"               # 'N', 'E', 'O', 'M', or, 'S'
    stopbits = 1               # 1, 1.5, or, 2
    read_timeout = 1.0         # max time to wait on read operations in seconds
    write_timeout = 1.0        # max time to wait on write operations in seconds
    max_serial_buffer_size = 8 # maximum message size in bytes which can be sent to serial device
    line_terminator = "\r\n"   # character used to encode a new line
    message_terminator = "# "  # characters used to encode the end of mesdsages sent by serial device
    encoding = "utf-8"         # encoding of messages
    tec_driver_boards = 5      # number of TEC driver boards, please make sure to set the proper board number below as well  # noqa: E501

[API]
    # This section contains configuration flags for the API
    # Please do not touch if you do not know what you are doing.
    # For a description of each flag please read the API documentation.
    api_version = "0.01" # API revision


# all flags need to be fully described as follows:
#     [FLAG_NAME]
#     flag_type = <"string" | "integer" | "float" | "bool"> # mandatory
#     vaild_values = [...] # optional. if not provided values are not checked and directly passed to the hardware.
#
#     supported types: "string", "integer", "float", "bool"
#     to add valid string options use TOML arrays:
#        e.g.: ["valid option A", "valid option B"]
#     to add minimum/maximum value checking for numbers add an array with [MIN, MAX]
#        e.g.: [0, 100]
#
#     example "bus-id" flag which supports values between 0 and 100:
#         [bus-id]
#             type = "integer"
#             vaild_values = [0, 100]
#
    [API.COMMON]
        custom_commands = []

        [API.COMMON.enter_gamemode]
            type = "bool"

    [API.LED]
        custom_commands = []

        [API.LED.board]
            # describes the connector label of a connected LED
            # {1 --> J3, 2 --> J4, 3 --> J5, 4 --> J6}
            type = "integer"
            valid_values = [1, 4]

        [API.LED.bus]
            type = "integer"
            vaild_values = [1, 2]

        [API.LED.addr]
            # the I2C address to communicate with
            type = "string"

        [API.LED.channel]
            # the id of a LED channel to address
            type = "integer"
            valid_values = [1, 54]

        [API.LED.duty]
            # pwm duty cycle to use
            type = "integer"
            valid_values = [0, 1000]

        [API.LED.freq]
            # pwm duty cycle to use
            type = "integer"
            valid_values = [100, 20000]

        [API.LED.ch]
            # LED channel as known by LED driver
            type = "string"
            valid_values = ["ch0", "ch1"]

        [API.LED.current]
            # current in mA to be used by LED drivers
            type = "integer"
            valid_values = [0, 1000]

        [API.LED.value]
            type = "string"

    [API.POWER]
        custom_commands = []

        [API.POWER.switch]
            type = "integer"
            valid_values = [1, 3]

        [API.POWER.board]
            # describes the connector label of a connected LED
            # {1 --> J3, 2 --> J4, 3 --> J5, 4 --> J6}
            type = "integer"
            valid_values = [1, 4]

        [API.POWER.ch]
            # LED channel as known by LED driver
            type = "string"
            valid_values = ["1", "2"]

        [API.POWER.temp]
            type = "integer"

    [API.TEC]
        custom_commands = []

        [API.TEC.board]
            # describes the connector label of a connected LED
            # {1 --> J3, 2 --> J4, 3 --> J5, 4 --> J6}
            type = "integer"
            valid_values = [1, 3]

        [API.TEC.ch]
            # LED channel as known by LED driver
            type = "string"
            valid_values = ["1", "2"]

        [API.TEC.temp]
            # LED channel as known by LED driver
            type = "integer"

    [API.FAKE]
        # a library with fake commands for testing purposes
        custom_commands = ["fake custom_command"]

        [API.FAKE.string]
            type = "string"
            # valid_values = ["a", "b", "c"]

        [API.FAKE.integer]
            type = "integer"
            valid_values = [0, 100]

        [API.FAKE.float_val]
            type = "float"
            valid_values = [0.0, 100.0]

        [API.FAKE.bool_val]
            type = "bool"

        [API.FAKE."fake custom_command"]
            [API.FAKE."fake custom_command".string]
                type = "string"
                valid_values = ["d", "e", "f", "g"]

            [API.FAKE."fake custom_command".integer]
                type = "integer"
                valid_values = [100, 1023]
"""
