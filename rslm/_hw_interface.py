import abc as _abc
import logging as _logging
import random as _random
import time
import typing as _tp
from time import sleep as _sleep

import serial as _serial

from rslm._sigint_handling import SIGINT_RECEIVED
from rslm.led_command_lib import GenericCommand as _GenericCommand

_logging.getLogger(__name__).addHandler(_logging.NullHandler())
_LOGGER = _logging.getLogger(__name__)
_LOGGER_INFO_ENABLED: bool = _LOGGER.isEnabledFor(_logging.INFO)


class CustomHandler:
    def __init__(self):
        self.__handle_callable: _tp.Optional[
            _tp.Callable[..., _tp.Awaitable[_tp.Any]]
        ] = None

    def log(self, record: str):
        if self.__handle_callable is not None:  # and self.__flet_page:
            self.__handle_callable(record)

    def reset_handler(self):
        self.__handle_callable = None

    def set_handler(self, hcallable: _tp.Callable) -> None:
        self.__handle_callable = hcallable


COMMS_LOGGER: CustomHandler = CustomHandler()


class BaseHardwareSimulatorClass(_abc.ABC):
    def __init__(
        self,
        config_dict: _tp.Dict[str, _tp.Any],
    ):
        self.read_timeout: int = config_dict["read_timeout"]
        self.write_timeout: int = config_dict["write_timeout"]
        self.communications_logger: CustomHandler = COMMS_LOGGER

    @_abc.abstractmethod
    def close_port(self):
        pass

    @_abc.abstractmethod
    def execute(
        self,
        cmd_str: _tp.Union[str, _tp.List[str]],
    ) -> _tp.Optional[_tp.Union[str, _tp.List[_tp.Optional[str]]]]:
        pass

    def set_log_handler_callable(
        self, func: _tp.Callable[..., _tp.Awaitable[_tp.Any]]
    ) -> None:
        self.communications_logger.set_handler(func)

    def reset_log_handler_callable(self) -> None:
        self.communications_logger.reset_handler()


class GenericHardwareSimulator(BaseHardwareSimulatorClass):
    def close_port(self):
        print("Simulate closing port")

    def execute(
        self,
        cmd_str: _tp.Union[str, _tp.List[str]],
    ) -> _tp.Optional[_tp.Union[str, _tp.List[_tp.Optional[str]]]]:

        r: str = "".join(_random.sample(str(cmd_str), len(str(cmd_str))))
        self.communications_logger.log(f"{time.ctime()} >> " + str(cmd_str))
        self.communications_logger.log(r)
        return r


class HWInterface(BaseHardwareSimulatorClass):
    """
    This class is a serial wrapper based on the PySerial library.
    It is used in the project to send commands to the hardware and
    retrieve its responses.
    """

    def __init__(
        self,
        config_dict: _tp.Dict[str, _tp.Any],
        # a dictionary containing the configuration
        # just pass the HARDWARE_CONFIG_SECTION section from the toml config file as a dict
    ) -> None:
        super().__init__(config_dict=config_dict)
        # the serial port as a string. in linux '/dev/tty*' and in windows 'COM*'
        self.port_url: str = config_dict["port"]
        # the _serial.Serial instance
        self.port: _serial.Serial = _serial.Serial(
            port=config_dict["port"],
            baudrate=config_dict["baudrate"],
            bytesize=config_dict["bytesize"],
            parity=config_dict["parity"],
            stopbits=config_dict["stopbits"],
            timeout=self.read_timeout,
            write_timeout=self.write_timeout,
        )  # port is opened by object creation
        # message encoding. most of the times its 'utf-8'
        self.encoding: str = config_dict["encoding"]
        # line feed character used by the device to terminate lines.
        self.LF: str = config_dict["line_terminator"]  # most likely '\r\n'
        # linefeed character length in bytes
        self.LF_len: int = len(bytes(self.LF, self.encoding))
        # termination character used by the hardware to indicate the end of a message
        # in engineering sample '# ' was used
        self.msg_terminator: str = config_dict["message_terminator"]
        # input buffer size of hardware in bytes
        # engineering sample had a buffer size of 8
        self.max_serial_buffer_size: int = config_dict["max_serial_buffer_size"]
        # close port. port is re-opened when used
        # self.port.close()

    def discard_in_waiting_bytes(self):
        self.port.reset_input_buffer()
        self.port.reset_output_buffer()
        while self.port.in_waiting > 0:
            self.port.read(self.port.in_waiting)
            _sleep(0.1)
        self.port.reset_input_buffer()
        self.port.reset_output_buffer()

    def execute(
        self,
        cmd_str: _tp.Union[str, _tp.List[str]],
    ) -> _tp.Optional[_tp.Union[str, _tp.List[_tp.Optional[str]]]]:
        """
        writes the passed string to the serial port and returns the received message as a string
        throws Exceptions if something goes wrong.
        Error handling needs to be performed by the caller.
        """

        def __execute_single_str(cmd: str) -> _tp.Optional[str]:
            self.discard_in_waiting_bytes()
            self.discard_in_waiting_bytes()
            chunked_cmd: _tp.List[bytes] = [
                bytes(cmd[_i : _i + 8], self.encoding)
                for _i in range(0, len(cmd), self.max_serial_buffer_size)
            ]
            cmd_echo: str = ""
            for chunk in chunked_cmd:
                _actual_l: int = len(chunk)
                _l: _tp.Optional[int] = self.port.write(chunk)
                assert (
                    _l is not None
                ), "Expected number of written bytes. Received None."
                assert _l == len(
                    chunk
                ), f"Message was {_actual_l} bytes long, but {_l} were written."
                try:
                    cmd_echo += self.port.read(_l).decode(self.encoding)
                except UnicodeDecodeError:
                    _LOGGER.critical("Failed to decode message.")
                    self.discard_in_waiting_bytes()
                    self.close_port()
                    return None
            self.communications_logger.log(f"{time.ctime()} >>" + cmd_echo)
            if cmd != cmd_echo:
                _LOGGER.critical(
                    """
        Unknown error. Sent message and message confirmation are not identical.
        Sent Message: %s
        Expected Message: %s
        Received Message: '%s'""",
                    cmd,
                    cmd,
                    cmd_echo,
                )
                self.discard_in_waiting_bytes()
                self.close_port()
                return None

            self.port.write(bytes(self.LF, self.encoding))  # execute command
            self.port.read(self.LF_len)
            answer: str = self.port.read_until(
                bytes(self.msg_terminator, self.encoding)
            ).decode(self.encoding)
            _LOGGER.info(
                """
        Executed Command: '%s'
        Hardware returned: '%s'""",
                str(cmd),
                answer
            )
            self.communications_logger.log(answer)
            return answer

        def __execute_list_of_str(cmds: _tp.List[str]) -> _tp.List[_tp.Optional[str]]:
            _return: _tp.List[_tp.Optional[str]] = []
            for _cmd in cmds:
                _return.append(__execute_single_str(_cmd))
            # return list(map(__execute_single_str, cmds))
            return _return

        try:
            if not self.port.is_open:
                _LOGGER.error(
                    "Serial port was closed. Re-opening and flushing buffers. Est. dur.: 1s."
                )
                self.port.open()
                self.port.write(bytes(self.LF, self.encoding))
                self.port.write(bytes(self.LF, self.encoding))
                self.discard_in_waiting_bytes()
                self.discard_in_waiting_bytes()

            assert isinstance(cmd_str, str) or isinstance(cmd_str, list)
            if isinstance(cmd_str, str):
                return __execute_single_str(cmd_str)
            if isinstance(cmd_str, list):
                for _ in cmd_str:
                    assert isinstance(_, str)
                return __execute_list_of_str(cmd_str)
            raise TypeError("Expected str or List[str], got: %s" % type(cmd_str))

        except _serial.SerialTimeoutException as ser_timeout:
            _LOGGER.critical("Serial Timeout. Consider re-connecting serial device.")
            _LOGGER.exception(ser_timeout)
            self.port.close()
        except _serial.SerialException as ser_except:
            _LOGGER.critical(
                "Serial Error. This should not happen. Please research reason."
            )
            _LOGGER.exception(ser_except)
            self.port.close()
        except Exception as _e:
            _LOGGER.critical("Unexpected exception.")
            _LOGGER.exception(_e)
            raise _e

    def close_port(self) -> None:
        if self.port.is_open:
            self.port.close()


def hw_response_simulator(hw: HWInterface, cmd_str: _GenericCommand) -> None:
    try:
        if cmd_str.timed_out.is_set():
            return  # cmd_str
        _: str = cmd_str.get_executable_string()
        return_val: str = str(hw.execute(cmd_str=_))
        cmd_str.set_cmd_result(
            return_val, ignore_result_check=True
        )  # random string response and do not check result as we are simulating
        if "tec status" in _:
            _LOGGER.critical("Simulating tec status. Going to sleep for 10 s.")
            _sleep(10)
        else:
            _sleep(0.2)  # simulate hw-delay
        cmd_str.set_cmd_as_executed()
        if _LOGGER_INFO_ENABLED:
            _LOGGER.info(
                "finished simulating : %s",
                str(cmd_str),
            )
        return
    except KeyboardInterrupt as _k:
        raise _k
    except Exception as e:
        raise e


def execute_command(
    hw: HWInterface,
    cmd: _tp.Union[_GenericCommand, _tp.List[_GenericCommand]],
    retries: int = 2,
) -> None:
    """
    reference wrapper function implementation
    for sending commands to the serial device
    and storing the received answers
    """

    def __execute_single_command(
        _hw: HWInterface, _cmd: _GenericCommand, _retries: int
    ) -> bool:
        for retry_num in range(_retries):
            try:
                result: _tp.Optional[_tp.Union[str, _tp.List[_tp.Optional[str]]]] = (
                    _hw.execute(_cmd.get_executable_string())
                )
                _cmd.set_cmd_as_executed()
                if result and isinstance(result, str):
                    _cmd.set_cmd_result(result)
                    return True
                return False
            except KeyboardInterrupt as _k:
                if _LOGGER_INFO_ENABLED:
                    _LOGGER.info("Encountered KeyboardInterrupt.")
                raise _k
            except AssertionError as e:
                _LOGGER.critical("Encountered AssertionError: %s", str(e))
                if retry_num < retries:
                    continue
                raise e
            except Exception as e:
                _LOGGER.exception("Encountered unexpected Exception: %s", type(e))
                raise e
        return False

    def __execute_multiple_command(
        _hw: HWInterface, _cmds: _tp.List[_GenericCommand], _retries: int
    ) -> None:
        for cmd in _cmds:
            if SIGINT_RECEIVED.is_set():
                return
            success: bool = __execute_single_command(_hw, cmd, _retries)
            if not success:
                break

    if isinstance(cmd, _GenericCommand):
        __execute_single_command(hw, cmd, retries)
    elif isinstance(cmd, list):
        for _ in cmd:
            assert isinstance(_, _GenericCommand)
        __execute_multiple_command(hw, cmd, retries)
    else:
        raise AssertionError(f"Input is not of type GenericCommand. It is {type(cmd)}")
