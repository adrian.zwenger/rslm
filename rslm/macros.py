import typing as _tp

import rslm.led_command_lib._serialization as _ser
import rslm.led_command_lib.common as _common
import rslm.led_command_lib.led as _led
import rslm.led_command_lib.tec as _tec


# TODO: think of generic handler for this
class _MacroBaseClass(_ser.ConsecutiveCommands):
    def __init__(self) -> None:
        super().__init__()


class SetAllLedPWM(_MacroBaseClass):
    def __init__(self, duty: int = 0) -> None:
        super().__init__()
        self.description = "Sets the duty cycle for all channels to the given value."
        self.short_name = "Set PWM duty cycle"

        self.duty: int = duty
        self.construct()

    def construct(self) -> None:
        super().construct()
        cmd: _led.LedPwm = _led.LedPwm()

        bounds: _tp.List[int] = cmd.get_attr_specs("channel")["valid_values"]

        for i in range(min(bounds), max(bounds) + 1):
            _cmd: _led.LedPwm = cmd.__class__()
            _cmd.channel = i
            _cmd.duty = self.duty
            _cmd.delay_seconds = 0
            self.append(_cmd)


class InitTec(_MacroBaseClass):
    def __init__(self, target_temp: int = 45) -> None:
        super().__init__()
        self.description = "This macro initializes the hardware by setting the target temperaturte for all TEC devices to the passed value (default is 45 Celsius)."  # noqa: E501
        self.short_name = "Initialise TEC devices"

        self.target_temp: int = target_temp
        self.construct()

    def construct(self) -> None:
        super().construct()
        cmd: _tec.TecTemp = _tec.TecTemp()
        board_bounds: _tp.List[int] = cmd.get_attr_specs("board")["valid_values"]
        channels: _tp.List[str] = cmd.get_attr_specs("ch")["valid_values"]

        for board_id in range(min(board_bounds), max(board_bounds) + 1):
            for ch_id in channels:
                _cmd: _tec.TecTemp = cmd.__class__()  # type: ignore
                _cmd.ch = ch_id
                _cmd.board = board_id
                _cmd.delay_seconds = 0
                _cmd.temp = self.target_temp
                self.append(_cmd)

        enter_gmode: _common.GameMode = _common.GameMode()
        enter_gmode.enter_gamemode = True

        exit_gmode: _common.GameMode = _common.GameMode()
        exit_gmode.enter_gamemode = False

        self.append(enter_gmode)

        for board_id in range(min(board_bounds), max(board_bounds) + 1):
            _cmd: _tec.TecStart = _tec.TecStart()
            _cmd.board = board_id
            self.append(_cmd)

        self.append(exit_gmode)


class InitLeds(_MacroBaseClass):
    def __init__(
        self,
        target_tec_temp: int = 45,
        default_current: int = 1000,
        default_freq: int = 2000,
        default_duty_cycle: int = 0,
        preconstruct: bool = False,
    ) -> None:
        super().__init__()
        self.description = "This macro initializes the LEDs by initialising the TEC controllers, starting them and turning all LED outputs off."  # noqa: E501
        self.short_name = "Initialise all devices"

        self.target_tec_temp: int = target_tec_temp
        self.default_current: int = default_current
        self.default_freq: int = default_freq
        self.default_duty_cycle: int = default_duty_cycle

        if preconstruct:
            self.construct()

    def construct(self) -> None:
        super().construct()
        # set current
        current_cmds: _tp.List[_led.LedCurrent] = []
        freq_cmds: _tp.List[_led.LedFreq] = []
        duty_cmds: _tp.List[_led.LedPwm] = []

        current_cmd: _led.LedCurrent = _led.LedCurrent()
        freq_cmd: _led.LedFreq = _led.LedFreq()
        duty_cmd: _led.LedPwm = _led.LedPwm()

        channel_bounds: _tp.List[int] = current_cmd.get_attr_specs("channel")[
            "valid_values"
        ]

        for channel_id in range(min(channel_bounds), max(channel_bounds) + 1):
            _current_cmd: _led.LedCurrent = current_cmd.__class__()
            _current_cmd.channel = channel_id
            _current_cmd.current = self.default_current
            _current_cmd.delay_seconds = 0
            current_cmds.append(_current_cmd)

            _freq_cmd: _led.LedFreq = freq_cmd.__class__()
            _freq_cmd.channel = channel_id
            _freq_cmd.freq = self.default_freq
            _freq_cmd.delay_seconds = 0
            freq_cmds.append(_freq_cmd)

            _duty_cmd: _led.LedPwm = duty_cmd.__class__()
            _duty_cmd.channel = channel_id
            _duty_cmd.duty = self.default_duty_cycle
            _duty_cmd.delay_seconds = 0
            duty_cmds.append(_duty_cmd)

        self.append(InitTec(target_temp=self.target_tec_temp))

        self.extend(duty_cmds)
        self.extend(current_cmds)
        self.extend(freq_cmds)
