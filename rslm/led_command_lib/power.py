"""
    functional power command classes are defined here
    Make sure that all definitions, which are not a command class, start with an undersco_re.
    Name definitions with an underscore will be filtered out. This way using the dir()-function all
    relevant classes can be attained programmatically.
"""
import logging as _logging
import re as _re
import typing as _tp

from rslm.led_command_lib import GenericCommand as _GenericCommand

_logging.getLogger(__name__).addHandler(_logging.NullHandler())

_CONFIG_SECTION_ID: str = "POWER"


class _BasePowerCommand(_GenericCommand):
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: _tp.List[str] = [],
        needs_gamemode: bool = False,
    ):
        super().__init__(
            base_command=base_command,
            ordered_param_name_list=ordered_param_name_list,
            needs_gamemode=needs_gamemode,
            advanced_usage=True,
        )
        self._config_section_name = _CONFIG_SECTION_ID


class PowerStatus(_BasePowerCommand):
    """
    this class wraps the 'power status' command.

    results are stored in a structured dictionary:
    ```
    {
        "msg": str,  # unparsed return value from hardware
        "PSU X": str,  # status of PSU X. this entry may appear multiple times
        "Board X": {  # contains status of board X. may appear multiple times
            "V_LED X": str  # status of V_LED X. may appear multiple times
        }
    }
    ```
    """

    def __init__(self):
        super().__init__("power status", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)

        # check board activity
        matches: _tp.List[_tp.Any] = _re.findall(
            pattern=r"PSU (?P<psu>.):\s+(?P<status>\w+)", string=msg
        )
        assert matches
        for match in matches:
            self._hw_return_val_dict[f"PSU {match[0]}"] = match[1]

        # check individual boards
        matches: _tp.List[_tp.Any] = _re.split(
            pattern=r"Board (?P<board>\d+):", string=msg
        )[1:]

        try:
            matches = [(matches[i], matches[i + 1]) for i in range(0, len(matches), 2)]
        except IndexError:
            # handle unexpected formatting
            raise AssertionError("Unable to parse HW-response")

        assert matches
        for match in matches:
            self._hw_return_val_dict[f"Board {match[0]}"] = {}

            # power
            _match: _tp.Optional[_re.Match] = _re.search(
                pattern=r"Power:\s(?P<power>\w+)", string=match[1]
            )
            assert _match
            assert _match.group("power")
            self._hw_return_val_dict[f"Board {match[0]}"]["power"] = _match.group(
                "power"
            )

            # vleds status
            _matches: _tp.List[_tp.Any] = _re.findall(
                pattern=r"V_LED (?P<vLedId>\w): (?P<status>\w+)", string=match[1]
            )
            for _ in _matches:
                self._hw_return_val_dict[f"Board {match[0]}"][f"V_LED {_[0]}"] = _[1]

        self._execution_successful_flag.set()


class PowerHssOff(_BasePowerCommand):
    def __init__(self):
        super().__init__("power hss off", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        if "ok" not in msg:
            self._execution_successful_flag.clear()


class PowerHss(_BasePowerCommand):
    def __init__(self):
        super().__init__("power hss", ["board", "switch"], True)
        self.board: _tp.Optional[int] = None
        self.switch: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        self._execution_successful_flag.set()
        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "ok" not in _msg:
            self._execution_successful_flag.clear()
            return

        if "wrongboardorswitch" in _msg or "wrongargument" in _msg:
            self._execution_successful_flag.clear()
            return

        raise AssertionError(f"Unexpected hardware return value: '{msg}'")
