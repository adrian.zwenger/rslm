"""
    functional led command classes are defined here.
    Make sure that all definitions, which are not a command class, start with an underscore.
    Name definitions with an underscore will be filtered out. This way using the dir()-function all
    relevant classes can be attained programmatically.
"""
import logging as _logging
import re as _re
import typing as _tp

from rslm.led_command_lib import GenericCommand as _GenericCommand

_logging.getLogger(__name__).addHandler(_logging.NullHandler())

_CONFIG_SECTION_ID: str = "LED"


class _BaseLedCommand(_GenericCommand):
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: _tp.List[str] = [],
        needs_gamemode: bool = False,
    ):
        super().__init__(
            base_command=base_command,
            ordered_param_name_list=ordered_param_name_list,
            needs_gamemode=needs_gamemode,
            advanced_usage=False,
        )
        self._config_section_name = _CONFIG_SECTION_ID


class LedVersion(_BaseLedCommand):
    """
    This class wraps the 'led version' command, which returns the LED softwarfe version.

    The output is stored asa structured dict.

    ```
    {
        "major": str,  # major software version
        "minor": str,  # minor software version
        "patch": str,  # patch software version
    }
    ```
    """

    def __init__(self):
        super().__init__("led version", ["bus", "addr"])
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)

        if "invalid bus or address" in msg:
            self._execution_successful_flag.clear()
            return

        if "could not get version" in msg:
            self._execution_successful_flag.clear()
            return

        match: _tp.Optional[_re.Match] = _re.search(
            pattern=r"version:\s+(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)",
            string=msg,
        )
        assert match

        major: str = match.group("major")
        assert major, "Unable to parse major LED-software version."
        self._hw_return_val_dict["major"] = major

        minor: str = match.group("minor")
        assert minor, "Unable to parse minor LED-software version."
        self._hw_return_val_dict["minor"] = minor

        patch: str = match.group("patch")
        assert patch, "Unable to parse patch LED-software version."
        self._hw_return_val_dict["patch"] = patch
        self._execution_successful_flag.set()


class LedConfig(_BaseLedCommand):
    """
    this class wraps the 'led conig' command which returns the configuration of a given LED.

    the ouput is stored as a structured dict:
    ```
    {
        "msg": str,  # the string returned by the hardware
        "config": str  # a string describing the configuration
    }
    ```
    """

    def __init__(self):
        super().__init__("led config", ["bus", "addr"])
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)

        _msg: str = msg.replace("\n", "").replace("\n", "").replace(" ", "")

        if "invalidbusoraddress" in _msg:
            self._execution_successful_flag.clear()
            self._hw_return_val_dict["config"] = "invalid bus or address"
            return
        elif "couldnotgetversion" in _msg:
            self._execution_successful_flag.clear()
            self._hw_return_val_dict["config"] = "could not get version"
            return
        elif "2singleDACs" in _msg:
            self._hw_return_val_dict["config"] = "2 single DACs"
        elif "1dualDAC" in _msg:
            self._hw_return_val_dict["config"] = "1 dual DAC"
        elif "8bit" in _msg:
            self._hw_return_val_dict["config"] = "8 bit"
        elif "10bit" in _msg:
            self._hw_return_val_dict["config"] = "10 bit"
        elif "12bit" in _msg:
            self._hw_return_val_dict["config"] = "12 bit"
        elif "invalidDACresolution" in _msg:
            self._hw_return_val_dict["config"] = "invalid DAC resolution"

        self._execution_successful_flag.set()


class LedPwm(_BaseLedCommand):
    """
    This class wraps the 'led pwm' command which adjusts the duty cycle of a given channel
    The hardware either returns 'ok' or 'not ok'.
    """

    def __init__(self):
        super().__init__("led pwm", ["channel", "duty"])
        self.channel: _tp.Optional[int] = None
        self.duty: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\r", "")
        super()._parse_cmd_result(_msg)
        self._hw_return_val_dict["msg"] = _msg
        _msg = _msg.replace(" ", "")

        if "notok" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")


class AdvancedLedPwm(_BaseLedCommand):
    """
    This class wraps the 'led pwm' command which adjusts the duty cycle of a given channel
    The hardware either returns 'ok' or 'not ok'.
    """

    def __init__(self):
        super().__init__("led pwm", ["bus", "addr", "ch", "duty"])
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.ch: _tp.Optional[str] = None
        self.duty: _tp.Optional[int] = None
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\n", "").replace(" ", "")
        super()._parse_cmd_result(_msg)
        self._hw_return_val_dict["msg"] = _msg

        if "wrongchannel" in _msg:
            self._execution_successful_flag.clear()
            return
        if "invalidbusoraddress" in _msg:
            self._execution_successful_flag.clear()
            return
        if "ok" in _msg:
            self._execution_successful_flag.set()
            return
        raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")


class LedFreq(_BaseLedCommand):
    """
    This class wraps the 'led freq' command which adjusts the frequency of a given channel
    The hardware either returns 'ok' or 'not ok'.
    """

    def __init__(self):
        super().__init__("led freq", ["channel", "freq"])
        self.channel: _tp.Optional[int] = None
        self.freq: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\r", "")
        super()._parse_cmd_result(_msg)
        _msg = _msg.replace(" ", "")
        if "notok" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")


class AdvancedLedFreq(_BaseLedCommand):
    """
    This class wraps the 'led freq' command which adjusts the frequency of a given channel
    The hardware either returns 'ok' or 'not ok'.
    """

    def __init__(self):
        super().__init__("led freq", ["bus", "addr", "ch", "freq"])
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.ch: _tp.Optional[str] = None
        self.freq: _tp.Optional[int] = None
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\r", "")
        super()._parse_cmd_result(_msg)
        _msg = _msg.replace(" ", "")
        if "notok" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")


class LedCurrent(_BaseLedCommand):
    """
    This class wraps the 'led current' command, which adjust the current of a LED specified by bus,
    address and channel.

    Expected hardware results are either:

        * 'ok'
        * 'wrong channel'
        * 'invalid bus or address'
    """

    def __init__(self):
        super().__init__("led current", ["channel", "current"])
        self.channel: _tp.Optional[int] = None
        self.current: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\n", "").replace(" ", "")
        super()._parse_cmd_result(msg)
        if "notok" in _msg:
            self._execution_successful_flag.clear()
        elif "ok" in _msg:
            self._execution_successful_flag.set()
        else:
            raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")
        self._execution_successful_flag.set()


class AdvancedLedCurrent(_BaseLedCommand):
    """
    This class wraps the 'led current' command, which adjust the current of a LED specified by bus,
    address and channel.

    Expected hardware results are either:

        * 'ok'
        * 'wrong channel'
        * 'invalid bus or address'
    """

    def __init__(self):
        super().__init__("led current", ["bus", "addr", "ch", "current"])
        self.advanced_usage = True
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.ch: _tp.Optional[str] = None
        self.current: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\n", "").replace(" ", "")
        super()._parse_cmd_result(msg)
        if "invalidbusoraddress" in _msg:
            self._execution_successful_flag.clear()
            return

        if "wrongchannel" in _msg:
            self._execution_successful_flag.clear()
            return

        if "notok" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError(f"Unexpected Hardware Response: '{msg}'.")


class LedDriverPwm(_BaseLedCommand):
    """
    this class wraps the 'led driver pwm' command which adjusts the raw PWM-value of the LED driver.
    """

    def __init__(self):
        super().__init__(
            "led driver pwm", ["bus", "addr", "ch", "value"], needs_gamemode=True
        )
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.ch: _tp.Optional[str] = None
        self.value: _tp.Any = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)

        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "invalidbusoraddress" in _msg or "wrongchannel" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError("Unexpected hardware return value.")


class LedDriverDac(_BaseLedCommand):
    """
    this class wraps the 'led driver dac' command which adjusts the raw DAC-value of the LED driver.
    """

    def __init__(self):
        super().__init__(
            "led driver dac", ["bus", "addr", "ch", "value"], needs_gamemode=True
        )
        self.bus: _tp.Optional[int] = None
        self.addr: _tp.Optional[str] = None
        self.ch: _tp.Optional[str] = None
        self.value: _tp.Optional[str] = None  # TODO: Test with actual hardware

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)

        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "invalidbusoraddress" in _msg or "wrongchannel" in _msg:
            self._execution_successful_flag.clear()
            return

        if "ok" in _msg:
            self._execution_successful_flag.set()
            return

        raise AssertionError("Unexpected hardware return value.")


class LedEnable(_BaseLedCommand):
    """
    this class wraps the command which enables the LED
    """

    def __init__(self):
        super().__init__("led enable", ["board"], needs_gamemode=True)
        self.board: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        super()._parse_cmd_result(msg)
        if "enabled" not in msg:
            self._execution_successful_flag.clear()
        else:
            self._execution_successful_flag.set()


class LedDisable(_BaseLedCommand):
    """
    this class wraps the command which enables the LED
    """

    def __init__(self):
        super().__init__("led disable", ["board"], needs_gamemode=True)
        self.board: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        super()._parse_cmd_result(msg)
        if "disabled" not in msg:
            self._execution_successful_flag.clear()
        else:
            self._execution_successful_flag.set()


class LedRescan(_BaseLedCommand):
    """
    python wrapper for the 'led rescan' command, which triggers a led driver rescan

    the command output is stored as structured dictionary
    ```
    {
        "msg": str,  # the complete device answer
    }
    ```
    """

    def __init__(self):
        super().__init__("led rescan", [], needs_gamemode=False)
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        match = _re.search(
            "scanning for new LED drivers...OK",
            msg,
        )
        assert match, "Led rescan Unsuccessful."
        super()._parse_cmd_result(msg)
        self._execution_successful_flag.set()


class LedStatus(_BaseLedCommand):
    """
    python wrapper for the 'led status' command, which returns information about the
    LED drivers, their bus, address and software version

    the command output is stored as a structured dictionary
    ```
    {
        "msg": str,  # the complete device answer
        "Board": {  # board id as key
            "Driver": {  # driver id as key
                'status': str,  # driver status
                'bus': str,  # bus id
                'address': str,  # address
                'major': str,  # major software version
                'minor': str,  # minor software version
                'patch': str  # patch software version
            }
        }
    }
    ```
    """

    def __init__(self):
        super().__init__("led status", [], needs_gamemode=False)
        self.advanced_usage = True

    def _parse_cmd_result(self, msg: str) -> None:
        super()._parse_cmd_result(msg)
        output = self._hw_return_val_dict
        _ = list(filter(None, _re.split(pattern=r"(Board\s\d+)", string=msg)))
        if _[0] == "\n":
            _ = _[1:]
        status_by_board = None
        try:
            status_by_board = [(_[i], _[i + 1]) for i in range(0, len(_), 2)]
        except IndexError:
            # handle unexpected formatting
            raise AssertionError("Unable to parse HW-response")

        for _ in status_by_board:
            board_name: str = _[0]
            board_info_str: str = _[1]
            board_info_dict: _tp.Dict[str, _tp.Union[str, _tp.Dict[str, str]]] = {}

            matches: _tp.List[_re.Match] = list(
                _re.finditer(
                    pattern=r"(?P<driver>Driver\s+\d+):\s+(?P<status>.*)\s+Bus:\s+(?P<bus>\d+),\s+address:\s+(?P<address>.*),\s+software version:\s+(?P<major>\d+).(?P<minor>\d+).(?P<patch>\d+)",  # noqa: E501
                    string=board_info_str,
                )
            )
            assert matches
            for match in matches:
                driver_info: _tp.Dict[str, str] = {}
                driver: str = match.group("driver")
                assert driver

                status: str = match.group("status")
                assert status
                driver_info["status"] = status

                bus: str = match.group("bus")
                assert bus
                driver_info["bus"] = bus

                address: str = match.group("address")
                assert address
                driver_info["address"] = address

                major: str = match.group("major")
                assert major
                driver_info["major"] = major

                minor: str = match.group("minor")
                assert minor
                driver_info["minor"] = minor

                patch: str = match.group("patch")
                assert patch
                driver_info["patch"] = patch

                board_info_dict[driver] = driver_info

            output[board_name] = board_info_dict

        output["msg"] = msg
        self._hw_return_val_dict = output
        self._execution_successful_flag.set()
