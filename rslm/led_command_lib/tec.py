"""
    functional tec command classes are defined here
    Make sure that all definitions, which are not a command class, start with an undersco_re.
    Name definitions with an underscore will be filtered out. This way using the dir()-function all
    relevant classes can be attained programmatically.
"""

import logging as _logging
import re as _re
import typing as _tp

from rslm.led_command_lib import GenericCommand as _GenericCommand

_logging.getLogger(__name__).addHandler(_logging.NullHandler())

_CONFIG_SECTION_ID: str = "TEC"


class _BaseTecCommand(_GenericCommand):
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: _tp.List[str] = [],
        needs_gamemode: bool = False,
    ):
        super().__init__(
            base_command=base_command,
            ordered_param_name_list=ordered_param_name_list,
            needs_gamemode=needs_gamemode,
            advanced_usage=False,
        )
        self._config_section_name = _CONFIG_SECTION_ID


class TecStatus(_BaseTecCommand):
    def __init__(self):
        super().__init__("tec status", [])
        # raise NotImplementedError("tec commands are currently not supported")

    def _parse_cmd_result(self, msg: str) -> None:
        """
        Example dict:
            {
                'timestamp': 1713959861.6167622,
                'Board 1': {
                    'TEC FW-Version': '0.5.1-simulation',
                    'TEC state': 'stopped',
                    'Channel 1': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    },
                    'Channel 2': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    }
                },
                'Board 2': {
                    'TEC FW-Version': '0.5.1-simulation',
                    'TEC state': 'stopped',
                    'Channel 1': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    },
                    'Channel 2': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    }
                },
                'Board 3': {
                    'TEC FW-Version': '0.5.1-simulation',
                    'TEC state': 'stopped',
                    'Channel 1': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    },
                    'Channel 2': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    }
                },
                'Board 4': {
                    'TEC FW-Version': '0.5.1-simulation',
                    'TEC state': 'stopped',
                    'Channel 1': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    },
                    'Channel 2': {
                        'LED temp (wanted)': '0.000 °C',
                        'LED temp (current)': '0.000 °C',
                        'Heatsink temp': '0.000 °C',
                        'Fan speed': '-1.000 rpm'
                    }
                }
            }
        """

        def __parse_tec_status(
            tec_status_str,
        ) -> _tp.Dict[str, _tp.Union[str, _tp.Dict[str, str]]]:
            output = {}
            _ = list(
                filter(None, _re.split(pattern=r"(Board\s\d+)", string=tec_status_str))
            )
            if _[0] == "\n":
                _ = _[1:]
            tec_by_board = None
            try:
                tec_by_board = [(_[i], _[i + 1]) for i in range(0, len(_), 2)]
            except IndexError:
                # handle unexpected formatting
                raise AssertionError("Unable to parse HW-response")

            for _ in tec_by_board:
                info_dict = {}
                board_name = _[0]
                board_info = _[1]

                for entry in ["TEC FW-Version", "TEC state"]:
                    match = _re.search(entry + r":\s*(?P<result>.*)", board_info)
                    assert match
                    result = match.group("result")
                    assert result
                    info_dict[entry] = result.replace("\r", "").replace("\n", "")

                board_by_channel = list(
                    filter(
                        None, _re.split(pattern=r"(Channel\s\d+)", string=board_info)
                    )
                )[1:]
                try:
                    board_by_channel = [
                        (board_by_channel[i], board_by_channel[i + 1])
                        for i in range(0, len(board_by_channel), 2)
                    ]
                except IndexError:
                    # handle unexpected formatting
                    raise AssertionError("Unable to parse HW-response")

                for channel in board_by_channel:
                    channel_dict = {}
                    for entry in [
                        r"LED temp \(wanted\)",
                        r"LED temp \(current\)",
                        "Heatsink temp",
                        "Fan speed",
                    ]:
                        match = _re.search(entry + r":\s*(?P<result>.*)", channel[1])
                        assert match
                        result = match.group("result")
                        assert result
                        channel_dict[entry.replace("\\", "")] = result.replace(
                            "\r", ""
                        ).replace("\n", "")
                    info_dict[channel[0]] = channel_dict

                output[board_name] = info_dict
            return output

        assert msg
        super()._parse_cmd_result(msg)
        self._hw_return_val_dict.update(__parse_tec_status(tec_status_str=msg))
        self._execution_successful_flag.set()


class TecTemp(_BaseTecCommand):
    def __init__(self):
        super().__init__("tec temp", ["board", "ch", "temp"])
        self.board: _tp.Optional[int] = None
        self.ch: _tp.Optional[str] = None
        self.temp: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "notok" in _msg:
            self._execution_successful_flag.clear()
        elif "ok" in _msg:
            self._execution_successful_flag.set()
        else:
            raise AssertionError(f"Unexpected hardware return value: '{msg}'")


class TecStart(_BaseTecCommand):
    def __init__(self):
        super().__init__("tec start", ["board"], True)
        self.advanced_usage = True
        self.board: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "notok" in _msg:
            self._execution_successful_flag.clear()
        elif "ok" in _msg:
            self._execution_successful_flag.set()
        else:
            raise AssertionError(f"Unexpected hardware return value: '{_msg}'")


class TecStop(_BaseTecCommand):
    def __init__(self):
        super().__init__("tec stop", ["board"], True)
        self.advanced_usage = True
        self.board: _tp.Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        _msg: str = msg.replace("\n", "").replace("\r", "").replace(" ", "")
        if "notok" in _msg:
            self._execution_successful_flag.clear()
        elif "ok" in _msg:
            self._execution_successful_flag.set()
        else:
            raise AssertionError(f"Unexpected hardware return value: '{msg}'")
