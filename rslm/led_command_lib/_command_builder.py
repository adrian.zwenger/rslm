import logging

from rslm.led_command_lib import common as _common
from rslm.led_command_lib import led as _led
from rslm.led_command_lib import power as _power
from rslm.led_command_lib import tec as _tec
from rslm.led_command_lib._generic_command import GenericCommand

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)
LOGGER_DEBUG_ENABLED: bool = LOGGER.isEnabledFor(logging.DEBUG)
LOGGER_INFO_ENABLED: bool = LOGGER.isEnabledFor(logging.INFO)


class CommandBuilder:
    @staticmethod
    def __assert_config_status() -> None:
        if not GenericCommand.hardware_config:
            """parse config toml and set dict"""
            raise AttributeError(
                "No config was loaded. Please configure the GenericCommand first."
            )

    @staticmethod
    def info(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.Info()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def restart(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.Restart()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def stop(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.Stop()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def status_led(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.StatusLed()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def game_mode_status(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.GameModeStatus()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def game_mode(enter_gamemode: bool, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.GameMode()
        cmd.enter_gamemode = enter_gamemode
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def memory(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _common.Memory()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_version(bus: int, addr: str, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedVersion()
        cmd.bus = bus
        cmd.addr = addr
        return cmd

    @staticmethod
    def led_config(bus: int, addr: str, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedConfig()
        cmd.bus = bus
        cmd.addr = addr
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_pwm(channel: int, duty: int, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedPwm()
        cmd.channel = channel
        cmd.duty = duty
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def advanced_led_pwm(
        bus: int, addr: str, ch: str, duty: int, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.AdvancedLedPwm()
        cmd.bus = bus
        cmd.addr = addr
        cmd.ch = ch
        cmd.duty = duty
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_freq(channel: int, freq: int, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedFreq()
        cmd.channel = channel
        cmd.freq = freq
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def advanced_led_freq(
        bus: int, addr: str, ch: str, freq: int, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.AdvancedLedFreq()
        cmd.bus = bus
        cmd.addr = addr
        cmd.ch = ch
        cmd.duty = freq
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_current(
        channel: int, current: int, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedCurrent()
        cmd.channel = channel
        cmd.current = current
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def advanced_led_current(
        bus: int, addr: str, ch: str, current: int, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.AdvancedLedCurrent()
        cmd.bus = bus
        cmd.addr = addr
        cmd.ch = ch
        cmd.current = current
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_driver_pwm(
        bus: int, addr: str, ch: str, value: int, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedDriverPwm()
        cmd.bus = bus
        cmd.addr = addr
        cmd.ch = ch
        cmd.value = value
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_driver_dac(
        bus: int, addr: str, ch: str, value: str, delay_seconds: float = 0.0
    ) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedDriverDac()
        cmd.bus = bus
        cmd.addr = addr
        cmd.ch = ch
        cmd.value = value
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_enable(board: int, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedEnable()
        cmd.board = board
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_disable(board: int, delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedDisable()
        cmd.board = board
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_rescan(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedRescan()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def led_status(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _led.LedStatus()
        cmd.delay_seconds = delay_seconds
        return cmd

    @staticmethod
    def power_status(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _power.PowerStatus()
        cmd.delay_seconds = delay_seconds
        return cmd

    # TODO implement Tec commmands
    @staticmethod
    def tec_status(delay_seconds: float = 0.0) -> GenericCommand:
        CommandBuilder.__assert_config_status()
        cmd: GenericCommand = _tec.TecStatus()
        cmd.delay_seconds = delay_seconds
        return cmd
