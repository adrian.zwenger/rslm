"""
    fake commands for testing purposes are defined here
"""
import logging
from typing import List, Optional

from rslm.led_command_lib import GenericCommand

logging.getLogger(__name__).addHandler(logging.NullHandler())

CONFIG_SECTION_ID: str = "FAKE"


class BaseFakeCommand(GenericCommand):
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: List[str] = [],
        needs_gamemode: bool = False,
    ):
        super().__init__(
            base_command=base_command,
            ordered_param_name_list=ordered_param_name_list,
            needs_gamemode=needs_gamemode,
            advanced_usage=False,
        )
        self._config_section_name = CONFIG_SECTION_ID


class FakeCommand(BaseFakeCommand):
    def __init__(
        self,
        int_val: Optional[int] = None,
    ):
        super().__init__("fake command", ["string", "integer", "float_val", "bool_val"])
        self.string: Optional[str] = str(int_val) if int_val else None
        self.integer: Optional[int] = int_val if int_val else None
        self.float_val: Optional[float] = int_val + 0.1 if int_val else None
        self.bool_val: Optional[bool] = bool(int_val) if int_val else None

    def _parse_cmd_result(self, msg: str) -> None:
        super()._parse_cmd_result(msg)
        self._execution_successful_flag.set()


class FakeCustomCommand(BaseFakeCommand):
    def __init__(self):
        super().__init__("fake custom_command", ["string", "integer"])
        self.string: Optional[str] = None
        self.integer: Optional[int] = None

    def _parse_cmd_result(self, msg: str) -> None:
        super()._parse_cmd_result(msg)
        self._execution_successful_flag.set()
