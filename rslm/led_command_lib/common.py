"""
    command command classes are defined here
    Make sure that all definitions, which are not a command class, start with an undersco_re.
    Name definitions with an underscore will be filtered out. This way using the dir()-function all
    relevant classes can be attained programmatically.
"""
import logging as _logging
import re as _re
import typing as _tp
from time import sleep as _sleep

from rslm.led_command_lib import GenericCommand as _GenericCommand

_logging.getLogger(__name__).addHandler(_logging.NullHandler())

_CONFIG_SECTION_ID: str = "COMMON"


class _BaseCommonCommand(_GenericCommand):
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: _tp.List[str] = [],
        needs_gamemode: bool = False,
    ):
        super().__init__(base_command, ordered_param_name_list, needs_gamemode)
        self._config_section_name = _CONFIG_SECTION_ID


class Info(_BaseCommonCommand):
    """
    python wrapper for the 'info' command
    'info' command returns the software revision
    Software revision information is structured as a dictionary with following structure:
    ```
    {
        "msg": str,  # the complete device answer
        "major": str,  # extracted major software revision number
        "minor": str,  # extracted minor software revision number
        "patch": str,  # extracted patch software revision number
        "sim": bool  # boolean flag if the software is of the simulation variety
    }
    ```
    """

    def __init__(self):
        super().__init__("info", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        match = _re.search(
            r"(?P<major>\d+)(?:\.(?P<minor>\d+))(?:\.(?P<patch>\d+))(?:(?P<simu>-simulation))?",
            # "Software version: <major>.<minor>.<patch>[-simulation]""
            msg,
        )
        assert match, "Unable to parse message from serial device."
        major: str = match.group("major")
        minor: str = match.group("minor")
        patch: str = match.group("patch")
        sim: bool = bool(match.group("simu"))
        assert major, "Unable to determine major software revision."
        assert minor, "Unable to determine minor software revision."
        assert patch, "Unable to determine software patch revision."

        self._hw_return_val_dict.update(
            {
                "major": major,
                "minor": minor,
                "patch": patch,
                "sim": sim,
            }
        )
        self._execution_successful_flag.set()


class Restart(_BaseCommonCommand):
    """
    python wrapper for the 'restart' command
    'restart' resets and reboots HW. All HW-outputs are switched off.
    """

    def __init__(self):
        super().__init__("restart", [])

    def _parse_cmd_result(self, msg: str) -> None:
        _sleep(secs=2)
        self._hw_return_val = None
        self._hw_return_val_dict = {}
        self._execution_successful_flag.set()


class Stop(_BaseCommonCommand):
    """
    python wrapper for the 'stop' command
    'stop' turns all VLED-voltages off and deactivates all LED-drivers
    """

    def __init__(self):
        super().__init__("stop", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert "ok" in msg, "Stop command was not acknowledged by the serial device."
        super()._parse_cmd_result("ok")
        self._execution_successful_flag.set()


class StatusLed(_BaseCommonCommand):
    """
    python wrapper for the 'statusled test' command
    'statusled test' turns all LEDs on for 2 seconds.
    Afterwards previous LED-state is recovered
    """

    def __init__(self):
        super().__init__("statusled test", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert (
            "ok" in msg
        ), "Status-LED test command was not acknowledged by the serial device."
        super()._parse_cmd_result("ok")
        self._execution_successful_flag.set()


class GameModeStatus(_BaseCommonCommand):
    """
    python wrapper for the 'gamemode' status command.
    returns if device access is currently privileged.

    hw answer is stored in a dict as follows:
    ```
    {
        "msg": str,  # the complete device answer
        "status": bool  # True -> gamemode on; False -> gamemode off
    }
    ```
    """

    def __init__(self):
        super().__init__(base_command="gamemode", ordered_param_name_list=[])

    def _parse_cmd_result(self, msg: str) -> None:
        match = _re.search(
            r"current gamemode: (?P<status>\d+)",
            # "current gamemode: <number>"
            msg,
        )
        assert match, "Unable to parse message from serial device."
        status: str = match.group("status")
        assert status, "Unable to determine if gamemode is active"
        super()._parse_cmd_result(msg)
        status_bool: bool = bool(int(status))  # 0->off; not 0 -> on
        self._hw_return_val = "ok"
        self._hw_return_val_dict.update({"status": status_bool})
        self._execution_successful_flag.set()


class GameMode(_BaseCommonCommand):
    """
    python wrapper for the 'gamemode' command
    'gamemode 0' exit privileged mode
    'gamemode 1' enter privileged mode
    """

    def __init__(self):
        super().__init__("gamemode", ["enter_gamemode"])
        self.enter_gamemode: _tp.Optional[bool] = None

    def to_str(self):
        """Overrides GenericCommand.to_str()
        creates string representation of this command"""
        return f"gamemode {'1' if self.enter_gamemode else '0'}"

    def _parse_cmd_result(self, msg: str) -> None:
        assert (
            "ok" in msg
        ), "Gamemmode command was not acknowledged by the serial device."
        super()._parse_cmd_result("ok")
        self._execution_successful_flag.set()


class Memory(_BaseCommonCommand):
    """
    This class wraps the 'memory' command, which returns the current memory usage of the board.

    The returned information is structured in a dict:

    ```
    {
        "total heap": int,  # total heap in bytes
        "free heap": int,  # total free heap in bytes
        "lowest free heap": int  # lowest free heap in bytes
    }
    ```
    """

    def __init__(self):
        super().__init__("memory", [])

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        super()._parse_cmd_result(msg)
        # self._hw_return_val = msg

        match: _tp.Optional[_re.Match] = _re.search(
            pattern=r"Total heap space:\s+(?P<total>\d+)\s+Free heap space:\s+(?P<free>\d+)\s+Lowest free heap space:\s+(?P<lowest>\d+)",  # noqa: E501
            string=msg,
        )
        assert match

        total_heap: str = match.group("total")
        assert total_heap, "Unable to parse total heap size"

        free_heap: str = match.group("free")
        assert free_heap, "Unable to parse free heap size"

        lowest_heap: str = match.group("lowest")
        assert lowest_heap, "Unable to parse lowest free heap size"

        self._hw_return_val_dict.update(
            {
                "total heap": total_heap,
                "free heap": free_heap,
                "lowest free heap": lowest_heap,
            }
        )
        self._execution_successful_flag.set()
