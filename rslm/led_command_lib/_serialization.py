import importlib
import logging
import xml.etree.ElementTree as ET
from typing import Any, List, Optional, Sequence, Union

from rslm.led_command_lib._generic_command import (
    API_CONFIG_PARAM_TYPE_BOOL_ID,
    API_CONFIG_PARAM_TYPE_FLOAT_ID,
    API_CONFIG_PARAM_TYPE_INT_ID,
    API_CONFIG_PARAM_TYPE_STRING_ID,
    ConsecutiveCommands,
    GenericCommand,
)

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)
LOGGER_DEBUG_ENABLED: bool = LOGGER.isEnabledFor(logging.DEBUG)
LOGGER_INFO_ENABLED: bool = LOGGER.isEnabledFor(logging.INFO)


def serialize_generic_command_program(
    cmds: Union[GenericCommand, List[Union[GenericCommand, List[GenericCommand]]]]
) -> ET.ElementTree:
    def __serialize(_cmds) -> ET.Element:
        if isinstance(_cmds, GenericCommand):
            _cmds.is_valid()
            return _cmds.serialize()
        assert isinstance(_cmds, list), f"Unknown type '{type(_cmds)}'."

        list_root: ET.Element = ET.Element("ConsecutiveCommands")

        if isinstance(_cmds, ConsecutiveCommands):
            if _cmds.short_name:
                list_root.set("name", _cmds.short_name)
            if _cmds.description:
                serialized_param: ET.Element = ET.SubElement(list_root, "description")
                serialized_param.text = _cmds.description

            if _cmds.delay_seconds is not None:
                list_root.set("delay_seconds", str(_cmds.delay_seconds))
            else:
                list_root.set("delay_seconds", str(_cmds.get_delay_seconds()))

        for element in _cmds:
            if isinstance(element, GenericCommand):
                element.delay_seconds = 0
                list_root.append(element.serialize())
            elif isinstance(element, list):
                list_root.append(__serialize(element))
            else:
                raise AssertionError(
                    f"Element not GenericCommand or List[GenericCommand], but'{type(element)}'."
                )
        return list_root

    root_element = ET.Element("LedCommandPlaylist")
    if isinstance(cmds, GenericCommand):
        root_element.append(__serialize(cmds))
        return ET.ElementTree(root_element)

    elif isinstance(cmds, list):
        if isinstance(cmds, ConsecutiveCommands):
            if cmds.short_name:
                root_element.set("name", cmds.short_name)
            if cmds.description:
                serialized_param: ET.Element = ET.SubElement(
                    root_element, "description"
                )
                serialized_param.text = cmds.description

        for cmd in cmds:
            root_element.append(__serialize(cmd))
        return ET.ElementTree(root_element)

    raise AssertionError(
        f"Element not GenericCommand or List[GenericCommand], but'{type(cmds)}'."
    )


def deserialize_generic_command_program(
    xml_root: Union[ET.ElementTree, str]
) -> Union[GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]]:
    # Union[GenericCommand, List[GenericCommand]]:
    """
    this methods takes xml serialized GenericCommands as input and returns a recursive list of
    GenericCommands.
    All recursively contained lists of GenericCommands are sets of Commands which are to
    be directly executed on after the other in the order of the list.
    """

    def __deserialize(
        _xml_element: ET.Element,
    ) -> Union[GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]]:
        # Union[GenericCommand, List[GenericCommand]]:
        program: List[Any] = ConsecutiveCommands()
        if (
            _xml_element.tag == "LedCommandPlaylist"
            or _xml_element.tag == "ConsecutiveCommands"
        ):
            if "name" in _xml_element.attrib:
                program.short_name = _xml_element.get("name")

            if "delay_seconds" in _xml_element.attrib:
                _: Optional[str] = _xml_element.get("delay_seconds")
                assert _
                program.delay_seconds = int(_)

            for child in _xml_element:
                if child.tag == "description":
                    program.description = child.text
                else:
                    program.append(__deserialize(child))

            if program.delay_seconds is None:
                program.delay_seconds = program.get_delay_seconds()
            return program

        elif _xml_element.tag == "LedCommand":
            assert "module" in _xml_element.keys()
            module_name: str = _xml_element.get(key="module")  # type: ignore

            assert "type" in _xml_element.keys()
            class_name: str = _xml_element.get("type")  # type: ignore

            assert "delayInSeconds" in _xml_element.keys()
            delay_seconds: float = float(_xml_element.get("delayInSeconds"))  # type: ignore

            cmd: GenericCommand = getattr(
                importlib.import_module(module_name), class_name
            )()
            cmd.delay_seconds = delay_seconds
            for param in _xml_element:
                param_name: str = param.tag
                assert "type" in param.keys()
                param_type: str = param.get("type")  # type: ignore
                if not param.text or param.text == "None":
                    LOGGER.critical(
                        "Parameter '%s' was serialized without a value.", param.tag
                    )
                assert param.text is not None
                if param_type == API_CONFIG_PARAM_TYPE_STRING_ID:
                    cmd.__setattr__(param_name, str(param.text))
                elif param_type == API_CONFIG_PARAM_TYPE_INT_ID:
                    cmd.__setattr__(param_name, int(param.text))
                elif param_type == API_CONFIG_PARAM_TYPE_FLOAT_ID:
                    cmd.__setattr__(param_name, float(param.text))
                elif param_type == API_CONFIG_PARAM_TYPE_BOOL_ID:
                    cmd.__setattr__(param_name, bool(param.text))
                else:
                    raise AssertionError(
                        f"Unknown parameter type '{param_type}'. Expected str, bool, int, or float."
                    )
            return cmd
        # else:
        #     raise AssertionError("Unknown XML-tag '{_xml_element.tag}'.")
        return program

    if isinstance(xml_root, ET.ElementTree):
        return __deserialize(xml_root.getroot())
    elif isinstance(xml_root, ET.Element):
        return __deserialize(xml_root)
    return __deserialize(ET.parse(xml_root).getroot())
