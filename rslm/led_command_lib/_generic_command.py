import importlib
import logging
import sys
import threading
import time as _time
import types
import uuid
import xml.etree.ElementTree as ET
from abc import ABCMeta, abstractmethod
from os.path import abspath, isfile
from typing import Any, Dict, List, Optional, Sequence, Union

import tomllib_python311 as tomllib
from rslm._default_hw_config import (
    API_CONFIG_CUSTOM_COMMANDS_SECTION_ID,
    API_CONFIG_PARAM_TYPE_BOOL_ID,
    API_CONFIG_PARAM_TYPE_FLOAT_ID,
    API_CONFIG_PARAM_TYPE_ID,
    API_CONFIG_PARAM_TYPE_INT_ID,
    API_CONFIG_PARAM_TYPE_STRING_ID,
    API_CONFIG_SECTION_ID,
    API_CONFIG_VALID_VALUES_SECTION_ID,
)

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)
LOGGER_DEBUG_ENABLED: bool = LOGGER.isEnabledFor(logging.DEBUG)
LOGGER_INFO_ENABLED: bool = LOGGER.isEnabledFor(logging.INFO)


def _make_getter(attr):
    def getter(self):
        return self.generic_getter(self, attr)

    return getter


def _make_setter(attr):
    def setter(self, value: Any):
        return self.generic_setter(attr=attr, value=value)

    return setter


class __GenericCommandMetaClass(ABCMeta):
    """
    Metaclass to execute commands after construction is done.
    It is used for __setattr__ override to include parameter checking on attribute assignment
    via "=", setattr() or __setattr__()
    """

    def __call__(cls, *args, **kwargs):
        obj = super().__call__(*args, **kwargs)
        obj._python_obj_initialized = True  # set object as initialized
        return obj


class GenericCommand(metaclass=__GenericCommandMetaClass):  # type: ignore
    hardware_config_file: Optional[str] = None
    hardware_config: Optional[Dict[str, Dict[str, Any]]] = None
    """Generic base class for all Commands"""

    @abstractmethod
    def __init__(
        self,
        base_command: str,
        ordered_param_name_list: List[str] = [],
        needs_gamemode: bool = False,
        advanced_usage: bool = True,
    ):
        # if self.__class__ is GenericCommand:
        #     LOGGER.critical("GenericCommand is an abstract class. Please subtype.")
        #     raise RuntimeError("GenericCommand is abstract.")
        if not self.hardware_config:
            """parse config toml and set dict"""
            raise AttributeError("No config was loaded")

        self._python_obj_initialized: bool = False

        self.sorted_param_list: List[str] = ordered_param_name_list
        """list of parameters this command has"""

        self.base_command: str = base_command
        """the command without parameters"""

        self._hw_return_val: Optional[str] = None
        """the return value from hardware as a string"""

        self._hw_return_val_dict: Dict[str, Any] = {}
        """the return value from hardware as a dict"""

        self.needs_gamemode: bool = needs_gamemode
        """boolean flag indicating if this command needs elevated priviliges"""

        self.lock: threading.Condition = threading.Condition()
        """lock to prevent concurrent modifications"""

        for attr in self.sorted_param_list:
            # add getter and setters
            _: str = attr.replace(" ", "_")
            # use MethodType to cast dynamic method to instance method
            self.__dict__[f"get_{_}"] = types.MethodType(_make_getter(attr), self)
            self.__dict__[f"set_{_}"] = types.MethodType(_make_setter(attr), self)

        self.uuid: uuid.UUID = uuid.uuid4()
        """uuid for command identification"""

        self.timed_out: threading.Event = threading.Event()
        """timeout flag. if set a timeout occured during execution"""

        self._config_section_name: Optional[str] = None
        """
            section name where command specifications are stored in the config toml-file
            value is to be set by sub classes
        """

        self._executed: threading.Event = threading.Event()
        """stores if cmd was executed. to be set by executor"""

        self._execution_successful_flag: threading.Event = threading.Event()
        """stores if execution was successful"""

        self.delay_seconds: float = 0
        """
            field where user can store how many seconds is to be waited before executing this
            command in a series of commands. The idea is that the user can create a list of commands
            and that a command executor takes that list, waits for X seconds, then executes a
            command, and repeating this process.
        """

        self.advanced_usage: bool = advanced_usage
        """
            boolean field that indicates if the commands is meant for advanced users that know
            how to handle the actual hardware.
        """

    def set_cmd_as_executed(self):
        if not self._executed.isSet():
            self._executed.set()

    def was_executed(self) -> bool:
        return self._executed.isSet()

    def execution_successful(self) -> bool:
        return self._execution_successful_flag.isSet()

    def generic_getter(self, attr: str):
        """access value of passed attribute
        Args:
            attr (str): name of the attribute
        Returns:
            Any: value of the attribute
        """
        return self.__getattribute__(attr)

    def generic_setter(self, attr: str, value: Any):
        """set value of the passed attribute
        Args:
            attr (str): name of the attribute to be set
            val (Any): the new value of the attribute
        """
        return self.__setattr__(attr, value)

    def set_cmd_result(self, msg: str, ignore_result_check: bool = False) -> None:
        """the execution result of this command is stored with this method.
        Result is parsed for needed data and stored in a dict.

        take a look at:
            * GenericCommand.__hw_return_val
            * GenericCommand.__hw_return_val_dict
            * GenericCommand.__parse_cmd_result()

        inheriting classes should implement result validity checking by overriding this or the
        GenericCommand.__parse_cmd_result() method.

        An AssertionError is propagated to the caller of this method if result is not valid.

        Args:
            msg (str): execution result as string
        """
        if ignore_result_check:
            self._hw_return_val = msg
            self._hw_return_val_dict = {"msg": msg, "timestamp": _time.time()}
            self._execution_successful_flag.set()
            return

        self._parse_cmd_result(msg=msg)

    @abstractmethod
    def _parse_cmd_result(self, msg: str) -> None:
        """Result parsing and structured storage into a dict.
        This method needs to be overriden by each command.

        inheriting classes should implement result validity checking by overriding this or the
        GenericCommand._parse_cmd_result() method.
        self.execution_successful_flag.set() is to be called to indicate successful execution.

        An AssertionError is propagated to the caller of this method if result is not valid.
        """
        self._hw_return_val = msg
        self._hw_return_val_dict = {"msg": msg, "timestamp": _time.time()}
        # self._execution_successful_flag.set()

    def get_cmd_result_as_dict(self) -> Optional[Dict[str, Any]]:
        """the execution result of this command is accessed with this method

        Returns:
            Dict[str, Any] or None: execution result as string if applicable
        """
        return self._hw_return_val_dict if self._hw_return_val_dict else None

    def get_cmd_result_as_str(self) -> Optional[str]:
        """the execution result of this command is accessed with this method
        Returns:
            str or None: execution result as string if applicable
        """
        return self._hw_return_val

    # def cmd_result_is_none(self) -> bool:
    #     """returns true if cmd result is None"""
    #     return self.__hw_return_val is None

    def __str__(self):
        """creates string representation of this command"""
        # return self.to_str()
        return self.get_executable_string()

    def to_str(self):
        """creates string representation of this command"""
        __cmd_as_str: str = self.base_command
        for param in self.sorted_param_list:
            __cmd_as_str += " " + str(self.generic_getter(param))
        return __cmd_as_str

    def get_executable_string(self) -> str:
        """
        returns the stringh which can be executed.
        Raises Union[AssertionError, AttributeError] if command is invalid
        """
        if not self.sorted_param_list:
            # no parameters? no check needed
            return self.to_str()
        if self._config_section_name is None:
            LOGGER.critical("Command type was not set. Unable to check config.")
            raise AttributeError("Command type not set.")
        cfg: Dict[str, Any] = self.__get_config()
        for param in self.sorted_param_list:
            assert (
                self.generic_getter(param) is not None
            ), f"mandatory parameter '{param}' is None."
            assert (
                self.__check_if_param_conforms_spec(
                    param_name=param,
                    param_value=self.generic_getter(attr=param),
                    specifications_dict=cfg[param],
                )
                is True
            ), f"Parameter '{param}' does not conform specification. View logs for more details"
        return self.to_str()

    def is_valid(self) -> None:
        """raises AssertionError if command is invalid"""
        self.get_executable_string()

    def __hash__(self):
        return hash(self.uuid)

    def __eq__(self, obj: Any):
        if not isinstance(obj, type(self)):
            return False
        return self.uuid == obj.uuid

    def __get_config(self) -> Dict[str, Any]:
        """returns the needed section of the hardware config"""
        assert (
            self._config_section_name and GenericCommand.hardware_config
        ), "GenericCommand not properly configured."
        cfg: Dict[str, Any] = GenericCommand.hardware_config[API_CONFIG_SECTION_ID][
            self._config_section_name
        ]
        if self.base_command in cfg[API_CONFIG_CUSTOM_COMMANDS_SECTION_ID]:
            return cfg[self.base_command]
        return cfg

    @classmethod
    def read_config(cls, config_path: str) -> None:
        """static method to parse config file"""
        if LOGGER_INFO_ENABLED:
            LOGGER.info("Using config following file: %s", config_path)
        GenericCommand.hardware_config_file = config_path
        GenericCommand.hardware_config = tomllib.load(open(config_path, "rb"))  # type: ignore
        if LOGGER_DEBUG_ENABLED:
            LOGGER.debug(
                "Config %s loaded.\nContents:\n%s",
                abspath(config_path),
                str(GenericCommand.hardware_config),
            )

    def __check_if_param_conforms_spec(
        self, param_name: str, param_value: Any, specifications_dict: Dict[str, Any]
    ) -> bool:
        """checks if param conforms to the given specification"""

        def __string_conforms_spec() -> bool:
            """checks if param conforms to the given specification"""
            valid_vals: List[Any] = specifications_dict[
                API_CONFIG_VALID_VALUES_SECTION_ID
            ]
            if param_value not in valid_vals:
                LOGGER.critical(
                    """
    Value of parameter '%s' does not conform specification.
    Command ID: %s
    Command: '%s'
    Parameter: '%s'
    Parameter Value: '%s'
    Valid Values: %s""",
                    param_name,
                    self.uuid,
                    str(self),
                    param_name,
                    param_value,
                    valid_vals,
                )
                return False
            return True

        def __number_conforms_spec() -> bool:
            """checks if param conforms to the given specification"""
            valid_vals: List[Any] = specifications_dict[
                API_CONFIG_VALID_VALUES_SECTION_ID
            ]
            if len(valid_vals) < 2:
                LOGGER.critical(
                    """
    Bounds for parameter '%s' are incomplete ('%s').
    Config seems to be incomplete.""",
                    param_name,
                    valid_vals,
                )
                raise AttributeError("Config malformed. Check logs for more detail")
            if len(valid_vals) > 2:
                LOGGER.critical(
                    """
    Bounds for parameter '%s' are malformed ('%s').
    Expected two values. %s were passed.
    Config seems to be incomplete.""",
                    param_name,
                    valid_vals,
                    str(len(valid_vals)),
                )
                raise NotImplementedError(
                    "Config malformed. Check logs for more detail"
                )

            _min: int = min(valid_vals)
            _max: int = max(valid_vals)

            if _min <= param_value and _max >= param_value:
                return True

            LOGGER.critical(
                """
    Value of parameter '%s' does not conform specification.
    Command ID: %s
    Command: '%s'
    Parameter: '%s'
    Parameter Value: '%s'
    Lower Bound: %s
    Upper Bound: %s""",
                param_name,
                self.uuid,
                str(self),
                param_name,
                param_value,
                str(_min),
                str(_max),
            )
            return False

        specified_param_type: str = specifications_dict[API_CONFIG_PARAM_TYPE_ID]

        assert param_value is not None

        if specified_param_type == API_CONFIG_PARAM_TYPE_STRING_ID:
            if not isinstance(param_value, str):
                raise AttributeError(
                    f"Parameter '{param_name}'='{param_value}' ({type(param_value)}) is not a string."
                )
            if API_CONFIG_VALID_VALUES_SECTION_ID not in specifications_dict:
                # config has no restrictions on possible values
                if param_value:
                    return True
                return False
            return __string_conforms_spec()

        elif specified_param_type == API_CONFIG_PARAM_TYPE_INT_ID:
            if not isinstance(param_value, int):
                raise AttributeError(
                    f"Parameter '{param_name}'='{param_value}' ({type(param_value)}) is not an int."
                )
            if API_CONFIG_VALID_VALUES_SECTION_ID not in specifications_dict:
                # config has no restrictions on possible values
                return True
            return __number_conforms_spec()

        elif specified_param_type == API_CONFIG_PARAM_TYPE_FLOAT_ID:
            if not isinstance(param_value, float):
                raise AttributeError(
                    f"Parameter '{param_name}'='{param_value}' ({type(param_value)}) is not a float."
                )
            if API_CONFIG_VALID_VALUES_SECTION_ID not in specifications_dict:
                # config has no restrictions on possible values
                return True
            return __number_conforms_spec()

        elif specified_param_type == API_CONFIG_PARAM_TYPE_BOOL_ID:
            if not isinstance(param_value, bool):
                raise AttributeError(
                    f"Parameter '{param_name}'='{param_value}' ({type(param_value)}) is not a boolean."
                )
            return True

        else:
            LOGGER.critical(
                """
    Unexpected parameter type encountered in config.
    Parameter '%s' is specified as '%s'.
    Supported types are:
        %s""",
                param_name,
                specified_param_type,
                [
                    API_CONFIG_PARAM_TYPE_STRING_ID,
                    API_CONFIG_PARAM_TYPE_INT_ID,
                    API_CONFIG_PARAM_TYPE_FLOAT_ID,
                    API_CONFIG_PARAM_TYPE_BOOL_ID,
                ],
            )
            raise NotImplementedError(
                f"Specified parameter type '{param_name}' of '{specified_param_type}' is not supported."
            )

    def serialize(self) -> ET.Element:
        serialized_cmd: ET.Element = ET.Element("LedCommand")
        serialized_cmd.set("module", self.__module__)
        serialized_cmd.set("type", self.__class__.__name__)
        serialized_cmd.set("delayInSeconds", str(self.delay_seconds))
        for param in self.sorted_param_list:
            serialized_param: ET.Element = ET.SubElement(serialized_cmd, param)
            value: Any = self.__getattribute__(param)
            serialized_param.text = str(value)
            serialized_param.set("type", self.__get_config()[param]["type"])
        return serialized_cmd

    def __setattr__(self, attr: str, value: Any) -> None:
        if (
            not hasattr(self, "_python_obj_initialized")
            or not self._python_obj_initialized
        ):  # object is still being constructed. No type checking needed yet
            super().__setattr__(attr, value)
            return
        if attr in self.sorted_param_list:
            assert self.__check_if_param_conforms_spec(
                param_name=attr,
                param_value=value,
                specifications_dict=self.get_attr_specs(attr=attr),
            )
        if attr == "delay_seconds":
            assert value >= 0
        super().__setattr__(attr, value)

    def get_specs(self) -> Dict[str, Any]:
        return self.__get_config()

    def get_attr_specs(self, attr: str) -> Dict[str, Any]:
        if attr not in self.sorted_param_list:
            return {}
        return self.__get_config()[attr]

    def _reset(self) -> None:
        self.timed_out.clear()
        self._execution_successful_flag.clear()
        self._executed.clear()
        self._hw_return_val = None
        self._hw_return_val_dict = {}

    def _carbon_copy(self):
        _cmd: GenericCommand = getattr(
            importlib.import_module(self.__module__), self.__class__.__name__
        )()
        _cmd.delay_seconds = self.delay_seconds
        for attr in self.sorted_param_list:
            _cmd.__setattr__(attr, self.__getattribute__(attr))
        return _cmd


class _DummyGenericCommand(GenericCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _parse_cmd_result(self, msg: str) -> None:
        assert msg
        _msg: str = msg.replace("\n", "").replace("\r", "")
        self._hw_return_val = msg
        self._hw_return_val_dict = {"msg": _msg, "timestamp": _time.time()}
        self._execution_successful_flag.set()


def reset_execution_state(cmd: Union[GenericCommand, List[GenericCommand]]) -> None:
    """
    this functions reverts GenericCommands to an unaltered state. Use this function before the same
    GenericCommand object is to be executed again.
    """

    def __single(_cmd: GenericCommand) -> None:
        _cmd._reset()

    def __multiple(_cmd: Union[GenericCommand, List[GenericCommand]]) -> None:
        if isinstance(_cmd, list):
            for _ in _cmd:
                __multiple(_)
        elif isinstance(_cmd, GenericCommand):
            __single(_cmd)
        else:
            raise AssertionError()

    __multiple(cmd)


def create_dummy_command_from_string(
    cmd_str: str, needs_gamemode: bool = False
) -> GenericCommand:
    """creates a GenericCommand from a string so it can be processed just like other implemented
    commands. Do not that this is not advised as it circumvents all checks and just executes the
    string directly.

    set needs_gamemode to True if the string needs admin priv.

    Strongly discouraged!
    """
    cmd: GenericCommand = _DummyGenericCommand(
        base_command=cmd_str,
        ordered_param_name_list=[],
        needs_gamemode=needs_gamemode,
        advanced_usage=True,
    )
    return cmd


class ConsecutiveCommands(list):
    def __init__(self) -> None:
        super().__init__()
        self.description: Optional[str] = None
        self.short_name: Optional[str] = None
        self.delay_seconds: Optional[float] = None

    def get_delay_seconds(self) -> float:
        def __get_delay(_cmd: Union[GenericCommand, List[GenericCommand]]) -> float:
            if isinstance(_cmd, GenericCommand):
                return _cmd.delay_seconds
            elif isinstance(_cmd, list):
                return __get_delay(_cmd[-1])
            raise AssertionError(
                f"Expected GenericCommand or list of GenericCommands and not '{type(_cmd)}'."
            )

        if self.delay_seconds is not None:
            return self.delay_seconds
        if self:
            return __get_delay(self[-1])
        return 0

    def construct(self) -> None:
        self.clear()

    def is_valid(self):
        def __is_valid(cmd):
            if isinstance(cmd, GenericCommand):
                cmd.is_valid()
                return
            elif isinstance(cmd, list):
                for _ in cmd:
                    __is_valid(_)
            return

        __is_valid(self)


def cmd_carbon_copy(
    cmd: Union[GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]]
) -> Union[GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]]:
    def __single(_cmd: GenericCommand) -> GenericCommand:
        return _cmd._carbon_copy()

    def __multiple(
        _cmd: Union[
            GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]
        ]
    ) -> Union[GenericCommand, ConsecutiveCommands]:
        if isinstance(_cmd, list):
            _return = ConsecutiveCommands()
            if isinstance(_cmd, ConsecutiveCommands):
                if _cmd.short_name:
                    _return.short_name = _cmd.short_name
                if _cmd.description:
                    _return.description = _cmd.description
                _return.delay_seconds = (
                    _cmd.delay_seconds
                    if _cmd.delay_seconds is not None
                    else _cmd.get_delay_seconds()
                )

            for _ in _cmd:
                _return.append(__multiple(_))
            return _return
        elif isinstance(_cmd, GenericCommand):
            return __single(_cmd)
        else:
            raise AssertionError()

    return __multiple(cmd)


def configure_generic_commands(config_file: str) -> None:
    if not isfile(config_file):
        """abort if no config was passed.
        creates a default config file in the current working directory before exiting.
        """

        LOGGER.critical("The config file '%s' does not exist.", config_file)
        LOGGER.critical(
            "A default config will be created in the current working directory."
        )
        with open(config_file, "w") as default_config:
            from rslm._default_hw_config import DEFAULT_HW_CONFIG_TOML

            default_config.write(DEFAULT_HW_CONFIG_TOML)
        LOGGER.critical("Please review the generated config and restart this script.")
        sys.exit(-1)

    GenericCommand.read_config(config_path=config_file)
    return
