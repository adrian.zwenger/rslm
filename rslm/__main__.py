import argparse
import logging
import os
import sys
import time
from pathlib import Path

DEFAULT_CONFIG_PATH: str = "./hw_config.toml"
DEFAULT_LOG_FILE: str = "rslm"
file = Path(__file__).resolve()
parent = file.parent
sys.path.append(str(parent.parent))
__package__ = parent.name


def main():
    parser = argparse.ArgumentParser(
        description="Execute the program in a specific mode."
    )
    parser.add_argument(
        "--debug",
        type=str,
        required=False,
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="DEBUG",
        help="Set the logging level.",
    )

    parser.add_argument(
        "--configfile",
        type=str,
        required=False,
        default=DEFAULT_CONFIG_PATH,
        help="Hardware configuration file to use or create.",
    )

    parser.add_argument(
        "--mode",
        type=str,
        required=False,
        choices=["webapp", "native"],
        help=f"""Mode in which to run RSLM ("webapp" or "native").
            If not provided {DEFAULT_CONFIG_PATH} is used as the configuration file.""",
    )

    parser.add_argument(
        "--simulation",
        action="store_true",
        required=False,
        help="If this flag is set RSLM is simulates the hardware and does not use it.",
    )

    parser.add_argument(
        "--createconfig",
        action="store_true",
        required=False,
        help=f"""If flag is set a default configuration file is created in the current working directory.
            If CONFIGFILE is not provided, {DEFAULT_CONFIG_PATH} will be created/overwritten.""",
    )

    parser.add_argument(
        "--port",
        type=int,
        required=False,
        default=8081,
        help="Port to be used if RSLM is running in webapp mode.",
    )

    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.debug))
    logging.getLogger("flet_core").setLevel(logging.CRITICAL)
    logging.getLogger("flet_runtime").setLevel(logging.CRITICAL)

    _ = logging.FileHandler(DEFAULT_LOG_FILE + str(time.time()) + ".log")
    _.setFormatter(
        logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    )
    logging.getLogger().addHandler(_)

    if args.createconfig:
        with open(os.path.join(os.getcwd(), args.config_file), "w") as f:
            f.write("This is a new file.")
        return

    if args.mode == "webapp":
        from rslm.gui import start_as_webserver

        start_as_webserver(
            host="localhost",
            port=args.port,
            config_file=args.configfile,
            simulation=args.simulation,
        )
    elif args.mode == "native":
        from rslm.gui import start_gui_as_standalone

        start_gui_as_standalone(config_file=args.configfile, simulation=args.simulation)


if __name__ == "__main__":
    main()
