"""
This module is used internally to handle SIGINT receival.
Do take care, that signals are handled asynchronously by the main thread.
In other words, if the main thread is not alive, blocked, or, waiting on an event,
then SIGINT will only be processed once the main thread is freed up. Threads do not receive signals
in Python.
"""

import logging
import signal
import threading

LOGGER = logging.getLogger(__name__)
SIGINT_RECEIVED: threading.Event = threading.Event()
"""This flag is set if main thread is not blocked.
This whole library needs the main thread to be alive and not blocked to be able to set this flag
for internal threads.
"""


def _keyboard_interrupt_handler(signum, frame) -> None:
    """Used to override the default SIGINT signal handler.
    Instead of just raising a KeyboardInterrupt, the SIGINT_RECEIVED flag is set beforehand.
    """
    LOGGER.critical("SIGINT received. Notifying threads.")
    SIGINT_RECEIVED.set()
    raise KeyboardInterrupt()


def register():
    LOGGER.critical("Registering sigint handler.")
    signal.signal(
        signal.SIGINT, _keyboard_interrupt_handler
    )  # register new SIGINT handler


register()
