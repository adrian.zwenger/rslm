import logging

# import threading
from typing import List, Optional, Union

from rslm import CmdExecutor
from rslm._executor import execute_cmd
from rslm.led_command_lib import CommandBuilder, GenericCommand

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)
LOGGER_DEBUG_ENABLED: bool = LOGGER.isEnabledFor(logging.DEBUG)
LOGGER_INFO_ENABLED: bool = LOGGER.isEnabledFor(logging.INFO)


class LedControl:
    """
    TODO: keep updated and add new commands. Document too
    """

    def __init__(self, cmd_executor: CmdExecutor):
        self.executor: CmdExecutor = cmd_executor

    def __execute_cmd(
        self, cmd: Union[GenericCommand, List[GenericCommand]]
    ) -> Union[GenericCommand, List[GenericCommand]]:
        execute_cmd(executor=self.executor, cmd=cmd)
        return cmd

    def __execute_privileged_cmd(self, cmd: GenericCommand) -> Optional[GenericCommand]:
        try:
            assert self.game_mode(enter_gamemode=True) is not None
        except AssertionError:
            LOGGER.critical("Unable to get elevated privileges.")
            return None
        return_val: Union[GenericCommand, List[GenericCommand]] = self.__execute_cmd(
            cmd
        )
        assert isinstance(return_val, GenericCommand)
        try:
            assert self.game_mode(enter_gamemode=False) is not None
        except AssertionError:
            LOGGER.critical("Unable to release elevated privileges.")
        return return_val

    def __execute_single_cmd(
        self, cmd: GenericCommand, privileged: bool = False
    ) -> Optional[GenericCommand]:
        return_val: Optional[Union[GenericCommand, List[GenericCommand]]]
        if privileged:
            return_val = self.__execute_privileged_cmd(cmd)
        else:
            return_val = self.__execute_cmd(cmd)
        assert return_val and isinstance(return_val, GenericCommand)
        return return_val

    def execute_pre_constructed_cmd(
        self, cmds: Union[GenericCommand, List[GenericCommand]]
    ) -> Union[GenericCommand, List[GenericCommand]]:
        return self.__execute_cmd(cmds)

    def led_version(self, bus: int, addr: str) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_version(bus=bus, addr=addr)
        return self.__execute_single_cmd(cmd)

    def led_config(self, bus: int, addr: str) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_config(bus=bus, addr=addr)
        return self.__execute_single_cmd(cmd)

    def led_pwm(self, channel: int, duty: int) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_pwm(channel=channel, duty=duty)
        return self.__execute_single_cmd(cmd)

    def advanced_led_pwm(
        self, bus: int, addr: str, ch: str, duty: int
    ) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.advanced_led_pwm(
            bus=bus,
            addr=addr,
            ch=ch,
            duty=duty,
        )
        return self.__execute_single_cmd(cmd)

    def led_current(self, channel: int, current: int) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_current(
            channel=channel, current=current
        )
        return self.__execute_single_cmd(cmd=cmd)

    def advanced_led_current(
        self,
        bus: int,
        addr: str,
        ch: str,
        current: int,
    ) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.advanced_led_current(
            bus=bus, addr=addr, ch=ch, current=current
        )
        return self.__execute_single_cmd(cmd)

    def led_freq(self, channel: int, freq: int) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_freq(channel=channel, freq=freq)
        return self.__execute_single_cmd(cmd=cmd)

    def advanced_led_freq(
        self,
        bus: int,
        addr: str,
        ch: str,
        freq: int,
    ) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.advanced_led_freq(
            bus=bus, addr=addr, ch=ch, freq=freq
        )
        return self.__execute_single_cmd(cmd)

    def led_driver_pwm(
        self, bus: int, addr: str, ch: str, value: int
    ) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_driver_pwm(
            bus=bus, addr=addr, ch=ch, value=value
        )
        return self.__execute_single_cmd(cmd, privileged=True)

    def led_driver_dac(
        self, bus: int, addr: str, ch: str, value: str
    ) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_driver_dac(
            bus=bus, addr=addr, ch=ch, value=value
        )
        return self.__execute_single_cmd(cmd, privileged=True)

    def led_enable(self, board: int) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_enable(board=board)
        return self.__execute_single_cmd(cmd, privileged=True)

    def led_disable(self, board: int) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.led_disable(board=board)
        return self.__execute_single_cmd(cmd, privileged=True)

    def info(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.info()
        return self.__execute_single_cmd(cmd)

    def restart(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.restart()
        return self.__execute_single_cmd(cmd)

    def stop(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.stop()
        return self.__execute_single_cmd(cmd)

    def status_led(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.status_led()
        return self.__execute_single_cmd(cmd)

    def game_mode_status(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.game_mode_status()
        return self.__execute_single_cmd(cmd)

    def game_mode(self, enter_gamemode: bool) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.game_mode(enter_gamemode=enter_gamemode)
        return self.__execute_single_cmd(cmd)

    def power_status(self) -> Optional[GenericCommand]:
        cmd: GenericCommand = CommandBuilder.power_status()
        return self.__execute_single_cmd(cmd)
