import logging
import threading
import typing as _tp
from queue import Empty, Queue
from time import perf_counter, sleep

import tomllib_python311
from rslm._default_hw_config import HARDWARE_CONFIG_SECTION
from rslm._hw_interface import BaseHardwareSimulatorClass, HWInterface, execute_command
from rslm._sigint_handling import SIGINT_RECEIVED
from rslm.led_command_lib import (
    ConsecutiveCommands,
    GenericCommand,
    cmd_carbon_copy,
    configure_generic_commands,
)

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)
LOGGER_DEBUG_ENABLED: bool = LOGGER.isEnabledFor(logging.DEBUG)
LOGGER_INFO_ENABLED: bool = LOGGER.isEnabledFor(logging.INFO)

# It has shown that when using the HW this driver outpaces the capabilities of the HW-controller
# this flag sets an execution delay in seconds after each command
EXECUTION_DELAY: float = 0.05


class CmdExecutor:
    """The ```CmdExecutor``` handles the execution of ```GenericCommands``` on the LED hardware.
    Basically it is a thread with an input queue and awaits elements to execute.
    To adapt this class to other hardware setups exchange the interfacing
    class and execution _tp.Callable.
    """

    def __init__(
        self,
        hw_interface: _tp.Optional[BaseHardwareSimulatorClass] = None,
        hw_response_callable: _tp.Optional[_tp.Callable] = None,
        daemon_mode: bool = False,
        config_file: str = "./hw_config.toml",
    ):
        """CmdExecutor constructor
        # Parmaters:
        hw_interface:
            class which interfaces with the LED hardware. If None, ```HWInterface``` is used.

        hw_response_callable:
            a _tp.Callable that takes a ```BaseHardwareSimulatorClass```, commands,
            and, an integer as input to execute passed ```GenericCommand```s.

        daemon_mode:
            a boolean flag. If ```True``` threads are started as deamon.

        config_file:
            path to the hardware configuration toml file as string
        """
        configure_generic_commands(config_file=config_file)

        self.hw: BaseHardwareSimulatorClass = (
            hw_interface
            if hw_interface
            else HWInterface(
                tomllib_python311.load(open(config_file, "rb"))[  # type: ignore
                    HARDWARE_CONFIG_SECTION
                ]
            )
        )
        """the LED hardware low-level-ish interfacing class.
        Take a look at ```HWInterface``` for the default implementation
        """

        self.__hw_response_callable: _tp.Callable = (
            hw_response_callable if hw_response_callable else execute_command
        )
        """interfacing function used to execute ```GenericCommand```s."""

        self.__input_queue: Queue[
            _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ] = Queue()
        """the Queue where commands to be executed are placed into"""

        self.__interrupt_queue: Queue[GenericCommand] = Queue()
        """items in this queue will be executed as soon as possible possibly interupting a macro
        and inserting the command in between concerning execution order"""

        # self.output_queue: Queue[GenericCommand] = Queue()
        self.__thread: _tp.Optional[threading.Thread] = None
        """the thread which handles execution of elements in the Queue"""

        self.__running: bool = False
        """boolean flag indicating if the ```CmdExecutor``` is considered running"""

        self.__processing_done: threading.Event = threading.Event()
        """used internally by the execution thread to indicate
        if endless processing loop has been exited"""

        self.__is_deamon: bool = daemon_mode
        """boolean flag indicating if threads shall be started in daemon mode"""

    def __execute_interrupt_cmds(self) -> None:
        while True:
            try:
                cmd: GenericCommand = self.__interrupt_queue.get_nowait()
            except Empty:
                return
            LOGGER.warning(
                "Encountered interrupt. Executing the following command out of order\n\t%s",
                str(cmd),
            )
            self.__execute_commands(cmd)

    def __execute_commands(
        self, cmds: _tp.Union[_tp.List[GenericCommand], GenericCommand]
    ) -> None:
        """command handling the execution of a single ```GenericCommand```.
        # Parameters:
        cmd:
            the ```GenericCommand```to execute

        # Raises:
            * forwards all raised exceptions to caller.
        """
        self.__execute_interrupt_cmds()
        if SIGINT_RECEIVED.is_set():
            LOGGER.critical("Received SIGINT. Now terminating.")
            return

        if isinstance(cmds, GenericCommand):
            if cmds.timed_out.is_set():
                # TODO implement timeout behaviour
                return
            if LOGGER_INFO_ENABLED:
                LOGGER.debug("Now processing command %s: '%s'", cmds.uuid, str(cmds))
            try:
                self.__hw_response_callable(self.hw, cmds)
            except KeyboardInterrupt as _k:
                raise _k
            except Exception as e:
                raise e
            with cmds.lock:
                cmds.lock.notify()
        elif isinstance(cmds, _tp.List):
            for _cmd in cmds:
                if isinstance(_cmd, GenericCommand):
                    return self.__execute_commands(_cmd)
                elif isinstance(_cmd, _tp.List):
                    self.__execute_commands(_cmd)
        else:
            raise TypeError(
                "Only GenericCommand and _tp.List[GenericCommand] are supported. Received %s instead.",
                type(cmds),
            )

    def __generic_command_processing_loop(self) -> None:
        """this method implements the command execution loop which is run in a seperate thread"""
        while self.__running:
            try:
                self.__execute_interrupt_cmds()
                cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]] = (
                    self.__input_queue.get(block=True, timeout=0.1)
                )
                if isinstance(cmd, GenericCommand) or isinstance(cmd, _tp.List):
                    self.__execute_commands(cmd)
                    sleep(EXECUTION_DELAY)
                else:
                    LOGGER.critical(
                        "Only GenericCommands and _tp.List[GenericCommand] are supported. Instead %s was received.",
                        type(cmd),
                    )
                    continue
                if SIGINT_RECEIVED.is_set():
                    LOGGER.error("Encountered KeyboardInterrupt. Now shutting down.")
                    self.__running = False
                    self.__processing_done.set()
                    self.stop_processing_thread()
                    return
            except Empty:
                pass
            except KeyboardInterrupt as _k:
                LOGGER.error("Encountered KeyboardInterrupt. Now shutting down.")
                self.__running = False
                self.__processing_done.set()
                self.stop_processing_thread()
                raise _k
            except AssertionError:
                LOGGER.critical(
                    "Encountered too many AssertionErrors. Now shutting down."
                )
                self.__running = False
                self.__processing_done.set()
                self.stop_processing_thread()
            except Exception as _e:
                LOGGER.exception("Encountered unexpected Exception. Now shutting down.")
                self.__running = False
                self.__processing_done.set()
                self.stop_processing_thread()
                raise _e
        self.__processing_done.set()

    def start_processing_thread(self) -> None:
        """this method starts the thread which handles the execution of ```GenericCommand```s."""
        try:
            self.__running = True
            self.__processing_done.clear()
            self.__thread = threading.Thread(
                target=self.__generic_command_processing_loop
            )
            self.__thread.setDaemon(self.__is_deamon)
            self.__thread.start()
        except KeyboardInterrupt as _k:
            LOGGER.error("Encountered KeyboardInterrupt. Now shutting down.")
            raise _k
        except Exception as _e:
            LOGGER.exception("Encountered unexpected Exception. Now shutting down.")
            self.stop_processing_thread()
            raise _e

    def stop_processing_thread(self) -> None:
        """stops the execution thread by waiting for the current execution to complete,
        flushing all Queue content and joining the thread.
        """
        try:
            if LOGGER_DEBUG_ENABLED:
                LOGGER.debug("Killing CommandExecutor in 2 seconds")
            sleep(2)
            self.hw.close_port()
            if self.__is_deamon or self.__running is False:
                return
            if LOGGER_DEBUG_ENABLED:
                LOGGER.debug("Now killing CommandExecutor")

            self.__running = False
            # wait for last cmd to finish processing
            self.__processing_done.wait()
            if self.__thread:
                self.__thread.join()
                self.__thread = None
            # flush input queue
            self.__input_queue = Queue()
        except KeyboardInterrupt as _k:
            LOGGER.critical("Encountered KeyboardInterrupt during shutdown.")
            LOGGER.critical("Proceeding to shutdown safely.")
            self.__running = False
            self.__processing_done.set()
            if self.__thread:
                self.__thread.join()
                self.__thread = None
            self.__input_queue = Queue()
            raise _k
        except Exception as _e:
            LOGGER.exception("Encountered unexpected Exception.")
            raise _e

    def is_running(self) -> bool:
        """returns True if the ```CmdExecutor``` is running."""
        return True if self.__running else False

    def interrupt(self, cmd: GenericCommand) -> None:
        self.__interrupt_queue.put(cmd)

    def queue(self, cmds: _tp.Union[_tp.List[GenericCommand], GenericCommand]) -> None:
        self.__input_queue.put(cmds)

    def reset_queues(self) -> None:
        self.__input_queue = Queue()
        self.__interrupt_queue = Queue()

    def set_log_handler_callable(self, func: _tp.Callable) -> None:
        self.hw.communications_logger.set_handler(func)

    def reset_log_handler_callable(self) -> None:
        self.hw.communications_logger.reset_handler()


def execute_command_w_logging(
    processor: CmdExecutor,
    _cmds: _tp.Union[GenericCommand, _tp.List[GenericCommand]],
    timeout_override: _tp.Optional[int] = None,
) -> None:
    """helper method which simplifies the execution
    of ```GenericCommand```s using a ```CommandExecutor```.
    # Parameters:
    processor:
        the ```CmdExecutor``` to use when executing a ```GenericCommand```.

    cmd:
        a single or a recursive _tp.Sequence of ```GenericCommand```s to execute

    # Raises:
        all raised Exceptions are forwarded to the caller.
    """

    def __execute(_cmds: _tp.Union[GenericCommand, _tp.List[GenericCommand]]):
        _timeout: int = max(
            int(2 * processor.hw.read_timeout), int(2 * processor.hw.write_timeout)
        )
        if timeout_override is not None:
            _timeout = timeout_override

        if isinstance(_cmds, _tp.List):
            for _ in _cmds:
                if SIGINT_RECEIVED.is_set():
                    return
                __execute(_)
            return

        if not isinstance(_cmds, GenericCommand):
            raise TypeError(
                "Only GenericCommands and _tp.List[GenericCommand] are supported. Got: %s",
                type(_cmds),
            )

        if not processor.is_running():
            raise RuntimeError("CommandExecutor is not running.")

        # TODO: what if the following happens:
        """
            cmd is put in queue
            tec status (takes 20 seconds) is called
            this cmd will timeout
            error state is assumed altough no error is there
        """
        with _cmds.lock:
            # processor.__input_queue.put(_cmds)
            LOGGER.info("Command '%s' added to execution queue", str(_cmds))
            processor.queue(_cmds)
            timeout_occured: bool = not _cmds.lock.wait(timeout=_timeout)

        if not timeout_occured:
            if LOGGER_INFO_ENABLED:
                LOGGER.debug(
                    """
    Command execution successful.
    ID: %s
    Command: '%s'
    Result: '%s'""",
                    _cmds.uuid,
                    str(_cmds),
                    _cmds.get_cmd_result_as_str(),
                )
        else:
            LOGGER.error(
                """
    Command execution timed out.
    ID: %s
    Command: '%s'""",
                _cmds.uuid,
                str(_cmds),
            )
            _cmds.timed_out.set()
        return

    try:
        if SIGINT_RECEIVED.is_set():
            return
        __execute(_cmds)
    except KeyboardInterrupt as _k:
        raise _k
    except Exception as e:
        LOGGER.exception("Encountered unexpected Exception.")
        raise e


def execute_cmd(
    executor: CmdExecutor, cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]]
) -> None:
    if not SIGINT_RECEIVED.is_set():
        execute_command_w_logging(processor=executor, _cmds=cmd)


class CommandPlayer:
    """
    This class is to be used in combination with a ```CmdExecutor``` to execute ```GenericCommands```
    like songs in a playlist.

    TODO: document
    """

    def __init__(
        self,
        cmds: _tp.Sequence[
            _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ],  # the commands to play
        executor: CmdExecutor,  # the CmdExecutor to use to play commands
        repeat: bool = True,  # wether to repeat the passed playlist
        retries: int = 2,  # number of retries on failed command/command-groups
    ) -> None:
        self.playlist: _tp.Sequence[
            _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ] = cmds
        """commands to play as nested _tp.List"""

        self.player: CmdExecutor = executor
        """the executor which interfaces with the hardware"""

        self.repeat: bool = repeat
        """if the commands should be repeated or not"""

        self.next_track: _tp.Optional[
            _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ] = (None if not cmds else self.playlist[0])
        """"the track which is going to be played next"""

        self.next_track_id: int = 0
        """the index of the next track; strating at 0"""

        self.timer: _tp.Optional[threading.Timer] = None
        """the timer object which leads to track execution"""

        self.__timer_start_time: _tp.Optional[float] = None
        """time as float when the last timer was started"""

        self.__delay_override: _tp.List[float] = []
        """stores how much time was left on the timer if player was paused in last position of _tp.List"""

        self.retries: int = retries
        """number of retries when a track fails to play"""

        self.__playing: bool = False
        """stores true if the player is currently running"""

        self.__paused: bool = False
        """stores true if the player is currently paused"""

        self.__stop_event: threading.Event = threading.Event()

        self.prev_results: _tp.Optional[
            _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ] = None

    def __execute_cmd(self) -> None:
        def __check_if_execution_failed(
            _cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ) -> bool:
            try:
                if isinstance(_cmd, GenericCommand):
                    assert (
                        _cmd.execution_successful()
                    ), f"Execution of '{str(_cmd)}' unsuccessful"
                elif isinstance(_cmd, _tp.List):
                    for _ in _cmd:
                        if __check_if_execution_failed(_):
                            return True
                else:
                    raise TypeError()
            except AssertionError:
                return True
            except Exception as e:
                LOGGER.exception(e)
                return True
            return False

        def __execute(
            _player: CmdExecutor,
            _cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]],
        ) -> bool:

            def __recursive_execute(
                _c: _tp.Union[GenericCommand, _tp.List[GenericCommand]]
            ):
                if isinstance(_c, list):
                    for _ in _c:
                        __recursive_execute(_)
                    return

                assert isinstance(_c, GenericCommand)
                if self.__stop_event.is_set():
                    return False
                execute_command_w_logging(processor=_player, _cmds=_c)

            cmd_copy: _tp.Union[
                GenericCommand, _tp.List[GenericCommand]
            ] = cmd_carbon_copy(
                _cmd
            )  # type: ignore # reset object if it was already previously executed
            try:
                __recursive_execute(cmd_copy)
            except RuntimeError as e:
                LOGGER.exception(e)
                self.pause()
            self.prev_results = cmd_copy  # type: ignore
            if self.__stop_event.is_set():
                return False
            return __check_if_execution_failed(_cmd=cmd_copy)  # type: ignore

        if self.next_track is None:
            self.stop()

        failure_status: bool = __execute(self.player, self.next_track)  # type: ignore

        if not failure_status:
            self.next()
            return

        for _ in range(self.retries):
            LOGGER.critical("CommandPlayer unable to execute next track. Retrying.")
            if not __execute(self.player, self.next_track):  # type: ignore
                self.next()
                return

        LOGGER.critical("CommandPlayer unable to execute tracks. Now stopping.")

        self.stop()

        # TODO implement error handling and notifications

    def __start_timer(self, delay_override: _tp.Optional[float] = None) -> None:
        if not self.next_track:
            self.stop()
            return
        self.timer = threading.Timer(
            interval=(
                delay_override
                if delay_override
                else CommandPlayer.__get_delay_of_cmd(self.next_track)
            ),
            function=self.__execute_cmd,
        )
        self.__timer_start_time = perf_counter()
        # self.__delay_override = []
        self.timer.start()

    def play(self) -> None:
        self.__stop_event.clear()
        if not self.playlist:
            self.stop()
            return

        if self.__playing:
            self.__paused = False
            return

        if not self.next_track:
            self.next_track = self.playlist[0]
            self.next_track_id = 0

        self.__playing = True
        self.__paused = False
        self.__start_timer()

    def get_current_track_duration(self) -> float:
        if self.next_track is None:
            return -1
        return CommandPlayer.__get_delay_of_cmd(self.next_track)

    def get_progressed_time(self) -> float:
        assert self.next_track
        _t: float = 0.0
        if self.__timer_start_time:
            _t += perf_counter() - self.__timer_start_time

        if self.__delay_override:
            assert self.next_track
            _d: float = CommandPlayer.__get_delay_of_cmd(self.next_track)
            for _ in self.__delay_override:
                _t += _d - _
                _d = _

        return _t

    def pause(self) -> None:
        if not self.__playing:  # player is paused
            self.__playing = True
            self.__paused = False
            self.__start_timer(self.__delay_override[-1])
            self.__stop_event.clear()
        else:  # player is running
            self.__playing = False
            self.__paused = True
            assert self.__timer_start_time
            assert self.next_track
            self.__delay_override.append(
                CommandPlayer.__get_delay_of_cmd(self.next_track)
                - self.get_progressed_time()
            )
            if self.timer:
                self.timer.cancel()
            self.__timer_start_time = None
            self.timer = None

    def stop(self) -> None:
        if self.timer:
            self.timer.cancel()
            self.timer = None
        self.__stop_event.set()
        LOGGER.critical("Stop Event received.")
        self.__playing = False
        self.__paused = False
        self.next_track = None if not self.playlist else self.playlist[0]
        self.next_track_id = 0
        self.__delay_override = []
        self.__timer_start_time = None
        self.prev_results = None

    def next(self) -> None:
        self.__delay_override = []
        if not self.__playing:
            if (
                self.next_track_id is None
                or self.next_track_id >= len(self.playlist) - 1
            ):
                self.next_track_id = 0
            else:
                self.next_track_id += 1

            self.next_track = self.playlist[self.next_track_id]
            return

        if self.timer:
            self.timer.cancel()
            self.timer = None

        if self.next_track_id >= len(self.playlist) - 1:
            self.next_track_id = 0
            if not self.repeat:  # exit when playlist is over
                self.stop()
                return
        else:
            self.next_track_id += 1

        self.next_track = self.playlist[self.next_track_id]

        self.__start_timer()

    def previous(self) -> None:
        """
        if player is playing, current timer is stopped and a new timer for the previous track is
        started. In the case that the first track was currently playing, the last track is started.
        If, however, repeat is false, the player is stopped.
        """
        if not self.__playing:
            if self.next_track_id is None or self.next_track_id <= 0:
                self.next_track_id = len(self.playlist) - 1
            else:
                self.next_track_id -= 1

            self.next_track = self.playlist[self.next_track_id]
            return

        if self.timer:
            self.timer.cancel()
            self.timer = None

        if self.next_track_id >= len(self.playlist):
            self.next_track_id = len(self.playlist) - 1
            if self.repeat:  # exit when playlist is over
                self.stop()
                return
        else:
            self.next_track_id -= 1

        self.playlist[self.next_track_id]

        self.__start_timer()

    def toggle_repeat(self) -> bool:
        self.repeat = not self.repeat
        return self.repeat

    def is_repeating(self) -> bool:
        return self.repeat

    def next_command(self) -> _tp.Union[GenericCommand, _tp.List[GenericCommand]]:
        """
        returns the next track and its index in the playlist
        """
        return cmd_carbon_copy(self.next_track)  # type: ignore

    def next_command_idx(self) -> int:
        """
        returns the next track and its index in the playlist
        """
        return self.next_track_id

    def is_playing(self) -> bool:
        return True if self.__playing else False

    def is_paused(self) -> bool:
        return True if self.__paused else False

    @staticmethod
    def __get_delay_of_cmd(
        cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]]
    ) -> float:
        """
        Returns time in seconds to wait before executing a command.
        Commands which are grouped and need to be executed directly one after the other are
        expected to be stored in a _tp.List. The delay of the (recursively) last element in said _tp.List
        is used.
        """

        def __multiple(
            _cmd: _tp.Union[GenericCommand, _tp.List[GenericCommand]]
        ) -> float:
            if isinstance(_cmd, GenericCommand):
                return _cmd.delay_seconds
            elif isinstance(_cmd, ConsecutiveCommands):
                return _cmd.get_delay_seconds()
            elif isinstance(_cmd, _tp.List):
                return __multiple(_cmd[-1])
            raise AssertionError(
                f"Expected GenericCommand or _tp.List of GenericCommands and not '{type(_cmd)}'."
            )

        return __multiple(cmd)

    def get_playlist(
        self,
    ) -> _tp.Sequence[_tp.Union[GenericCommand, ConsecutiveCommands]]:
        return cmd_carbon_copy(self.playlist)  # type: ignore

    def set_playlist(
        self,
        playlist: _tp.Sequence[_tp.Union[GenericCommand, _tp.List[GenericCommand]]],
    ) -> None:
        self.playlist = playlist
        self.stop()

    def interrupt(self, cmd: GenericCommand) -> None:
        """execute a command as soon as possible"""
        assert self.player.is_running()
        assert cmd is not None
        assert isinstance(cmd, GenericCommand)
        self.player.interrupt(cmd)

    def set_log_handler_callable(
        self, func: _tp.Callable[..., _tp.Awaitable[_tp.Any]]
    ) -> None:
        self.player.set_log_handler_callable(func)

    def reset_log_handler_callable(self) -> None:
        self.player.reset_log_handler_callable()
