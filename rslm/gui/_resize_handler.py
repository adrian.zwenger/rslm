import uuid
from inspect import iscoroutinefunction
from typing import Callable, Dict, Optional, Union

import flet as flt

__RESIZE_HANDLERS: Dict[Union[str, uuid.UUID], Callable] = {}
_FLET_PAGE: Optional[flt.Page]


def add_resize_handler(event_id: Union[str, uuid.UUID], handler: Callable) -> None:
    global __RESIZE_HANDLERS
    global _FLET_PAGE
    assert _FLET_PAGE
    if iscoroutinefunction(handler):
        __RESIZE_HANDLERS[event_id] = handler
    else:
        raise ValueError()


def remove_resize_handler(event_id: Union[str, uuid.UUID]) -> None:
    global __RESIZE_HANDLERS
    if event_id in __RESIZE_HANDLERS:
        del __RESIZE_HANDLERS[event_id]
    return


async def global_resize_handler(e: flt.ControlEvent) -> None:
    global __RESIZE_HANDLERS
    global _FLET_PAGE
    assert _FLET_PAGE
    for _, handler in __RESIZE_HANDLERS.items():
        try:
            await handler(e)
        except (
            AssertionError
        ):  # skip control handlers for components that are not mounted
            pass

    return await _FLET_PAGE.update_async()
