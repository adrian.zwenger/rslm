import asyncio
import logging
import sys
import typing as _tp
import uuid
from importlib.metadata import version as _get_pkg_version
from typing import Optional

import flet as flt
import flet.fastapi as flet_fastapi
import uvicorn
from fastapi import APIRouter

import rslm.gui._resize_handler as GlobalResizeHandler
import rslm.gui.backend as BACKEND
import rslm.gui.rest_api as rest_api
import rslm.gui.routes as routes
from rslm._sigint_handling import register as _register_sigint_handler
from rslm.gui.routes import (
    APP_ROOT_ROUTE,
    UPLOAD_DIR,
    change_route,
    exit_view,
    register_routes,
)

REQUIRED_FLET_VERSION: str = "0.22.0"

assert _get_pkg_version("flet") == REQUIRED_FLET_VERSION, (
    f"RSLM requires flet=={REQUIRED_FLET_VERSION} as a dependency."
    + "Please install it with the following command:\n\t"
    + f"'pip install --force-reinstall -v flet=={REQUIRED_FLET_VERSION}'"
)

LOGGER = logging.getLogger(__name__)

SECRET_KEY: str = str(uuid.uuid4())


async def build_app(page: flt.Page) -> None:
    await register_routes(page)
    page.on_route_change = change_route
    await page.go_async(APP_ROOT_ROUTE)


async def configure_flet_app(flt_page: _tp.Union[flt.Page, flt.ControlEvent]) -> None:
    if isinstance(flt_page, flt.Page):
        page: flt.Page = flt_page
    elif isinstance(flt_page, flt.ControlEvent):
        page: flt.Page = flt_page.page
    else:
        raise ValueError()

    page.title = "RSLM (Refrigerator-Sized LED Manager)"
    page.theme_mode = flt.ThemeMode.DARK
    GlobalResizeHandler._FLET_PAGE = page
    page.expert_mode: bool = False  # type: ignore as the attribute does not exist yet
    page.on_resize = GlobalResizeHandler.global_resize_handler
    page.on_view_pop = exit_view
    await build_app(page)


async def _release_backend(flt_page: _tp.Union[flt.Page, flt.ControlEvent]) -> None:
    if not BACKEND.LED_BACKEND:
        return

    if isinstance(flt_page, flt.Page):
        page: flt.Page = flt_page
    elif isinstance(flt_page, flt.ControlEvent):
        page: flt.Page = flt_page.page
    else:
        raise ValueError()

    await BACKEND.LED_BACKEND.release(session_id=page.session_id)


async def _aquire_backend(page: flt.Page) -> bool:
    # TODO: block until backend is not used by another process
    assert BACKEND.LED_BACKEND

    if await BACKEND.LED_BACKEND.aquire(session_id=page.session_id):
        return True

    if page.controls:
        page.controls.clear()

    async def __retry(e: flt.ControlEvent):
        if await _aquire_backend(e.page):
            return await configure_flet_app(e.page)

    page.add(
        flt.Column(
            [
                flt.Text(
                    "Unable to connect to the backend as it is being used by somebody else. Try again?",
                    selectable=True,
                ),
                flt.ElevatedButton(text="Yes", on_click=__retry),
            ],
            horizontal_alignment=flt.CrossAxisAlignment.START,
            expand=True,
        ),
    )
    return False


def configure_backend(
    config_file_path: str = "./hw_config.toml",
    playlist_xml: str = "./default_playlist.xml",
    simulation: bool = True,
) -> None:
    if not BACKEND.LED_BACKEND:
        BACKEND.configure_backend(
            config_file=config_file_path,
            simulate_hardware=simulation,
            playlist_xml=playlist_xml,
        )


async def init_backend(page: flt.Page) -> None:
    assert BACKEND.LED_BACKEND, "No backend was configured."
    if BACKEND.LED_BACKEND.simulation_mode:
        return
    page.add(
        flt.Markdown("# Configuring hardware, please wait..."),
        flt.ProgressRing(),
        # flet bug? if not added at the same time second element will not be rendered
    )
    await asyncio.sleep(
        2
    )  # flet bug? when not waiting on linux, page content is not rendered during init
    page.run_task(BACKEND.initialize_hardware)
    if page.controls:
        page.controls.clear()


async def __main(page: flt.Page) -> None:
    page.on_close = _release_backend
    page.on_disconnect = _release_backend
    page.on_logout = _release_backend
    _register_sigint_handler()  # if not explicitly called, flet overrides the global sigint handler
    await init_backend(page)
    backend_aquired: bool = await _aquire_backend(page)
    if backend_aquired:
        await configure_flet_app(page)


def start_gui_as_standalone(
    config_file: str = "./hw_config.toml",
    simulation: bool = True,
) -> None:
    configure_backend(config_file_path=config_file, simulation=simulation)
    try:
        routes.runs_as_webapp = False
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            future=flt.app_async(target=__main, upload_dir=UPLOAD_DIR)  # type: ignore
        )
        if BACKEND.LED_BACKEND:
            BACKEND.kill_backend()
    except KeyboardInterrupt:
        print("Received KeyboardInterrupt. Now shutting down.")
        if BACKEND.LED_BACKEND:
            BACKEND.kill_backend()
        sys.exit(-1)


def start_as_webserver(
    host: Optional[str] = None,
    port: int = 8001,
    config_file: str = "./hw_config.toml",
    simulation: bool = True,
) -> None:
    configure_backend(config_file, simulation=simulation)
    try:
        routes.runs_as_webapp = True
        app: APIRouter = rest_api.api

        flutter_app = flet_fastapi.app(
            __main, upload_dir=UPLOAD_DIR, secret_key=SECRET_KEY
        )
        app.mount("/gui/", flutter_app)

        if host:
            uvicorn.run(app, host=host, port=port)
        else:
            uvicorn.run(app, port=port)

        if BACKEND.LED_BACKEND:
            BACKEND.kill_backend()

    except KeyboardInterrupt:
        print("Received KeyboardInterrupt. Now shutting down.")
        if BACKEND.LED_BACKEND:
            BACKEND.kill_backend()
        sys.exit(-1)
