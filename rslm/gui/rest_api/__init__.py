from fastapi import APIRouter as _APIRouter

import rslm.gui.rest_api._main as _main

api: _APIRouter = _main.router
