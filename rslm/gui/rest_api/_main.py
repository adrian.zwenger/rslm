from fastapi import APIRouter as _APIRouter
from fastapi.responses import RedirectResponse as _RedirectResponse

BASE_ROUTE: str = "/api"


router = _APIRouter()


@router.get("/")
async def redirect_to_gui() -> _RedirectResponse:
    return _RedirectResponse(url="/gui")


@router.get(f"{BASE_ROUTE}")
async def hello_world() -> str:
    return "Hello World!"
