import asyncio as _asyncio
import os as _os
import typing as _tp

from rslm.gui.backend._hw_interface import LedBackend as _LedBackend
from rslm.led_command_lib import deserialize_generic_command_program
from rslm.macros import InitLeds as _InitLeds

LED_BACKEND: _tp.Optional[_LedBackend] = None
_IS_INITIALIZED: _asyncio.Event = _asyncio.Event()
_INIT_IS_RUNNING: _asyncio.Event = _asyncio.Event()


def configure_backend(
    config_file: str = "./hw_config.toml",
    simulate_hardware: bool = True,
    playlist_xml: _tp.Optional[str] = None,
) -> None:
    global LED_BACKEND
    if not LED_BACKEND:
        LED_BACKEND = _LedBackend(
            config_file=config_file, simulate_hardware=simulate_hardware
        )
        LED_BACKEND.cmd_executor.start_processing_thread()

        if not playlist_xml:
            return

        if not _os.path.exists(playlist_xml):
            return

        _playlist = deserialize_generic_command_program(playlist_xml)
        LED_BACKEND._set_playlist(playlist=_playlist)  # type: ignore


async def initialize_hardware() -> None:
    global LED_BACKEND, _IS_INITIALIZED
    assert LED_BACKEND, "HW was not configured. Configure it first."
    if _IS_INITIALIZED.is_set():
        while _IS_INITIALIZED.is_set():
            await _asyncio.sleep(0.25)

    if not _IS_INITIALIZED.is_set():
        _INIT_IS_RUNNING.set()
        LED_BACKEND._execute_cmd(cmd=_InitLeds(preconstruct=True))
        _IS_INITIALIZED.set()
        _INIT_IS_RUNNING.clear()


def kill_backend() -> None:
    global LED_BACKEND
    if LED_BACKEND:
        LED_BACKEND.cmd_player.stop()
        LED_BACKEND.cmd_executor.stop_processing_thread()
        LED_BACKEND = None
