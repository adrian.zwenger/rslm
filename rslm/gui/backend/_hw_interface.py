import asyncio as _asyncio
import logging
import typing as _tp
from concurrent.futures import ThreadPoolExecutor as _ThreadPoolExecutor

import tomllib_python311
from rslm._default_hw_config import HARDWARE_CONFIG_SECTION
from rslm._executor import CmdExecutor, CommandPlayer, execute_cmd
from rslm._hw_interface import GenericHardwareSimulator, hw_response_simulator
from rslm._sigint_handling import SIGINT_RECEIVED
from rslm.led_command_lib import (
    ConsecutiveCommands,
    GenericCommand,
    create_dummy_command_from_string,
)
from rslm.led_command_lib.tec import TecStatus
from rslm.macros import InitLeds

logging.getLogger(__name__).addHandler(logging.NullHandler())
LOGGER = logging.getLogger(__name__)

TEC_STATUS_CMD: TecStatus.__class__ = TecStatus
TEC_STATUS_SAMPLING_TIME: float = 3.0
# in multiples of max(serieal read timeout, serial max timeout)

# TODO error state handling (self.error_state.set())


_pool: _ThreadPoolExecutor = _ThreadPoolExecutor()


class LedBackend:
    def __init__(
        self,
        config_file: str = "./hw_config.toml",  # fqdn of the LED hardware config
        simulate_hardware: bool = True,
    ) -> None:
        # configure_generic_commands(config_file=config_file)
        self.simulation_mode: bool = simulate_hardware
        self.hw_config: _tp.Dict[str, _tp.Any] = tomllib_python311.load(
            open(config_file, "rb")
        )
        self.cmd_executor: CmdExecutor = CmdExecutor(
            hw_interface=(
                GenericHardwareSimulator(
                    config_dict=self.hw_config[HARDWARE_CONFIG_SECTION]
                )
                if simulate_hardware
                else None
            ),
            hw_response_callable=hw_response_simulator if simulate_hardware else None,
            config_file=config_file,
        )
        self.async_lock: _asyncio.Lock = _asyncio.Lock()
        self.session_id: _tp.Optional[str] = None
        self.cmd_player: CommandPlayer = CommandPlayer(
            cmds=[], executor=self.cmd_executor
        )

        self.monitor_result: _tp.Optional[TecStatus] = None

        self.error_state: _asyncio.Event = _asyncio.Event()

        _asyncio.get_event_loop().create_task(self._monitoring_loop())

    def get_monitoring_result(self) -> _tp.Optional[_tp.Dict[str, _tp.Any]]:
        if not self.monitor_result:
            return {}
        return self.monitor_result.get_cmd_result_as_dict()

    async def __execute_monitoring_cmd(self) -> None:
        result_obj: GenericCommand = TEC_STATUS_CMD()
        await self.execute_interrupting_cmd(result_obj)
        if not result_obj.timed_out.is_set():
            self.monitor_result = result_obj
            return

        # self.error_state.set()
        # if self.cmd_player.is_playing():
        #     self.cmd_player.pause()

    async def _monitoring_loop(self) -> None:
        _t: int = int(
            TEC_STATUS_SAMPLING_TIME
            * max(self.cmd_executor.hw.read_timeout, self.cmd_executor.hw.write_timeout)
        )
        await _asyncio.sleep(3)
        while True:
            await self.__execute_monitoring_cmd()
            for _ in range(5):
                await _asyncio.sleep(_t // 5)
                if self.error_state.is_set():
                    LOGGER.critical("BACKEND HAS ENTERED ERROR STATE.")
                    return
                if SIGINT_RECEIVED.is_set():
                    return

    async def aquire(self, session_id: str) -> bool:
        if session_id == self.session_id:
            return True
        if not self.async_lock.locked():
            self.session_id = session_id
            await self.async_lock.acquire()
            LOGGER.info("session: %s aquired lock", session_id)
            return True
        return False

    async def release(self, session_id: str) -> bool:
        if session_id != self.session_id:
            return False

        LOGGER.info("session: %s releasing lock", session_id)

        self.session_id = None
        if not self.async_lock.locked():
            return True
        self.async_lock.release()
        return True

    async def _force_release(self) -> None:
        self.session_id = None
        if not self.async_lock.locked():
            return
        self.async_lock.release()

    async def play(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if not self.session_id == session_id:
            return
        if self.cmd_player.is_paused():
            self.cmd_player.pause()
            return
        self.cmd_player.play()

    async def pause(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if self.session_id == session_id:
            self.cmd_player.pause()

    async def init_leds(self) -> None:
        def _():
            LOGGER.info("Proceeding to initialize LEDs via seperate ThreadPool")
            self._execute_cmd(cmd=InitLeds(preconstruct=True))

        await _asyncio.get_event_loop().run_in_executor(_pool, _)

    async def stop(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if not self.session_id == session_id:
            return
        self.cmd_player.stop()
        self.cmd_executor.reset_queues()

    async def next(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if self.session_id == session_id:
            self.cmd_player.next()

    async def toggle_repeat(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if self.session_id == session_id:
            self.cmd_player.toggle_repeat()

    async def previous(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        if self.session_id == session_id:
            self.cmd_player.previous()

    async def get_playlist(
        self, session_id: str
    ) -> _tp.Sequence[_tp.Union[GenericCommand, ConsecutiveCommands]]:
        if self.error_state.is_set():
            return []
        if session_id == self.session_id:
            return self.cmd_player.get_playlist()
        return []

    async def set_playlist(
        self,
        session_id: str,
        playlist: _tp.Sequence[_tp.Union[GenericCommand, ConsecutiveCommands]],
    ) -> bool:
        if self.error_state.is_set():
            return False
        if session_id != self.session_id:
            return False
        self.cmd_player.set_playlist(playlist)
        return True

    async def reset(self, session_id: str) -> None:
        if self.error_state.is_set():
            return
        await self.stop(session_id=session_id)

    async def execute_cmd(self, session_id: str, cmd: GenericCommand) -> bool:
        if self.error_state.is_set():
            return False
        if self.session_id != session_id:
            return False
        execute_cmd(cmd=cmd, executor=self.cmd_executor)
        return True

    def _execute_cmd(self, cmd: _tp.Union[GenericCommand, ConsecutiveCommands]):
        """
        Regular users please do not use this!! It is used internally on startup
        """
        if self.error_state.is_set():
            return

        execute_cmd(cmd=cmd, executor=self.cmd_executor)

    def _set_playlist(
        self,
        playlist: _tp.Sequence[_tp.Union[GenericCommand, ConsecutiveCommands]],
    ) -> None:
        if self.error_state.is_set():
            return
        self.cmd_player.set_playlist(playlist)

    async def next_track_idx(self) -> int:
        if self.error_state.is_set():
            return -1
        return self.cmd_player.next_command_idx()

    def is_playing(self) -> bool:
        if self.error_state.is_set():
            return False
        return self.cmd_player.is_playing()

    def is_paused(self) -> bool:
        if self.error_state.is_set():
            return True
        return self.cmd_player.is_paused()

    def is_repeating(self) -> bool:
        return self.cmd_player.is_repeating()

    def get_progressed_time(self) -> float:
        if self.error_state.is_set():
            return -1.0
        return self.cmd_player.get_progressed_time()

    def get_cmd_delay(self) -> float:
        if self.error_state.is_set():
            return -1.0
        return self.cmd_player.get_current_track_duration()

    def is_simulating(self) -> bool:
        return self.simulation_mode

    async def attempt_reconnect(self) -> bool:
        if not self.error_state.is_set():
            return True
        self.error_state.clear()
        await self.__execute_monitoring_cmd()
        return self.error_state.is_set()

    async def execute_interrupting_cmd(
        self, cmd_obj: _tp.Union[str, GenericCommand]
    ) -> str:
        assert self.cmd_executor
        if isinstance(cmd_obj, str):
            cmd: GenericCommand = create_dummy_command_from_string(cmd_obj)
        else:
            cmd = cmd_obj
        _timeout: int = max(
            int(1.7 * self.cmd_executor.hw.read_timeout),
            int(1.7 * self.cmd_executor.hw.write_timeout),
        )

        def _() -> bool:
            # return cmd.lock.acquire(timeout=_timeout)
            with cmd.lock:
                self.cmd_executor.interrupt(cmd)
                timeout_occured: bool = not cmd.lock.wait(timeout=_timeout)
            return timeout_occured

        timeout_occured = await _asyncio.get_event_loop().run_in_executor(_pool, _)

        return (
            "timeout occured"
            if timeout_occured
            else cmd.get_cmd_result_as_dict()["msg"]  # type: ignore
        )
