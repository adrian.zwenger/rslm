import logging
import os
import shutil
from typing import Callable, Optional

import flet as _flt

import rslm.gui.routes as routes
from rslm.led_command_lib._generic_command import ConsecutiveCommands, GenericCommand
from rslm.led_command_lib._serialization import deserialize_generic_command_program

LOGGER = logging.getLogger(__name__)


class PlaylistPicker(_flt.UserControl):
    def __init__(
        self, label: str = "Pick a file", on_change: Optional[Callable] = None
    ) -> None:
        super().__init__()
        if not os.path.exists(routes.UPLOAD_DIR):
            os.makedirs(routes.UPLOAD_DIR)
        self.label: str = label
        self.playlist: Optional[ConsecutiveCommands] = None
        self.on_change: Optional[Callable] = on_change

    async def __download_file(self, e: _flt.FilePickerResultEvent) -> None:
        if not e.files:
            return
        result = e.files[0]
        if routes.runs_as_webapp:
            upload_url = await e.page.get_upload_url_async(
                file_name=result.name, expires=60
            )
            await self.file_picker.upload_async(
                [_flt.FilePickerUploadFile(name=result.name, upload_url=upload_url)]
            )
        else:
            if not os.path.exists(routes.UPLOAD_DIR):
                os.makedirs(routes.UPLOAD_DIR)
            shutil.copyfile(result.path, os.path.join(routes.UPLOAD_DIR, result.name))

        try:
            _ = deserialize_generic_command_program(
                os.path.join(routes.UPLOAD_DIR, result.name)
            )

            if isinstance(_, GenericCommand):
                self.playlist = [_]  # type: ignore
            elif isinstance(_, list):
                self.playlist = _  # type: ignore
            else:
                raise AssertionError()

            if self.on_change is not None:
                await self.on_change(e)

        except FileNotFoundError:
            e.page.snack_bar = _flt.SnackBar(
                content=_flt.Text(
                    selectable=True, value="Upload failed. Please try again."
                ),
                bgcolor="red",
            )
            e.page.snack_bar.open = True
            await e.page.update_async()
        except AssertionError:
            e.page.snack_bar = _flt.SnackBar(
                content=_flt.Text(selectable=True, value="Invalid XML content."),
                bgcolor="red",
            )
            e.page.snack_bar.open = True
            await e.page.update_async()
        except Exception as _:
            LOGGER.exception(_)
            e.page.snack_bar = _flt.SnackBar(
                content=_flt.Text(selectable=True, value="Something went wrong...."),
                bgcolor="red",
            )
            e.page.snack_bar.open = True
            await e.page.update_async()

    async def __on_click(self, e: _flt.ControlEvent):
        e.page.overlay.append(self.file_picker)
        await e.page.update_async()
        await self.file_picker.pick_files_async(
            dialog_title=self.label,
            allow_multiple=False,
            allowed_extensions=["xml"],
        )

    def build(self):
        self.file_picker: _flt.FilePicker = _flt.FilePicker()
        self.file_picker.on_result = self.__download_file

        return _flt.ElevatedButton(
            text=self.label,
            icon=_flt.icons.UPLOAD_FILE,
            on_click=self.__on_click,
        )
