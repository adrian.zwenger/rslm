import asyncio as _asyncio
import logging as _logging
import uuid as _uuid
from concurrent.futures import ThreadPoolExecutor as _ThreadPoolExecutor

import flet as _flt

from rslm.gui.backend import LED_BACKEND
from rslm.gui.components._playlist_viewer import Playlist as _Playlist

LOGGER: _logging.Logger = _logging.getLogger(__name__)

_pool: _ThreadPoolExecutor = _ThreadPoolExecutor()


# TODO: when stop is called, GUI remains unresponsive
class PlayerControls(_flt.UserControl):
    def __init__(self, playlist: _Playlist):
        """
        make sure that the passed playlist is empty as it will get populated using the LED_BACKEND
        """
        super().__init__()
        assert playlist and not playlist.tracks
        self._async_lock = _asyncio.Lock()
        self.is_mounted: _asyncio.Event = _asyncio.Event()
        self.stop_in_progress: _asyncio.Event = _asyncio.Event()
        self.uuid: _uuid.UUID = _uuid.uuid4()
        self.playlist: _Playlist = playlist

        self.total_time: _flt.Ref = _flt.Ref[_flt.TextField]()
        self.progressed_time: _flt.Ref = _flt.Ref[_flt.TextField]()
        self.progress_bar: _flt.Ref = _flt.Ref[_flt.ProgressBar]()

    async def highlight_butons(self):
        assert LED_BACKEND
        for btn in [
            self.previous_button,
            self.play_button,
            self.pause_button,
            self.stop_button,
            self.next_button,
            self.repeat_button,
        ]:
            btn.selected = False

        if LED_BACKEND.is_playing():
            self.play_button.selected = True
        elif LED_BACKEND.is_paused():
            self.pause_button.selected = True
        else:
            self.stop_button.selected = True

        self.repeat_button.selected = LED_BACKEND.is_repeating()

        for btn in [
            self.previous_button,
            self.play_button,
            self.pause_button,
            self.stop_button,
            self.next_button,
            self.repeat_button,
        ]:
            await btn.update_async()

    async def __app_is_busy(self) -> None:
        if not self.page:
            return
        self.page.snack_bar = _flt.SnackBar(
            content=_flt.Text(
                "Hardware is busy. Please wait and try again later",
                selectable=True,
            ),
            bgcolor="red",
        )
        self.page.snack_bar.open = True
        self.page.update()

    async def __track_highlight_loop(self, update_delay: float = 1.5) -> None:
        """periodically checks the state of the backend player and sets state accordingly.
        is started as a coroutine by ``did_mount_async``"""
        assert LED_BACKEND
        while self.is_mounted.is_set():
            if (
                not LED_BACKEND.is_playing()
                or not self.page
                or self.stop_in_progress.is_set()
            ):
                await _asyncio.sleep(delay=1.5)
                continue
            await self.__highlight_current_track(update_delay=update_delay)

    async def __highlight_current_track(self, update_delay: float = 1.5) -> None:
        """highlights the current track."""
        assert LED_BACKEND
        idx: int = await LED_BACKEND.next_track_idx()
        await self.playlist.highlight_track(idx=idx)
        await self.playlist.update_async()
        await _asyncio.sleep(delay=update_delay)

    async def __progress_update_loop(self, update_delay: float = 0.5) -> None:
        assert LED_BACKEND
        while self.is_mounted.is_set():
            if (
                not LED_BACKEND.is_playing()
                or not self.page
                or self.stop_in_progress.is_set()
            ):
                await _asyncio.sleep(delay=2.0)
                continue
            await self.__update_progress()
            await _asyncio.sleep(update_delay)

    async def __update_progress(self) -> None:
        assert LED_BACKEND
        _progress: int = int(LED_BACKEND.get_progressed_time())
        _total: int = int(LED_BACKEND.get_cmd_delay())

        self.progressed_time.current.value = f"{_progress if _progress > 0 else '-'} s"
        if _progress >= _total and LED_BACKEND.is_playing():
            self.total_time.current.value = "executing ..."
        else:
            self.total_time.current.value = f"{_total if _total > 0 else '-'} s"

        await self.total_time.current.update_async()
        await self.progressed_time.current.update_async()

        if _total == 0:
            return

        if _progress < 0 or _total < 0:
            self.progress_bar.current.value = 0.0
            await self.progress_bar.current.update_async()
            return

        _current = _progress / _total

        self.progress_bar.current.value = _current
        await self.progress_bar.current.update_async()

    async def on_play_click(self, e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        assert LED_BACKEND
        assert self.page
        if LED_BACKEND.is_playing():
            return
        elif LED_BACKEND.is_paused():
            await LED_BACKEND.pause(self.page.session_id)
        else:
            await LED_BACKEND.play(self.page.session_id)
        await self.__update_progress()
        await self.highlight_butons()

    async def on_pause_click(self, e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        assert LED_BACKEND
        assert self.page
        if LED_BACKEND.is_playing():
            await LED_BACKEND.pause(self.page.session_id)
            await self.__update_progress()
            await self.highlight_butons()

    async def on_stop_click(self, e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        assert LED_BACKEND
        assert self.page
        self.stop_in_progress.set()

        await LED_BACKEND.stop(self.page.session_id)
        await self.highlight_butons()
        await self.__highlight_current_track(update_delay=0)
        await self.__update_progress()
        await self.highlight_butons()
        e.page.update()

        async def _():
            assert LED_BACKEND
            await LED_BACKEND.init_leds()
            # await LED_BACKEND.init_leds()
            self.stop_in_progress.clear()

        e.page.run_task(_)

    async def on_next_click(self, e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        assert LED_BACKEND
        assert self.page
        await LED_BACKEND.next(self.page.session_id)
        await self.__highlight_current_track(update_delay=0)
        await self.__update_progress()

    async def on_previous_click(self, e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        assert LED_BACKEND
        assert self.page
        await LED_BACKEND.previous(self.page.session_id)
        await self.__update_progress()
        await self.__highlight_current_track(update_delay=0)

    async def on_repeat_click(self, _e: _flt.ControlEvent) -> None:
        if self.stop_in_progress.is_set():
            return await self.__app_is_busy()
        if LED_BACKEND:
            assert self.page
            await LED_BACKEND.toggle_repeat(self.page.session_id)
            await self.highlight_butons()

    def build(self):
        """assembles the visual components"""
        assert LED_BACKEND is not None

        _r: float = LED_BACKEND.get_progressed_time()
        _time_txt: _flt.Row = _flt.Row(
            controls=[
                _flt.Text(
                    value=f"{_r if _r > 0 else '-'} s",
                    ref=self.progressed_time,
                    size=12,
                    selectable=True,
                ),
                _flt.Text(
                    " : ",
                    size=12,
                    selectable=True,
                ),
                _flt.Text(
                    f"{LED_BACKEND.get_cmd_delay()} s",
                    ref=self.total_time,
                    size=12,
                    selectable=True,
                ),
            ]
        )

        _progress: _flt.ProgressBar = _flt.ProgressBar(ref=self.progress_bar, value=0.0)

        self.play_button = _flt.IconButton(
            icon=_flt.icons.PLAY_ARROW,
            selected_icon=_flt.icons.PLAY_CIRCLE_FILL,
            selected_icon_color=_flt.colors.RED,
            on_click=self.on_play_click,
            tooltip="Play",
        )
        self.pause_button = _flt.IconButton(
            icon=_flt.icons.PAUSE,
            selected_icon=_flt.icons.PAUSE_CIRCLE_FILLED_SHARP,
            selected_icon_color=_flt.colors.RED,
            on_click=self.on_pause_click,
            tooltip="Pause",
        )
        self.stop_button = _flt.IconButton(
            icon=_flt.icons.STOP,
            selected_icon=_flt.icons.STOP_CIRCLE_OUTLINED,
            selected_icon_color=_flt.colors.RED,
            on_click=self.on_stop_click,
            tooltip="Stop",
        )
        self.next_button = _flt.IconButton(
            icon=_flt.icons.SKIP_NEXT, on_click=self.on_next_click, tooltip="Next"
        )
        self.previous_button = _flt.IconButton(
            icon=_flt.icons.SKIP_PREVIOUS,
            on_click=self.on_previous_click,
            tooltip="Previous",
        )
        self.repeat_button = _flt.IconButton(
            icon=_flt.icons.REPEAT,
            selected_icon=_flt.icons.REPEAT_ON,
            selected_icon_color=_flt.colors.RED,
            on_click=self.on_repeat_click,
            tooltip="Repeat Playlist",
        )

        _buttons = _flt.Row(
            controls=[
                self.previous_button,
                self.play_button,
                self.pause_button,
                self.stop_button,
                self.next_button,
                self.repeat_button,
                _time_txt,
            ],
            alignment=_flt.MainAxisAlignment.CENTER,
            expand=False,
        )

        self.view = _flt.Container(
            expand=True,
            content=_flt.Column(
                controls=[
                    _progress,
                    _buttons,
                    # _time_txt,
                ],
                spacing=0,
            ),
        )

        self.last_idx: int = -1

        return self.view

    async def rebuild(self, __retries: int = 0):
        """rebuild the visual components if playlist is changed."""
        # LOGGER.critical("START REBUILDING, is mounted: %s", self.is_mounted.is_set())
        if LED_BACKEND and self.page:
            # LOGGER.critical("ACTUALLY REBUILDING")
            await self.playlist.reset()
            await self.playlist.add_cmds(
                await LED_BACKEND.get_playlist(session_id=self.page.session_id)
            )
            await self.playlist.update_async()
            await self.playlist.highlight_track(idx=await LED_BACKEND.next_track_idx())
            await self.playlist.update_async()
        else:
            if __retries > 5:
                LOGGER.critical("Unable to update playlist contents.")
                return
            await _asyncio.sleep(0.5)
            LOGGER.warning("Unable to update playlist contents. Now retrtying")
            await self.rebuild(__retries + 1)

    def did_mount(self) -> None:
        """executed by flet framework after visual components are loaded and
        configures the player using async tasks"""
        self.is_mounted.set()
        super().did_mount()

        assert self.page

        async def __load_tracks():
            assert LED_BACKEND
            for i in range(0, 10):
                if not self.is_mounted.is_set():
                    return
                if self.page:
                    # TODO: implement logging
                    playlist = await LED_BACKEND.get_playlist(
                        session_id=self.page.session_id
                    )
                    await self.playlist.add_cmds(playlist)
                    await self.highlight_butons()
                    await self.playlist.update_async()
                    if playlist:
                        await self.playlist.highlight_track(idx=0)
                    await self.playlist.update_async()
                    return
                await _asyncio.sleep(delay=0.5)

        self.page.run_task(__load_tracks)
        self.page.run_task(self.__track_highlight_loop)
        self.page.run_task(self.__progress_update_loop)

    def will_unmount(self) -> None:
        self.is_mounted.clear()
        # return await super().will_unmount_async()
        return super().will_unmount()
