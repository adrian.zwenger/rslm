# TODO document UserControls and Control interplay
import asyncio
import logging as _logging
import uuid
from types import ModuleType
from typing import Any, Callable, Dict, List, Optional, Sequence, TypeVar, Union

import flet as flt

import rslm.gui._resize_handler as GlobalResizeHandler
import rslm.led_command_lib.common as _common_commands
import rslm.led_command_lib.led as _led_commands
import rslm.led_command_lib.power as _power_commands
import rslm.led_command_lib.tec as _tec_commands
import rslm.macros as _macros
from rslm.gui.components._input_controls import (
    BoolInput,
    CustomFletControl,
    FloatInput,
    IntegerInput,
    StringInput,
)
from rslm.gui.routes import exit_view
from rslm.led_command_lib import GenericCommand, cmd_carbon_copy
from rslm.led_command_lib._generic_command import (
    API_CONFIG_PARAM_TYPE_BOOL_ID,
    API_CONFIG_PARAM_TYPE_FLOAT_ID,
    API_CONFIG_PARAM_TYPE_ID,
    API_CONFIG_PARAM_TYPE_INT_ID,
    API_CONFIG_PARAM_TYPE_STRING_ID,
    API_CONFIG_VALID_VALUES_SECTION_ID,
)
from rslm.led_command_lib._serialization import ConsecutiveCommands

LOGGER: _logging.Logger = _logging.getLogger(__name__)

DEFAULT_GROUP_ID: str = "default_playlist_group"

COMMAND_LIBS: Dict[str, ModuleType] = {
    "COMMON": _common_commands,
    "LED": _led_commands,
    "POWER": _power_commands,
    "TEC": _tec_commands,
}

MACRO_SELECTOR_LABEL: str = "Custom Macro"
DraggableTrack = TypeVar("DraggableTrack")  # type: ignore


BORDER_COLOR = flt.colors.WHITE70
REGISTERED_MACROS = {}


def _string_input_field(
    _attr: str, _attr_specs: Dict[str, Any], default_val: Optional[str] = None
) -> CustomFletControl:
    if API_CONFIG_VALID_VALUES_SECTION_ID in _attr_specs:
        str_input: StringInput = StringInput(
            default_val=(
                _attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID][0]
                if not default_val
                else default_val
            ),
            valid_vals=_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID],
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Enter a value {_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID]}",
            # width=width,
        )
    else:
        str_input: StringInput = StringInput(
            default_val=default_val if default_val else "",
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Choose a value for the '{_attr}' attribute.",
            # width=width,
        )
    return str_input


def _int_input_field(
    _attr: str, _attr_specs: Dict[str, Any], default_val: Optional[int] = None
) -> CustomFletControl:
    if API_CONFIG_VALID_VALUES_SECTION_ID in _attr_specs:
        # bounded int input
        return IntegerInput(
            default_val=(
                _attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID][0]
                if default_val is None
                else default_val
            ),
            int_range=_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID],
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Enter a value {_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID]}",
        )
    else:
        # unbounded int input
        return IntegerInput(
            default_val=default_val if default_val is not None else 0,
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Choose a value for the '{_attr}' attribute.",
        )


def _float_input_field(
    _attr: str, _attr_specs: Dict[str, Any], default_val: Optional[float] = None
) -> CustomFletControl:
    if API_CONFIG_VALID_VALUES_SECTION_ID in _attr_specs:
        # bounded float input
        return FloatInput(
            default_val=(
                _attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID][0]
                if default_val is None
                else default_val
            ),
            float_range=_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID],
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Enter a value {_attr_specs[API_CONFIG_VALID_VALUES_SECTION_ID]}",
        )
    else:
        # unbounded float input
        return FloatInput(
            default_val=default_val if default_val is not None else 0.0,
            description=f"Choose a value for the '{_attr}' attribute:",
            tooltip=f"Choose a value for the '{_attr}' attribute.",
        )


def _bool_input_field(
    _attr: str, default_val: Optional[bool] = False
) -> CustomFletControl:
    return BoolInput(
        default_val=default_val if default_val is not None else False,
        description=f"Choose a value for the '{_attr}' attribute:",
        tooltip=f"Choose a value for the '{_attr}' attribute.",
    )


class _Configurator(flt.UserControl):
    def __init__(self) -> None:
        super().__init__()
        self._async_lock = asyncio.Lock()
        self.uuid: uuid.UUID = uuid.uuid4()
        self.resize_handlers: Dict[Union[str, uuid.UUID], Callable] = {}
        self.attr_to_ctrl_dict: Dict[str, CustomFletControl] = {}

    async def is_valid(self) -> bool:
        raise NotImplementedError()

    async def _resize(self, e: flt.ControlEvent) -> None:
        async with self._async_lock:
            self._col.width = self.width
            self._col.height = self.height
            for _, handler in self.resize_handlers.items():
                await handler(e)
        await self._col.update_async()
        await self.update_async()
        return await e.page.update_async()

    def _build_controls(self) -> List[CustomFletControl]:
        raise NotImplementedError()

    def build(self):
        __input_controls: List[CustomFletControl] = self._build_controls()
        self._col: flt.Column = flt.Column(
            alignment=flt.MainAxisAlignment.CENTER,
            controls=__input_controls,  # type: ignore
            spacing=15,
            scroll=flt.ScrollMode.AUTO,
            width=self.width,
            height=self.height,
        )
        self.view = self._col
        return self.view

    def did_mount(self) -> None:
        GlobalResizeHandler.add_resize_handler(self.uuid, self._resize)
        return super().did_mount()

    def will_unmount(self) -> None:
        # async with self._async_lock:
        #     if self._col.controls:
        #         # for ctrl in self._col.controls:
        #         #     if isinstance(ctrl, CustomFletControl):
        #         #         del self.resize_handlers[ctrl.uuid]
        #         # for attr in self.attr_to_ctrl_dict:
        #         #     del self.attr_to_ctrl_dict[attr]
        #         self._col.controls = None
        self._col.controls = None
        GlobalResizeHandler.remove_resize_handler(self.uuid)
        return super().will_unmount()

    async def configure_output(self) -> List[str]:
        """reads the the values from the configuration form stored in self.cmd_configurator
        and configures the GenericCommand stored in self.output.
        returns list of attribute names which could not be set.
        """
        raise NotImplementedError()


class GenericCommandConfigurator(_Configurator):
    def __init__(
        self,
        cmd: Optional[GenericCommand] = None,
    ) -> None:
        super().__init__()
        self.cmd: Optional[GenericCommand] = cmd

    async def is_valid(self) -> bool:
        async with self._async_lock:
            if not self.cmd:
                return False
            is_valid: bool = True
            for attr in self.cmd.sorted_param_list:
                ctrl: CustomFletControl = self.attr_to_ctrl_dict[attr]
                try:
                    assert hasattr(ctrl, "value")
                    assert ctrl.value is not None
                    self.cmd.__setattr__(attr=attr, value=ctrl.value)
                except AssertionError:
                    pass
        return is_valid

    def _build_controls(self) -> List[CustomFletControl]:
        if not self.cmd:
            return []

        _return: List[CustomFletControl] = []
        for attr in self.cmd.sorted_param_list:
            attr_specs: Dict[str, Any] = self.cmd.get_attr_specs(attr=attr)
            default_val: Optional[Any] = self.cmd.__getattribute__(attr)

            if attr_specs[API_CONFIG_PARAM_TYPE_ID] == API_CONFIG_PARAM_TYPE_STRING_ID:
                _input_ctrl: CustomFletControl = _string_input_field(
                    _attr=attr, _attr_specs=attr_specs, default_val=default_val
                )
            elif attr_specs[API_CONFIG_PARAM_TYPE_ID] == API_CONFIG_PARAM_TYPE_INT_ID:
                _input_ctrl: CustomFletControl = _int_input_field(
                    _attr=attr, _attr_specs=attr_specs, default_val=default_val
                )
            elif attr_specs[API_CONFIG_PARAM_TYPE_ID] == API_CONFIG_PARAM_TYPE_FLOAT_ID:
                _input_ctrl: CustomFletControl = _float_input_field(
                    _attr=attr, _attr_specs=attr_specs, default_val=default_val
                )
            elif attr_specs[API_CONFIG_PARAM_TYPE_ID] == API_CONFIG_PARAM_TYPE_BOOL_ID:
                _input_ctrl: CustomFletControl = _bool_input_field(
                    _attr=attr, default_val=default_val
                )
            else:
                raise ValueError(
                    f'unexpected attribute type: "{attr_specs[API_CONFIG_PARAM_TYPE_ID]}" for attribute "{attr}"'
                )

            self.attr_to_ctrl_dict[attr] = _input_ctrl
            _return.append(_input_ctrl)

        _input_ctrl = IntegerInput(
            default_val=self.cmd.__getattribute__("delay_seconds"),
            description="How many seconds to wait before executing the command?",
            tooltip="Choose how long to wait in seconds before a command is executed. 0 means immediately execute.",
        )
        self.attr_to_ctrl_dict["delay_seconds"] = _input_ctrl
        _return.insert(0, _input_ctrl)
        return _return

    async def configure_output(self) -> List[str]:
        """reads the the values from the configuration form stored in self.cmd_configurator
        and configures the GenericCommand stored in self.output.
        returns list of attribute names which could not be set.
        """
        malformed_params: List[str] = []
        for param, ctrl in self.attr_to_ctrl_dict.items():
            try:
                self.cmd.__setattr__(param, ctrl.value)
            except AssertionError:
                malformed_params.append(param)
        return malformed_params


# TODO: think of generic handler for this
class MacroConfigurator(_Configurator):
    def __init__(
        self,
        cmd: Optional[_macros._MacroBaseClass] = None,
    ) -> None:
        super().__init__()
        self.cmd: Optional[_macros._MacroBaseClass] = cmd

    async def is_valid(self) -> bool:
        # TODO: implement as soon as configurable Macros are ready
        async with self._async_lock:
            if self.cmd is not None:
                return True
        return False

    def _build_controls(self) -> List[CustomFletControl]:
        _return = []
        _input_ctrl = IntegerInput(
            default_val=0,
            description="How many seconds to wait before executing this Macro?",
            tooltip="Choose how long to wait in seconds before the Macro is executed. 0 means immediately execute.",
        )
        self.attr_to_ctrl_dict["delay_seconds"] = _input_ctrl
        _return.insert(0, _input_ctrl)
        return _return

    async def configure_output(self) -> List[str]:
        """reads the the values from the configuration form stored in self.cmd_configurator
        and configures the GenericCommand stored in self.output.
        returns list of attribute names which could not be set.
        """
        # print(f"Configuring Macro: {self.cmd}")
        malformed_params: List[str] = []
        for param, ctrl in self.attr_to_ctrl_dict.items():
            try:
                self.cmd.__setattr__(param, ctrl.value)
            except AssertionError:
                malformed_params.append(param)
        if not malformed_params:
            print(f"Configured Macro: {self.cmd}")
            assert self.cmd and isinstance(self.cmd, _macros._MacroBaseClass)
            self.cmd.construct()
        return malformed_params


class CreateGenericCommandDialog(flt.UserControl):
    def __init__(self):
        super().__init__()
        self.__async_lock = asyncio.Lock()
        self.uuid: uuid.UUID = uuid.uuid4()
        self.output: Optional[Union[GenericCommand, ConsecutiveCommands]] = None

    async def __on_cmd_type_selection(self, e: flt.ControlEvent) -> None:
        assert e.control
        assert isinstance(e.control, flt.Dropdown)
        _page: flt.Page = self.page if self.page else e.page

        if not e.control.value:
            return

        if (
            e.control.value not in COMMAND_LIBS
            and e.control.value != MACRO_SELECTOR_LABEL
        ):
            return

        if self.cmd_configurator:
            # clear configurator content if already present.
            async with self.__async_lock:
                self.cmd_configurator_container.content = None
                # if self.cmd_configurator in self.view.controls:
                #     self.view.controls.remove(self.cmd_configurator)
                self.cmd_configurator = None
                await self.cmd_configurator_container.update_async()
                await self.view.update_async()

            await self.view.update_async()
            await _page.update_async()

        _options: List[flt.dropdown.Option] = []

        if e.control.value == MACRO_SELECTOR_LABEL:
            for _ in dir(_macros):
                if not _.startswith("_"):
                    macro: Any = getattr(_macros, _)()
                    assert isinstance(macro, ConsecutiveCommands)
                    _options.append(flt.dropdown.Option(_, text=macro.short_name))
        else:
            for _ in dir(COMMAND_LIBS[e.control.value]):
                if not _.startswith("_"):  # remove all non GenericCommand definitions
                    cmd: Any = getattr(
                        COMMAND_LIBS[e.control.value],
                        _,
                    )()
                    assert isinstance(cmd, GenericCommand)
                    if self.expert_mode:
                        # add all commands in expert mode
                        _options.append(flt.dropdown.Option(_, text=cmd.base_command))
                    elif not cmd.advanced_usage and not cmd.needs_gamemode:
                        # only add non advanced commands to options
                        _options.append(flt.dropdown.Option(_, text=cmd.base_command))

        self.cmd_selector.options = _options  # add new options
        self.cmd_selector.visible = True  # make field visible
        return await self.cmd_selector.focus_async()

    async def __on_cmd_selection(self, e: flt.ControlEvent) -> None:
        _page: flt.Page = self.page if self.page else e.page

        if (
            self.type_selector.value in COMMAND_LIBS
        ):  # regular command, not macros, were chosen
            async with self.__async_lock:
                assert self.type_selector.value
                _cmd: GenericCommand = getattr(
                    COMMAND_LIBS[self.type_selector.value],
                    e.control.value,
                )()
                self.output = _cmd
                self.cmd_configurator = GenericCommandConfigurator(cmd=_cmd)
                self.cmd_configurator_container.content = self.cmd_configurator
        elif self.type_selector.value == MACRO_SELECTOR_LABEL:
            # TODO: think of generic handler for this
            async with self.__async_lock:
                assert self.type_selector.value
                _macro: _macros._MacroBaseClass = getattr(
                    _macros,
                    e.control.value,
                )()
                self.output = _macro
                self.cmd_configurator = MacroConfigurator(cmd=_macro)
                self.cmd_configurator_container.content = self.cmd_configurator
        else:
            raise RuntimeError()

        await self.cmd_configurator_container.update_async()
        await self.view.update_async()
        await self.update_async()
        # await self.cmd_configurator.set_visibility(True)  # , width=self.width)
        await _page.update_async()
        # return await e.page.update_async()

    def build(self) -> flt.Column:
        self.cmd_configurator: Optional[_Configurator] = None
        # TODO extend to be able to configure macros

        self.type_selector: flt.Dropdown = flt.Dropdown(
            label="Command Type",
            hint_text="Choose type of command to create",
            options=[],
            autofocus=True,
            visible=True,
            # expand=True,
            border_color=BORDER_COLOR,
        )

        self.cmd_selector: flt.Dropdown = flt.Dropdown(
            label="Command",
            hint_text="Chose command to create.",
            autofocus=False,
            visible=False,
            border_color=BORDER_COLOR,
            # expand=True,
        )

        self.selector_row: flt.Column = flt.Column(
            controls=[
                flt.Row(
                    controls=[
                        self.type_selector,
                    ],
                    wrap=True,
                    # expand=True,
                    alignment=flt.MainAxisAlignment.CENTER,
                ),
                flt.Row(
                    controls=[
                        self.cmd_selector,
                    ],
                    wrap=True,
                    # expand=True,
                    alignment=flt.MainAxisAlignment.CENTER,
                ),
            ],  # type: ignore
        )

        self.type_selector.on_change = self.__on_cmd_type_selection
        self.cmd_selector.on_change = self.__on_cmd_selection

        self.cmd_configurator_container: flt.Container = flt.Container(
            # bgcolor=flt.colors.WHITE,
            expand=True,
        )

        self.view = flt.Column(
            # controls=[self.selector_row],
            controls=[self.selector_row, self.cmd_configurator_container],
            expand=True,
            # alignment=flt.MainAxisAlignment.CENTER,
            spacing=20,
        )
        return self.view

    async def reset(self, reset_focus: bool = True) -> None:
        async with self.__async_lock:
            self.output = None
            self.type_selector.value = None
            self.cmd_selector.value = None
            self.cmd_selector.visible = False

        if self.cmd_configurator:
            self.cmd_configurator = None
            self.cmd_configurator_container.content = None
            await self.cmd_configurator_container.update_async()
            self.cmd_configurator = None

        if reset_focus:
            await self.type_selector.focus_async()

        if self.page:
            await self.page.update_async()
        return

    def did_mount(self) -> None:
        """called after component is added to the page.
        here it loads options into the form depending if the page is in expert mode or not.

        requires the flet page to have the ´´´expert_mode´´´ boolean attribute
        """

        async def resize(e: flt.ControlEvent) -> None:
            if self.cmd_configurator:
                self.cmd_configurator.width = e.page.width * 0.85
                self.cmd_configurator.height = e.page.height * 0.85

                self.cmd_configurator_container.width = e.page.width * 0.85
                self.cmd_configurator_container.height = e.page.height * 0.85

                self.view.width = e.page.width * 0.85
                self.view.height = e.page.height * 0.85

                await self.cmd_configurator_container.update_async()
                await self.cmd_configurator._resize(e)
            return await self.selector_row.update_async()

        GlobalResizeHandler.add_resize_handler(event_id=self.uuid, handler=resize)

        assert hasattr(self.page, "expert_mode")
        self.expert_mode: bool = self.page.expert_mode  # type: ignore

        _options: List[flt.dropdown.Option] = []
        for lib in COMMAND_LIBS:
            cmds: ConsecutiveCommands = ConsecutiveCommands()
            lib_contents = dir(COMMAND_LIBS[lib])
            for _ in lib_contents:
                if not _.startswith("_"):  # remove all non GenericCommand definitions
                    cmd: Any = getattr(
                        COMMAND_LIBS[lib],
                        _,
                    )()
                    if not isinstance(cmd, GenericCommand):
                        pass
                    if self.expert_mode:
                        # add all commands in expert mode
                        cmds.append(cmd)
                    elif not cmd.advanced_usage and not cmd.needs_gamemode:
                        # only add non advanced commands to options
                        cmds.append(cmd)
            if cmds:
                _options.append(flt.dropdown.Option(lib))
        _options.append(flt.dropdown.Option(MACRO_SELECTOR_LABEL))

        self.type_selector.options = _options
        assert self.page
        self.page.run_task(self.type_selector.update_async)
        # asyncio.get_running_loop().run_until_complete(self.type_selector.update_async())

        super().did_mount()

    def will_unmount(self) -> None:
        # called before component is removed from page
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        if self.cmd_configurator:
            self.cmd_configurator.will_unmount()
        super().will_unmount()

    async def output_is_valid(self) -> bool:
        async with self.__async_lock:
            if not self.output:
                return False
            try:
                self.output.is_valid()
            except AssertionError:
                # LOGGER.exception(_e)
                # LOGGER.exception(self.output)
                return False
            return True

    async def configure_output(self) -> bool:
        """reads the the values from the configuration form stored in self.cmd_configurator
        and configures the GenericCommand stored in self.output.
        returns False if errors occur while doing so.
        """
        if not self.cmd_configurator:
            return False
        async with self.__async_lock:
            malformed_params: List[str] = await self.cmd_configurator.configure_output()
            for malformed_param in malformed_params:
                ctrl: CustomFletControl = self.cmd_configurator.attr_to_ctrl_dict[
                    malformed_param
                ]
                ctrl.view.border = flt.border.all(10, flt.colors.RED)
                await ctrl.update_async()
            if malformed_params:
                await self.cmd_configurator.update_async()
                await self.update_async()
                assert self.page
                await self.page.update_async()
                return False
        return True


class Playlist(flt.UserControl):
    def __init__(
        self,
        group: str = DEFAULT_GROUP_ID,
        tracks: Optional[
            Union[GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]]
        ] = None,
        edit_mode: bool = True,
        ref: Optional[flt.Ref] = None,
        title: str = "Playlist:",
    ) -> None:
        super().__init__(expand=True, ref=ref)
        self._async_lock = asyncio.Lock()
        self.uuid: uuid.UUID = uuid.uuid4()
        self.tracks: List[Track] = []
        self.group_id: str = group
        self.start_indicator: flt.Control = self.__create_seperator()
        self.cmd_creator: Optional[CreateGenericCommandDialog] = None
        self.edit_mode: bool = edit_mode
        self.title: str = title

        if tracks:
            if isinstance(tracks, list):
                for _t in tracks:
                    self.tracks.append(
                        DraggableTrack(self, _t) if self.edit_mode else Track(self, _t)
                    )
            if isinstance(tracks, GenericCommand):
                self.tracks.append(
                    DraggableTrack(self, tracks)
                    if self.edit_mode
                    else Track(self, tracks)
                )

    async def add_cmds(
        self,
        cmds: Union[
            GenericCommand, Sequence[Union[GenericCommand, ConsecutiveCommands]]
        ],
    ) -> None:
        async with self._async_lock:
            if isinstance(cmds, list):
                for _t in cmds:
                    _ = DraggableTrack(self, _t) if self.edit_mode else Track(self, _t)
                    self.tracks.append(_)
                    self.track_column.controls.append(_)
                    self.track_column.controls.append(self.__create_seperator())
            if isinstance(cmds, GenericCommand):
                _ = DraggableTrack(self, cmds) if self.edit_mode else Track(self, cmds)
                self.tracks.append(_)
                self.track_column.controls.append(_)
                self.track_column.controls.append(self.__create_seperator())

    def __create_seperator(self) -> flt.Control:
        return flt.DragTarget(
            group=self.group_id,
            on_will_accept=self.drag_will_accept,
            on_accept=self.drag_accept,
            on_leave=self.on_drag_leave,
            content=flt.Container(
                bgcolor=flt.colors.WHITE,
                border_radius=flt.border_radius.all(value=20),
                height=10,
                opacity=0.0,
                expand=True,
            ),
        )

    def build(self) -> flt.Control:
        _contents: List[flt.Control] = [flt.Text(selectable=True, value=self.title)]

        self.track_column: flt.Column = flt.Column(
            controls=[],
            spacing=0,
            # tight=False,
            expand=True,
            scroll=flt.ScrollMode.ALWAYS,
            # alignment=flt.MainAxisAlignment.CENTER,
        )
        for _ in self.tracks:
            self.track_column.controls.append(_)
            self.track_column.controls.append(self.__create_seperator())

        _contents.append(self.start_indicator)
        _contents.append(
            # flt.ListView(controls=[self.track_column], expand=True, spacing=0)
            self.track_column
        )
        self.view: flt.Control = flt.Column(
            spacing=0,
            tight=False,
            expand=True,
            controls=_contents,
        )
        return self.view

    async def __add_track_dialog_close(self, e: flt.ControlEvent) -> None:
        _page: flt.Page = self.page if self.page else e.page
        self.cmd_creator = None
        _page.splash = None
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        await self.update_async()
        # if _page.floating_action_button:
        #     _page.floating_action_button.visible = True
        await _page.update_async()

    async def __add_track_dialog_apply(self, e: flt.ControlEvent) -> None:
        assert self.cmd_creator

        if self.cmd_creator.output is None:

            async def close_alert(_e: flt.ControlEvent) -> None:
                _alert.open = False
                await _e.page.update_async()

            _alert: flt.AlertDialog = flt.AlertDialog(
                title=flt.Text("Please choose an item to add.", selectable=True),
                modal=False,
                open=True,
                actions_alignment=flt.MainAxisAlignment.END,
                actions=[
                    flt.ElevatedButton(
                        text="Dismiss.",
                        color=flt.colors.WHITE,
                        bgcolor=flt.colors.RED,
                        icon=flt.icons.CANCEL_OUTLINED,
                        on_click=close_alert,
                    )
                ],
            )

            e.page.dialog = _alert
            await e.page.update_async()
            return

        await self.cmd_creator.configure_output()
        if await self.cmd_creator.output_is_valid():
            await self.add_item(item=self.cmd_creator.output)
            return await self.__add_track_dialog_close(e=e)
        await e.page.update_async()

    async def _open_add_track_dialog(self, e: flt.ControlEvent) -> None:
        _page: flt.Page = self.page if self.page else e.page
        if not self.page:
            self.page = e.page

        self.cmd_creator = CreateGenericCommandDialog()

        self._dialog_section: flt.Control = flt.Container(
            # height=0.85 * self.page.height,
            # width=0.85 * self.page.width,
            # expand=1,
            alignment=flt.alignment.top_center,
            content=self.cmd_creator,
        )

        self._dialog_section.height = 0.80 * _page.height
        self._dialog_section.width = 0.85 * _page.width

        self._btn_section: flt.Container = flt.Container(
            height=0.15 * _page.height,
            width=0.85 * _page.width,
            alignment=flt.alignment.bottom_right,
            expand=True,
            content=flt.Row(
                controls=[
                    flt.FloatingActionButton(
                        text="Apply",
                        on_click=self.__add_track_dialog_apply,
                        icon=flt.icons.CHECK_OUTLINED,
                        bgcolor=flt.colors.GREEN,
                    ),
                    flt.FloatingActionButton(
                        text="Cancel",
                        on_click=self.__add_track_dialog_close,
                        icon=flt.icons.CANCEL_OUTLINED,
                        bgcolor=flt.colors.RED,
                    ),
                ],
                expand=True,
                alignment=flt.MainAxisAlignment.END,
            ),
        )

        self.splash: flt.Container = flt.Container(
            bgcolor=flt.colors.BLACK,
            border_radius=flt.border_radius.all(20),
            content=flt.Column(
                controls=[self._dialog_section, self._btn_section], expand=True
            ),
            alignment=flt.alignment.top_center,
        )

        self.splash.width = 0.95 * _page.width
        self.splash.height = 0.95 * _page.height
        self.splash.padding = 0.02 * _page.width

        _page.splash = flt.Container(
            expand=True, alignment=flt.alignment.center, content=self.splash
        )

        # if _page.floating_action_button:
        #     _page.floating_action_button.visible = False

        async def __resize(e: flt.ControlEvent) -> None:
            self.splash.height = 0.95 * e.page.height
            self.splash.width = 0.95 * e.page.width
            self.splash.padding = 0.02 * e.page.width
            await self.splash.update_async()

            self._dialog_section.height = 0.80 * e.page.height  # type: ignore
            self._dialog_section.width = 0.85 * e.page.width  # type: ignore
            await self._dialog_section.update_async()

            self._btn_section.height = 0.15 * e.page.height
            self._btn_section.width = 0.85 * e.page.width  # type: ignore
            return await self._btn_section.update_async()

        GlobalResizeHandler.add_resize_handler(event_id=self.uuid, handler=__resize)

        return await _page.update_async()

    async def add_item(
        self,
        item: Optional[Union[GenericCommand, ConsecutiveCommands]] = None,
    ) -> None:
        assert self.page
        if item:
            new_track: Track = (
                DraggableTrack(self, item) if self.edit_mode else Track(self, item)
            )
            async with self._async_lock:
                self.tracks.append(new_track)
                self.track_column.controls.append(new_track)
                self.track_column.controls.append(self.__create_seperator())

            await self.view.update_async()
            await self.page.update_async()
            await self.track_column.scroll_to_async(
                offset=-1, duration=500, curve=flt.AnimationCurve.EASE_IN_OUT
            )

    async def remove_item(self, item: DraggableTrack) -> None:
        async with self._async_lock:
            if item not in self.tracks:
                return
            self.tracks.remove(item)
            track_idx: int = self.track_column.controls.index(item)
            self.track_column.controls.pop(track_idx)
            self.track_column.controls.pop(track_idx)
        await self.update_async()
        return

    async def drag_accept(self, e: flt.DragTargetAcceptEvent) -> None:
        trg: flt.DragTarget = e.control
        assert trg
        if isinstance(trg.content, flt.Container):
            # remove element highlighting
            trg.content.opacity = 0.0
            await trg.content.update_async()

        _src: flt.Draggable = self.page.get_control(id=e.src_id)  # type: ignore
        assert _src
        if (
            _src.group != self.group_id
            or trg.group != self.group_id
            # or _src.group != trg.group
        ):
            return

        if not _src.data:
            # Draggables containing a Track are expected to have the track stored in data
            return await self.update_async()
        if not isinstance(_src.data, DraggableTrack):
            # Draggables containing a Track are expected to have the track stored in data
            return await self.update_async()

        src: DraggableTrack = _src.data

        if src not in self.tracks:
            return await self.update_async()

        if (
            trg == self.start_indicator
        ):  # user wants to move target to the very start of the list
            async with self._async_lock:
                src_idx: int = self.tracks.index(src)
                if src_idx == 0:
                    # element already at the beginning of list
                    return await self.update_async()

                # move logical representation to beginning
                self.tracks.pop(src_idx)
                self.tracks.insert(0, src)

                # move visual representation
                src_idx: int = self.track_column.controls.index(src)
                self.track_column.controls.pop(src_idx)  # pop Track
                seperator: flt.Control = self.track_column.controls.pop(
                    src_idx
                )  # pop seperator
                self.track_column.controls.insert(0, seperator)
                self.track_column.controls.insert(0, src)

            await self.update_async(update_all=False)
            return await self.update_async(update_all=True)

        if trg not in self.track_column.controls:
            # exit if for some reason the target is not part of this playlist
            return await self.update_async()

        async with self._async_lock:
            src_idx: int = self.track_column.controls.index(src)
            trg_idx: int = self.track_column.controls.index(trg)

            # first track is to be moved to the first seperator
            if src_idx == 0 and trg_idx == 1:
                # move first element to first place
                # change nothing
                return  # await self.update_async()

            # last track is to be moved to the last seperator
            _len: int = len(self.track_column.controls)
            if src_idx == _len - 2 and trg_idx == _len - 1:
                # move last element to last place
                # change nothing
                return  # await self.update_async()

            # src and trg are in playlist. the src is neither the first or last track
            prev_track: flt.Control = self.track_column.controls[trg_idx - 1]
            assert isinstance(prev_track, DraggableTrack)
            # reorder visual representation
            self.track_column.controls.pop(src_idx)
            seperator: flt.Control = self.track_column.controls.pop(src_idx)
            if trg not in self.track_column.controls:
                # LOGGER.critical("DragTarget not found for:\n%s", str(trg))
                # dragtarget unmounted before this task was scheduled
                return
            trg_idx: int = self.track_column.controls.index(
                trg
            )  # trg_idx might have changed
            self.track_column.controls.insert(trg_idx + 1, seperator)
            self.track_column.controls.insert(trg_idx + 1, src)

            # reorder logical representation
            self.tracks.pop(self.tracks.index(src))
            self.tracks.insert(self.tracks.index(prev_track) + 1, src)

        return await self.update_async(update_all=True)

    async def drag_will_accept(self, e: flt.ControlEvent) -> None:
        drag_target: flt.DragTarget = e.control
        if (
            isinstance(drag_target.content, flt.Container)
            and drag_target.group == self.group_id
        ):
            drag_target.content.opacity = 1.0
            try:
                await drag_target.content.update_async()  # fails if in process of unmounting
            except AssertionError:
                pass
        return

    async def on_drag_leave(self, e: flt.ControlEvent) -> None:
        drag_target: flt.DragTarget = e.control
        if (
            isinstance(drag_target.content, flt.Container)
            and drag_target.group == self.group_id
        ):
            drag_target.content.opacity = 0.0
            try:
                await drag_target.content.update_async()  # fails if in process of unmounting
            except AssertionError:
                pass
        return

    def will_unmount(self) -> None:
        assert self.page
        # if self.page.floating_action_button:
        #     if self.page.floating_action_button.data == self.uuid:
        # self.page.floating_action_button = None
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        self.tracks.clear()
        self.cmd_creator = None
        self.track_column.controls.clear()
        return super().will_unmount()

    # def did_mount(self) -> None:
    # assert self.page
    # self.page.floating_action_button = flt.FloatingActionButton(
    #     text="Add Item",
    #     icon=flt.icons.ADD,
    #     on_click=self._open_add_track_dialog,
    #     data=self.uuid,
    # )

    # await self.page.update_async()
    # return super().did_mount()

    async def update_async(self, update_all: bool = False) -> None:
        if update_all:
            for _ in self.track_column.controls:
                if isinstance(_, DraggableTrack):
                    await _.update_async()
        try:
            return await super().update_async()  # do not remove this line.
        except AssertionError as _e:
            LOGGER.critical("Unable to update %s.", self.__class__)
            LOGGER.error(_e)
            pass

    async def highlight_track(
        self,
        idx: Union[int, List[int]],
        # current_highlighted: Optional[int] = None
    ) -> None:
        # TODO: implement scroll to first highlighted element:
        if not self.tracks:
            return
        for i in range(0, len(self.tracks)):
            if self.tracks[i]._is_highlighted:
                await self.tracks[i].toggle_highlight()

        if isinstance(idx, int):
            if idx < 0:
                return
            if idx >= len(self.tracks):
                return
            await self.tracks[idx].toggle_highlight()
            await self.tracks[idx].update_async()
            # if current_highlighted is None:
            #     return

            # goto = current_highlighted - self.track_column.controls.index(
            #     self.tracks[idx]
            # )
            # print(goto, len(self.track_column.controls) - 1)
            # await self.track_column.scroll_to_async(
            #     offset=goto,
            #     # duration=50,
            #     # curve=flt.AnimationCurve.EASE_IN_OUT,
            # )

        elif isinstance(idx, list):
            for i in idx:
                if not isinstance(i, int):
                    continue
                if i < 0 or i >= len(self.tracks):
                    continue
                await self.tracks[i].toggle_highlight()
            # if current_highlighted is None:
            #     return
            # await self.track_column.scroll_to_async(
            #     offset=self.track_column.controls.index(self.tracks[idx[0]]),
            #     # duration=500,
            #     # curve=flt.AnimationCurve.EASE_IN_OUT,
            # )

    async def reset(self, e: Optional[flt.ControlEvent] = None) -> None:
        """
        deletes all tracks from this playlist
        """
        async with self._async_lock:
            if self.tracks:
                self.tracks = []
                self.cmd_creator = None
                self.track_column.controls.clear()

                await self.track_column.update_async()

    def get_playlist(self) -> Union[ConsecutiveCommands, GenericCommand]:
        _playlist: ConsecutiveCommands = ConsecutiveCommands()
        _playlist.extend([x.data for x in self.tracks])
        return _playlist


class Track(flt.UserControl):
    def __init__(
        self, playlist: Playlist, cmd: Union[ConsecutiveCommands, GenericCommand]
    ) -> None:
        super().__init__()
        assert cmd
        # self.__async_lock = asyncio.Lock()
        self.uuid: uuid.UUID = uuid.uuid4()
        self.data: Union[ConsecutiveCommands, GenericCommand] = cmd
        self.view: flt.Control
        self.playlist: Playlist = playlist
        self.idx_text: flt.Ref = flt.Ref[flt.Text]()
        self.cmd_editor: Optional[Union[GenericCommandConfigurator, Playlist]] = None
        self.info_text: flt.Ref = flt.Ref[flt.Text]()
        self.card: flt.Ref = flt.Ref[flt.Card]()
        self._is_highlighted: bool = False
        self.card_default_color: str = flt.colors.BLACK
        self.highlight_color: str = flt.colors.BLUE_ACCENT_700
        self.delay_text: flt.Ref = flt.Ref[flt.Text]()

    def __cmp__(self, other) -> bool:
        if not isinstance(other, Track):
            return False
        return self.uuid == other.uuid

    def __build_card(
        self,
        short_name: str,
        icon_buttons: Optional[List[flt.Control]] = None,
        text: Optional[flt.Text] = None,
    ) -> flt.Card:
        text_rows: List[flt.Control] = [
            flt.Text(
                selectable=True,
                value=f"{short_name}",
                size=20,
                weight=flt.FontWeight.BOLD,
            ),
        ]

        if text:
            text_rows.append(text)

        _t: float = (
            self.data.delay_seconds
            if isinstance(self.data, GenericCommand)
            else self.data.get_delay_seconds()
        )
        # text_rows.append(
        #     flt.Row(
        #         [
        #             flt.Icon(
        #                 flt.icons.ACCESS_TIME,
        #                 tooltip="Delay in seconds before the command is executed.",
        #             ),
        #             flt.Text(selectable=True,value=f"{_t} s"),
        #         ]
        #     )
        # )

        quickinfo_display: List[flt.Control] = [
            flt.Column(  # drag handle
                controls=[
                    flt.Row(
                        [
                            flt.Icon(name=flt.icons.DRAG_HANDLE),
                            flt.Text(
                                selectable=True,
                                ref=self.idx_text,
                                value=f"Track {self.playlist.tracks.index(self) + 1}:",
                            ),
                        ]
                    ),
                ],
            ),
            flt.Column(  # actual content
                controls=text_rows,
                expand=True,
            ),
            flt.Column(
                controls=[
                    flt.Row(
                        [
                            flt.Icon(
                                flt.icons.ACCESS_TIME,
                                tooltip="Delay in seconds before the command is executed.",
                            ),
                            flt.Text(
                                selectable=True, value=f"{_t} s", ref=self.delay_text
                            ),
                        ]
                    )
                ]
            ),
            flt.Column(  # delete, info, edit buttons
                controls=[flt.Row(controls=[*icon_buttons] if icon_buttons else [])],
            ),
        ]

        card: flt.Card = flt.Card(
            ref=self.card,
            color=self.card_default_color,
            elevation=1,
            data=self,
            content=flt.Container(
                padding=7,
                content=flt.Row(
                    alignment=flt.MainAxisAlignment.START,
                    controls=quickinfo_display,  # type: ignore
                ),
            ),
        )

        return card

    def _build_generic_cmd_card(
        self,
        icon_buttons: Optional[List[flt.Control]] = None,
        text: Optional[flt.Text] = None,
    ) -> flt.Card:
        assert isinstance(self.data, GenericCommand)
        short_name: str = self.data.base_command
        return self.__build_card(
            short_name=short_name, icon_buttons=icon_buttons, text=text
        )

    def _build_macro_card(
        self,
        icon_buttons: Optional[List[flt.Control]] = None,
        text: Optional[flt.Text] = None,
    ):
        assert isinstance(self.data, list)
        short_name: str = "Custom Macro"

        if isinstance(self.data, ConsecutiveCommands) and self.data.short_name:
            short_name = self.data.short_name

        return self.__build_card(
            short_name=short_name, icon_buttons=icon_buttons, text=text
        )

    async def update_async(self):
        self.idx_text.current.value = f"Track {self.playlist.tracks.index(self) + 1}:"
        await self.idx_text.current.update_async()
        _t: float = (
            self.data.delay_seconds
            if isinstance(self.data, GenericCommand)
            else self.data.get_delay_seconds()
        )
        self.delay_text.current.value = f"{_t} s"
        await self.delay_text.current.update_async()
        # return await super().update_async()

    async def _view_macro(self, _e: flt.ControlEvent) -> None:
        assert isinstance(self.data, ConsecutiveCommands)
        contents: List[flt.Control] = []

        if self.playlist.edit_mode:
            label: str = f"Editing Track {self.playlist.tracks.index(self) + 1}"
        else:
            label = f"Viewing Track {self.playlist.tracks.index(self) + 1}"

        app_bar: flt.AppBar = flt.AppBar(
            title=flt.Text(label, selectable=True),
            bgcolor=flt.colors.SURFACE_VARIANT,
            actions=[],
        )

        if self.playlist.edit_mode:
            self.delay_input_ctrl = IntegerInput(
                default_val=int(round(self.data.get_delay_seconds())),
                description="How many seconds to wait before executing this Macro?",
                tooltip="Choose how long to wait in seconds before the Macro is executed. 0 means immediately execute.",
            )

            async def apply_changes(e: flt.ControlEvent) -> None:
                await self.update_async()
                assert isinstance(self.cmd_editor, Playlist)
                if not self.cmd_editor.tracks:
                    return await exit_view(e=e)

                if len(self.cmd_editor.tracks) == 1:
                    __new_data = self.cmd_editor.tracks[0].data
                    self.data = __new_data  # type: ignore
                elif isinstance(self.data, list):
                    # __new_data = [_.data for _ in self.cmd_editor.tracks]
                    self.data.clear()
                    self.data.extend([_.data for _ in self.cmd_editor.tracks])
                    self.data.delay_seconds = self.delay_input_ctrl.value
                else:
                    raise RuntimeError(
                        f"Unexpected data type: {type(self.data)}. Expected list or GenericCommand."
                    )
                await exit_view(e=e)
                return await self.update_async()

            app_bar.actions.append(
                flt.TextButton(text="Save changes.", on_click=apply_changes)
            )

        contents.append(app_bar)

        self.cmd_editor = Playlist(
            group=f"edit_{self.uuid}",
            tracks=cmd_carbon_copy(self.data),  # type: ignore
            edit_mode=self.playlist.edit_mode,
        )

        if isinstance(self.data, ConsecutiveCommands):
            if self.data.short_name:
                contents.append(
                    flt.Text(selectable=True, value=self.data.short_name, size=20)
                )
            if self.data.description:
                contents.append(flt.Text(selectable=True, value=self.data.description))

        if self.playlist.edit_mode:
            contents.append(self.delay_input_ctrl)
            contents.append(
                flt.ElevatedButton(
                    text="Add Item",
                    icon=flt.icons.ADD,
                    on_click=self.cmd_editor._open_add_track_dialog,
                )
            )

        contents.append(flt.Container(content=self.cmd_editor, expand=True))

        _e.page.views.append(
            flt.View(
                route=f"edit_track_{self.uuid}",
                fullscreen_dialog=False,
                # padding=0.1 * _e.page.height,
                controls=contents,
            )
        )

        top_view = _e.page.views[-1]
        await _e.page.go_async(route=top_view.route)

    def build(self) -> flt.Control:
        if isinstance(self.data, GenericCommand):
            actions: List[flt.Control] = []

            _content = self._build_generic_cmd_card(
                icon_buttons=actions,
                text=flt.Text(
                    selectable=True, ref=self.info_text, value=str(self.data)
                ),
            )
        elif isinstance(self.data, list):
            actions: List[flt.Control] = [
                flt.IconButton(
                    icon=flt.icons.REMOVE_RED_EYE,
                    tooltip="Edit" if self.playlist.edit_mode else "View",
                    on_click=self._view_macro,
                ),
            ]
            _content = self._build_macro_card(icon_buttons=actions)
        else:
            raise NotImplementedError()

        self.view = flt.Container(
            content=_content,
            data=self,
        )
        return self.view

    async def toggle_highlight(self, e: Optional[flt.Control] = None) -> None:
        if not self._is_highlighted:
            self.card.current.color = self.highlight_color
        else:
            self.card.current.color = self.card_default_color
        self._is_highlighted = not self._is_highlighted
        await self.card.current.update_async()

    async def reset_highlight(self, e: Optional[flt.Control] = None) -> None:
        if self._is_highlighted:
            await self.toggle_highlight(e=e)


class DraggableTrack(Track):
    def __init__(
        self,
        playlist: Playlist,
        cmd: Union[ConsecutiveCommands, GenericCommand],
    ) -> None:
        super().__init__(cmd=cmd, playlist=playlist)

    def __get_page(self) -> flt.Page:
        """
        flet sets the page attribute to None when using pop to remove Tracks from another control.
        This can be bad. to mitigate this problem, the page from the playlist can be used.
        """
        if self.page:
            return self.page
        elif self.playlist.page:
            return self.playlist.page
        elif GlobalResizeHandler._FLET_PAGE:
            return GlobalResizeHandler._FLET_PAGE
        else:
            raise ValueError()

    def build(self) -> flt.Control:
        if isinstance(self.data, GenericCommand):
            actions: List[flt.Control] = [
                flt.IconButton(
                    icon=flt.icons.DELETE_ROUNDED,
                    tooltip="Delete",
                    on_click=self.__delete,
                ),
                flt.IconButton(
                    icon=flt.icons.EDIT_ROUNDED,
                    tooltip="Edit",
                    on_click=self.__edit_cmd,
                ),
            ]

            _content = self._build_generic_cmd_card(
                icon_buttons=actions,
                text=flt.Text(
                    selectable=True, ref=self.info_text, value=str(self.data)
                ),
            )
        elif isinstance(self.data, list):
            actions: List[flt.Control] = [
                flt.IconButton(
                    icon=flt.icons.DELETE_ROUNDED,
                    tooltip="Delete",
                    on_click=self.__delete,
                ),
                flt.IconButton(
                    icon=flt.icons.EDIT_ROUNDED,
                    tooltip="Edit",
                    on_click=self._view_macro,
                ),
            ]
            _content = self._build_macro_card(icon_buttons=actions)
        else:
            raise NotImplementedError()

        self.view = flt.Container(
            flt.Draggable(
                group=self.playlist.group_id,
                content=flt.DragTarget(
                    group=self.playlist.group_id,
                    content=_content,
                    on_accept=self.drag_accept,
                    # on_will_accept=self.drag_will_accept,
                ),
                data=self,
            )
        )
        return self.view

    # async def drag_will_accept(self, e: flt.ControlEvent) -> None:
    #     drag_target: flt.DragTarget = e.control
    #     print(type(drag_target))
    #     # if isinstance(drag_target.content, DraggableTrack):
    #     drag_target.content.border = flt.border.all(2, flt.colors.RED)
    #     await drag_target.content.update_async()
    #     return

    # async def on_drag_leave(self, e: flt.ControlEvent) -> None:
    #     drag_target: flt.DragTarget = e.control
    #     if isinstance(drag_target.content, DraggableTrack):
    #         drag_target.content.view.border = flt.border.all(0, flt.colors.RED)
    #         await drag_target.content.view.update_async()
    #     return

    async def drag_accept(self, e: flt.DragTargetAcceptEvent) -> None:
        src: flt.Draggable = self.__get_page().get_control(id=e.src_id)  # type: ignore
        trg: flt.DragTarget = e.control.content

        assert src
        assert trg
        if not isinstance(src.data, DraggableTrack):
            return
        if isinstance(trg.data, flt.Container):
            trg.data.opacity = 0
            return await trg.data.update_async()
        if not isinstance(trg.data, DraggableTrack):
            return

        src_track: DraggableTrack = src.data
        trg_track: DraggableTrack = trg.data

        if src_track == trg_track:
            return

        if src_track.playlist != self.playlist:  # guard against multiple playlists
            return

        # swap visual representation
        async with self.playlist._async_lock:
            tracks: List[flt.Control] = self.playlist.track_column.controls
            src_index: int = tracks.index(src_track)
            trg_index: int = tracks.index(trg_track)
            tracks[src_index], tracks[trg_index] = tracks[trg_index], tracks[src_index]

            # swap data representation
            tracks: List[flt.Control] = self.playlist.tracks  # type: ignore type check unable to process inheritance
            src_index: int = tracks.index(src_track)
            trg_index: int = tracks.index(trg_track)

            tracks[src_index], tracks[trg_index] = tracks[trg_index], tracks[src_index]

        await self.playlist.track_column.update_async()
        # return await self.playlist.update_async(update_all=True)

    async def __delete(self, e: flt.ControlEvent) -> None:
        async def __abort(e: flt.ControlEvent) -> None:
            _alert.open = False
            await e.page.update_async()

        async def __accept(e: flt.ControlEvent) -> None:
            await self.playlist.remove_item(self)
            await self.playlist.update_async(update_all=True)
            await self.__get_page().update_async()
            return await __abort(e)

        _alert: flt.AlertDialog = flt.AlertDialog(
            title=flt.Text(
                selectable=True, value="Are you sure you want to delete this element?"
            ),
            actions_alignment=flt.MainAxisAlignment.END,
            actions=[
                flt.ElevatedButton(
                    text="Yes.",
                    on_click=__accept,
                    bgcolor=flt.colors.GREEN,
                    icon=flt.icons.CHECK_OUTLINED,
                    color=flt.colors.WHITE,
                ),
                flt.ElevatedButton(
                    text="No.",
                    on_click=__abort,
                    bgcolor=flt.colors.RED,
                    icon=flt.icons.CANCEL_OUTLINED,
                    color=flt.colors.WHITE,
                ),
            ],
        )

        self.__get_page().dialog = _alert
        _alert.open = True
        await self.__get_page().update_async()

    async def __edit_cmd(self, _e: flt.ControlEvent) -> None:
        assert isinstance(self.data, GenericCommand)

        async def apply_changes(e: flt.ControlEvent) -> None:
            assert self.cmd_editor and isinstance(
                self.cmd_editor, GenericCommandConfigurator
            )
            malformed_params: list[str] = await self.cmd_editor.configure_output()
            for malformed_param in malformed_params:
                ctrl: CustomFletControl = self.cmd_editor.attr_to_ctrl_dict[
                    malformed_param
                ]
                ctrl.view.border = flt.border.all(10, flt.colors.RED)
                await ctrl.update_async()
            if not malformed_params:
                await exit_view(e=e)
                return await self.update_async()

        assert hasattr(_e.page, "expert_mode")

        contents: List[flt.Control] = [
            flt.AppBar(
                title=flt.Text(
                    selectable=True,
                    value=f"Editing Track {self.playlist.tracks.index(self) + 1}",
                ),
                bgcolor=flt.colors.SURFACE_VARIANT,
                actions=[flt.TextButton(text="Save changes.", on_click=apply_changes)],
            ),
        ]

        if not _e.page.expert_mode and (
            self.data.advanced_usage or self.data.needs_gamemode
        ):
            # non-expert users may not edit this command
            contents.append(
                flt.Text(
                    selectable=True,
                    value="Expert mode needs to be enabled to edit this command.",
                )
            )
        else:
            if self.data.sorted_param_list:
                self.cmd_editor = GenericCommandConfigurator(self.data)
                contents.append(self.cmd_editor)
            else:
                contents.append(
                    flt.Text(
                        selectable=True,
                        value="This command does not contain any parameters.",
                    )
                )

        _e.page.views.append(
            flt.View(
                route=f"edit_track_{self.data.uuid}",
                fullscreen_dialog=False,
                padding=0.1 * _e.page.height,
                controls=contents,
            )
        )

        top_view = _e.page.views[-1]
        await _e.page.go_async(route=top_view.route)

    async def update_async(self):
        if isinstance(self.data, GenericCommand):
            # print("yeoh", str(self.data))
            self.info_text.current.value = str(self.data)

            await self.info_text.current.update_async()
            await self.playlist.track_column.update_async()
            await self.__get_page().update_async()
        return await super().update_async()
