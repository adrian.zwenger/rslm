# import asyncio as _asyncio
import asyncio as _asyncio
import logging as _logging
import re as _regex
import typing as _tp
import uuid as _uuid
from copy import deepcopy as _deepcopy
from datetime import datetime as _datetime

import flet as _flt

from rslm._default_hw_config import (
    API_CONFIG_SECTION_ID,
    API_CONFIG_VALID_VALUES_SECTION_ID,
    HARDWARE_CONFIG_SECTION,
)
from rslm.gui.backend import LED_BACKEND

LOGGER: _logging.Logger = _logging.getLogger(__name__)

FLOAT_REGEX: str = r"[-+]?\d*\.\d+|\d+"

H1_FONT_SIZE: int = 30
H2_FONT_SIZE: int = 25
CONTENT_FONT_SIZE: int = 17


class HealthMonitor(_flt.UserControl):
    def __init__(self: "HealthMonitor") -> None:
        super().__init__()
        self.uuid: _uuid.UUID = _uuid.uuid4()
        self.is_mounted: _asyncio.Event = _asyncio.Event()
        self.last_updated: _tp.Optional[_datetime] = None

    def build(self: "HealthMonitor") -> _flt.Column:
        self.content: _flt.Column = _flt.Column(
            controls=[_flt.Text("Detecting System health. Please wait :)")],
            spacing=0,
            expand=True,
        )

        self.view = self.content

        return self.view

    async def __update(self: "HealthMonitor") -> None:
        assert LED_BACKEND, "no backend configured"
        if LED_BACKEND.simulation_mode:
            return
        if not LED_BACKEND.get_monitoring_result():
            return

        try:
            exec_result: _tp.Dict[str, _tp.Any] = _deepcopy(
                LED_BACKEND.monitor_result.get_cmd_result_as_dict(),  # type: ignore
            )
        except Exception as e:
            LOGGER.exception(e)
            return

        # last update?
        try:
            last_execution: _datetime = _datetime.fromtimestamp(
                int(exec_result["timestamp"]),
            )
        except KeyError as _k:
            LOGGER.critical("tec status did not contain a timestamp")
            LOGGER.exception(_k)
            return

        # skip if status remains unchanged
        if self.last_updated and self.last_updated == last_execution:
            return

        self.content.controls.clear()
        self.last_updated = last_execution
        self.content.controls.append(_flt.Text(f"Last update: {last_execution}"))
        self.content.controls.append(_flt.Divider())
        _content_row: _flt.Row = _flt.Row(wrap=True, run_spacing=20)

        # update board health
        try:
            tec_boards: int = self.__hw_config[HARDWARE_CONFIG_SECTION][
                "tec_driver_boards"
            ]
            channels: _tp.List[_tp.Any] = self.__hw_config[API_CONFIG_SECTION_ID][
                "TEC"
            ]["ch"][API_CONFIG_VALID_VALUES_SECTION_ID]
        except KeyError as _k:
            LOGGER.exception(_k)

        tec_in_error_state: bool = False

        for board_id in range(1, tec_boards + 1):
            _new_cell: _tp.List[_flt.Control] = []
            dict_entry: str = f"Board {board_id}"
            board_is_running: bool = (
                dict_entry in exec_result and
                "running" in exec_result[dict_entry]["TEC state"].lower()
            )
            _new_cell.append(
                _flt.Text(
                    f"Tec {dict_entry}",
                    style=_flt.TextStyle(
                        weight=_flt.FontWeight.BOLD,
                        decoration=_flt.TextDecoration.UNDERLINE,
                        size=H1_FONT_SIZE,
                        color=None if board_is_running else _flt.colors.RED,
                    ),
                    color=None if board_is_running else _flt.colors.RED,
                ),
            )

            # get board status:
            _status: _flt.Text = _flt.Text(
                "Status: ",
                size=CONTENT_FONT_SIZE,
                style=_flt.TextStyle(
                    weight=_flt.FontWeight.BOLD,
                    color=None if board_is_running else _flt.colors.RED,
                ),
                color=None if board_is_running else _flt.colors.RED,
                spans=[],
            )

            if dict_entry not in exec_result:
                _status.spans = [
                    _flt.TextSpan(
                        "DISCONNECTED",
                        style=_flt.TextStyle(
                            weight=_flt.FontWeight.BOLD,
                            color=_flt.colors.RED,
                            size=CONTENT_FONT_SIZE,
                        ),
                    ),
                ]
                _new_cell.append(_status)
                _new_cell.append(_flt.Text())
                _content_row.controls.append(_flt.Column(controls=_new_cell))
                tec_in_error_state = True
                continue

            _st_str: str = exec_result[dict_entry]["TEC state"].lower()
            _status.spans = [
                _flt.TextSpan(
                    _st_str.upper(),
                    style=_flt.TextStyle(
                        weight=_flt.FontWeight.BOLD,
                        color=(
                            _flt.colors.RED
                            if ("stopped" in _st_str or not board_is_running)
                            else _flt.colors.GREEN
                        ),
                        size=CONTENT_FONT_SIZE,
                    ),
                ),
            ]
            _new_cell.append(_status)

            for channel_id in channels:
                channel_select: str = f"Channel {channel_id}"
                _new_cell.append(
                    _flt.Text(
                        f"{channel_select}:",
                        style=_flt.TextStyle(
                            weight=_flt.FontWeight.BOLD,
                            decoration=_flt.TextDecoration.UNDERLINE,
                            color=None if board_is_running else _flt.colors.RED,
                        ),
                        color=None if board_is_running else _flt.colors.RED,
                        size=H2_FONT_SIZE,
                    ),
                )

                # LED temps
                target_temp_float: _tp.Union[float, str] = 0.0
                current_temp_float: _tp.Union[float, str] = 0.0
                try:
                    target_temp: str = exec_result[dict_entry][channel_select][
                        "LED temp (wanted)"
                    ]
                    _regex_results = _regex.findall(FLOAT_REGEX, target_temp)
                    if _regex_results:
                        target_temp_float = float(_regex_results[0])
                except KeyError:
                    LOGGER.critical(
                        "Unable to get target LED temp for tec board %s channel %s", board_id, channel_id,
                    )
                    tec_in_error_state = True
                    target_temp_float = "NA"
                try:
                    current_temp: str = exec_result[dict_entry][channel_select][
                        "LED temp (wanted)"
                    ]
                    _regex_results = _regex.findall(FLOAT_REGEX, current_temp)
                    if _regex_results:
                        current_temp_float = float(_regex_results[0])
                except KeyError:
                    LOGGER.critical(
                        "Unable to get current LED temp for tec board %s channel %s", board_id, channel_id,
                    )
                    tec_in_error_state = True
                    current_temp_float = "NA"

                status_text: _flt.Text = _flt.Text(
                    selectable=True,
                    style=_flt.TextStyle(weight=_flt.FontWeight.BOLD),
                    size=CONTENT_FONT_SIZE,
                    value="LED: ",
                    color=None if board_is_running else _flt.colors.RED,
                )

                status_text.spans = [
                    _flt.TextSpan(
                        text=f"{current_temp_float}°C / {target_temp_float}°C",
                        style=_flt.TextStyle(
                            # weight=_flt.FontWeight.BOLD,
                            weight=_flt.FontWeight.NORMAL,
                            color=(
                                _flt.colors.RED
                                if (
                                    isinstance(current_temp_float, str)
                                    or isinstance(target_temp_float, str)
                                    or not board_is_running
                                )
                                else None
                            ),
                            size=CONTENT_FONT_SIZE,
                        ),
                    ),
                ]
                _new_cell.append(status_text)

                # heatsink temp
                status_text: _flt.Text = _flt.Text(
                    selectable=True,
                    size=CONTENT_FONT_SIZE,
                    value="Heatsink: ",
                    color=None if board_is_running else _flt.colors.RED,
                    style=_flt.TextStyle(
                        weight=_flt.FontWeight.BOLD,
                        color=None if board_is_running else _flt.colors.RED,
                    ),
                )
                try:
                    status_text.spans = [
                        _flt.TextSpan(
                            text=exec_result[dict_entry][channel_select][
                                "Heatsink temp"
                            ],
                            style=_flt.TextStyle(
                                size=CONTENT_FONT_SIZE,
                                weight=_flt.FontWeight.NORMAL,
                                color=None if board_is_running else _flt.colors.RED,
                            ),
                        ),
                    ]
                except KeyError:
                    status_text.spans = [
                        _flt.TextSpan(
                            "UNKOWN",
                            style=_flt.TextStyle(
                                weight=_flt.FontWeight.BOLD,
                                color=_flt.colors.RED,
                                size=CONTENT_FONT_SIZE,
                            ),
                        ),
                    ]
                    LOGGER.critical(
                        "Unable to get heatsink temp for tec board %s channel %s", board_id, channel_id,
                    )
                    tec_in_error_state = True
                _new_cell.append(status_text)

                # fan speed
                if (
                    self.page
                    and hasattr(self.page, "expert_mode")
                    and self.page.expert_mode # type: ignore
                ):
                    status_text: _flt.Text = _flt.Text(
                        selectable=True,
                        size=CONTENT_FONT_SIZE,
                        value="Fan Speed: ",
                        style=_flt.TextStyle(
                            weight=_flt.FontWeight.BOLD,
                            color=None if board_is_running else _flt.colors.RED,
                        ),
                        color=None if board_is_running else _flt.colors.RED,
                    )
                    try:
                        status_text.spans = [
                            _flt.TextSpan(
                                text=exec_result[dict_entry][channel_select][
                                    "Fan speed"
                                ],
                                style=_flt.TextStyle(
                                    size=CONTENT_FONT_SIZE,
                                    weight=_flt.FontWeight.NORMAL,
                                    color=None if board_is_running else _flt.colors.RED,
                                ),
                            ),
                        ]
                    except KeyError:
                        status_text.spans = [
                            _flt.TextSpan(
                                "UNKOWN",
                                style=_flt.TextStyle(
                                    weight=_flt.FontWeight.BOLD,
                                    color=_flt.colors.RED,
                                    size=CONTENT_FONT_SIZE,
                                ),
                            ),
                        ]
                        LOGGER.critical(
                            "Unable to get target temp for tec board %s channel %s", board_id, channel_id,
                        )
                        tec_in_error_state = True
                    _new_cell.append(status_text)

            _content_row.controls.append(_flt.Column(controls=_new_cell))

        # add new contents
        if tec_in_error_state:
            self.content.controls.append(
                _flt.Text(
                    value="Something seems wrong. Please check the hardware and the TEC status below.",
                    style=_flt.TextStyle(
                        size=H1_FONT_SIZE,
                        color=_flt.colors.RED,
                    ),
                    color=_flt.colors.RED,
                ),
            )

        self.content.controls.append(_content_row)

    async def __updater(self: "HealthMonitor", update_delay: float = 5) -> None:
        while self.is_mounted.is_set():
            await self.__update()
            self.content.update()
            await _asyncio.sleep(update_delay)

    def did_mount(self: "HealthMonitor") -> None:
        self.is_mounted.set()
        super().did_mount()
        assert self.page
        assert LED_BACKEND, "no backend configured"
        self.__hw_config: _tp.Dict[str, _tp.Any] = _deepcopy(LED_BACKEND.hw_config)
        if LED_BACKEND.simulation_mode and self.content:
            self.content.controls.append(
                _flt.Text("Simulations are always in perfect health : )"),
            )
            return self.content.update()
        self.page.run_task(self.__updater)
        return None

    def will_unmount(self: "HealthMonitor") -> None:
        self.is_mounted.clear()
        self.last_updated = None
        return super().will_unmount()
