import asyncio
import uuid
from inspect import iscoroutinefunction
from typing import Any, Callable, List, Optional, Union

import flet as flt
from flet_core.control import OptionalNumber
from flet_core.ref import Ref
from flet_core.types import (
    AnimationValue,
    OffsetValue,
    ResponsiveNumber,
    RotateValue,
    ScaleValue,
)

import rslm.gui._resize_handler as GlobalResizeHandler

BORDER_COLOR = flt.colors.WHITE70


async def execute_handler(handler: Callable, e: flt.ControlEvent) -> None:
    if iscoroutinefunction(handler):
        await handler(e)
    else:
        handler(e)


class CustomFletControl(flt.UserControl):
    def __init__(
        self,
        tooltip: str,
        ref: Optional[Ref] = None,
        key: Optional[str] = None,
        left: OptionalNumber = None,
        top: OptionalNumber = None,
        right: OptionalNumber = None,
        bottom: OptionalNumber = None,
        height: OptionalNumber = None,
        width: OptionalNumber = None,
        expand: Union[None, bool, int] = None,
        col: Optional[ResponsiveNumber] = None,
        opacity: OptionalNumber = None,
        rotate: RotateValue = None,
        scale: ScaleValue = None,
        offset: OffsetValue = None,
        aspect_ratio: OptionalNumber = None,
        animate_opacity: AnimationValue = None,
        animate_size: AnimationValue = None,
        animate_position: AnimationValue = None,
        animate_rotation: AnimationValue = None,
        animate_scale: AnimationValue = None,
        animate_offset: AnimationValue = None,
        on_animation_end=None,
        visible: Optional[bool] = None,
        disabled: Optional[bool] = None,
        data: Any = None,
    ):
        super().__init__(
            ref=ref,
            key=key,
            width=width,
            height=height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            expand=expand,
            col=col,
            opacity=opacity,
            rotate=rotate,
            scale=scale,
            offset=offset,
            aspect_ratio=aspect_ratio,
            animate_opacity=animate_opacity,
            animate_size=animate_size,
            animate_position=animate_position,
            animate_rotation=animate_rotation,
            animate_scale=animate_scale,
            animate_offset=animate_offset,
            on_animation_end=on_animation_end,
            visible=visible,
            disabled=disabled,
            data=data,
        )
        self._async_lock = asyncio.Lock()
        self.uuid: uuid.UUID = uuid.uuid4()
        self.tooltip: str = tooltip
        self.value: Any
        self.view: flt.Container


# TODO: Consider improving floats and integers
class IntegerInput(CustomFletControl):
    def __init__(
        self,
        #
        # IntegerInput-specific
        #
        default_val: int,
        tooltip: str,
        int_range: Optional[List[int]] = None,
        valid_vals: Optional[List[int]] = None,
        on_change: Optional[Callable] = None,
        description: Optional[str] = None,
        #
        # super()-specific
        #
        ref: Optional[Ref] = None,
        key: Optional[str] = None,
        left: OptionalNumber = None,
        top: OptionalNumber = None,
        right: OptionalNumber = None,
        bottom: OptionalNumber = None,
        height: OptionalNumber = None,
        width: OptionalNumber = None,
        expand: Union[None, bool, int] = None,
        col: Optional[ResponsiveNumber] = None,
        opacity: OptionalNumber = None,
        rotate: RotateValue = None,
        scale: ScaleValue = None,
        offset: OffsetValue = None,
        aspect_ratio: OptionalNumber = None,
        animate_opacity: AnimationValue = None,
        animate_size: AnimationValue = None,
        animate_position: AnimationValue = None,
        animate_rotation: AnimationValue = None,
        animate_scale: AnimationValue = None,
        animate_offset: AnimationValue = None,
        on_animation_end=None,
        visible: Optional[bool] = None,
        disabled: Optional[bool] = None,
        data: Any = None,
    ):
        super().__init__(
            tooltip=tooltip,
            ref=ref,
            key=key,
            width=width,
            height=height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            expand=expand,
            col=col,
            opacity=opacity,
            rotate=rotate,
            scale=scale,
            offset=offset,
            aspect_ratio=aspect_ratio,
            animate_opacity=animate_opacity,
            animate_size=animate_size,
            animate_position=animate_position,
            animate_rotation=animate_rotation,
            animate_scale=animate_scale,
            animate_offset=animate_offset,
            on_animation_end=on_animation_end,
            visible=visible,
            disabled=disabled,
            data=data,
        )

        if valid_vals and int_range:
            raise AssertionError(
                "Please pass a sequence of valid values or integer bounds. Not both"
            )

        assert default_val is not None and (
            isinstance(default_val, int) or isinstance(default_val, float)
        )

        self.default: int = default_val
        self.__range: Optional[List[int]] = int_range
        self.__valids: Optional[List[int]] = valid_vals
        self.value: Optional[int] = default_val
        self.on_change: Optional[Callable] = on_change
        self.description: Optional[str] = description

    def __int_slider(self) -> flt.Control:
        assert self.__range
        assert len(self.__range) == 2

        self.__slider: flt.Slider = flt.Slider(
            min=self.__range[0],
            max=self.__range[1],
            divisions=max(self.__range) - min(self.__range),
            value=(
                int((max(self.__range) - min(self.__range)) * 0.5)
                if self.default is None
                else self.default
            ),
            # width=self.width * 0.85,
            label="{value}",  # flet internally replaces {value} with slider value
        )
        # self.__slider.width = self.width * 0.85

        async def _slider_change(e: flt.ControlEvent, clear_val: bool = False) -> None:
            async with self._async_lock:
                if not clear_val:
                    self.value = int(e.control.value)

            if self.on_change:
                await execute_handler(self.on_change, e)
            return await e.control.update_async()

        self.__slider.on_change = _slider_change

        return self.__slider

    def __int_slider_broken(self) -> List[flt.Control]:  # noqa F811
        assert self.__range
        assert len(self.__range) == 2

        if not self.width:
            if self.page:
                self.width = 0.85 * self.page.width
            else:
                assert GlobalResizeHandler._FLET_PAGE is not None
                self.width = 0.85 * GlobalResizeHandler._FLET_PAGE.width

        self.__slider: flt.Slider = flt.Slider(
            min=self.__range[0],
            max=self.__range[1],
            divisions=max(self.__range) - min(self.__range),
            value=int((max(self.__range) - min(self.__range)) * 0.5),
            # width=self.width * 0.85,
            label="{value}",  # flet internally replaces {value} with slider value
        )
        self.__slider.width = self.width * 0.85

        self.__field: flt.TextField = flt.TextField(
            value=str(int((max(self.__range) - min(self.__range)) * 0.5)),
            # width=self.width * 0.15,
            on_submit=None,
            border_color=BORDER_COLOR,
            expand=True,
        )
        self.__field.width = self.width * 0.15

        async def _slider_change(e: flt.ControlEvent, clear_val: bool = False) -> None:
            async with self._async_lock:
                if not clear_val:
                    self.__field.value = str(int(e.control.value))
                    self.value = int(e.control.value)

                self.__field.value = str(self.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            return await self.__field.update_async()
            # return await e.page.update_async()

        async def _field_change(e: flt.ControlEvent) -> None:
            try:
                int(e.control.value)
            except (TypeError, ValueError):
                # return await _slider_change(e=e, clear_val=True)
                return

            async with self._async_lock:
                assert self.__range

                if int(e.control.value) > int(max(self.__range)):
                    e.control.value = str(int(max(self.__range)))
                    await self.__field.update_async()
                elif int(e.control.value) < int(min(self.__range)):
                    e.control.value = str(int(min(self.__range)))
                    await self.__field.update_async()

                self.__slider.value = int(e.control.value)
                self.value = int(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            return await self.__slider.update_async()
            # return await e.page.update_async()

        self.__slider.on_change = _slider_change
        self.__field.on_change = _field_change

        return [self.__slider, self.__field]

    def __int_dropdown(self) -> flt.Control:
        assert self.__valids

        self.__dropdown: flt.Dropdown = flt.Dropdown(
            options=[flt.dropdown.Option(key=_) for _ in self.__valids],  # type: ignore
            value=str(self.default),
            border_color=BORDER_COLOR,
            expand=True,
        )

        async def __dropdown_change(e: flt.ControlEvent) -> None:
            async with self._async_lock:
                self.value = int(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        self.__dropdown.on_change = __dropdown_change

        return self.__dropdown

    def __int_unbounded_field(self) -> flt.Control:
        async def __field_change(e: flt.ControlEvent) -> None:
            if not e.control.value:
                async with self._async_lock:
                    self.value = None
                    return

            try:
                if e.control.value == "-":
                    return
                int(e.control.value)
            except (TypeError, ValueError):
                self.__unbounded_field.value = str(self.value)
                await self.__unbounded_field.update_async()
                return await e.page.update_async()

            async with self._async_lock:
                self.value = int(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        async def __field_submit(e: flt.ControlEvent) -> None:
            e.control.value = int(e.control.value)
            await e.control.update_async()
            return await e.page.update_async()

        self.__unbounded_field: flt.TextField = flt.TextField(
            value=str(self.value), border_color=BORDER_COLOR, expand=True
        )
        self.__unbounded_field.on_change = __field_change
        self.__unbounded_field.on_submit = __field_submit

        return self.__unbounded_field

    def build(self) -> flt.Control:
        if self.__range:
            handler: Callable = self.__int_slider
        elif self.__valids:
            handler: Callable = self.__int_dropdown
        else:
            handler: Callable = self.__int_unbounded_field

        view = handler()
        # view = flt.Row(controls=[view])

        view = flt.Tooltip(message=self.tooltip, content=view)
        if self.description:
            view = flt.Column(
                controls=[flt.Text(selectable=True, value=self.description), view],
                alignment=flt.MainAxisAlignment.CENTER,
                # expand=True,
                # wrap=True,
            )
        view = flt.Container(
            expand=True,
            content=view,
        )
        self.view = view
        return self.view

    def did_mount(self) -> None:
        async def __resize_int_slider_broken(e: flt.ControlEvent) -> None:
            if self.width:
                self.__slider.width = self.width * 0.845
                self.__field.width = self.width * 0.145
            await self.__slider.update_async()
            return await self.__field.update_async()

        async def __resize_int_slider(e: flt.ControlEvent) -> None:
            if self.width:
                self.__slider.width = self.width * 0.845
            return await self.__slider.update_async()

        async def __resize_int_dropdown(e: flt.ControlEvent) -> None:
            if self.width:
                self.__dropdown.width = self.width * 0.99
            return await self.__dropdown.update_async()

        async def __resize_int_field(e: flt.ControlEvent) -> None:
            if self.width:
                self.__unbounded_field.width = self.width * 0.99
            return await self.__unbounded_field.update_async()

        if self.__range:
            GlobalResizeHandler.add_resize_handler(
                event_id=self.uuid, handler=__resize_int_slider
            )
        elif self.__valids:
            GlobalResizeHandler.add_resize_handler(
                event_id=self.uuid,
                handler=__resize_int_dropdown,
            )
        else:
            GlobalResizeHandler.add_resize_handler(
                event_id=self.uuid, handler=__resize_int_field
            )
        return super().did_mount()

    def will_unmount(self) -> None:
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        return super().will_unmount()


class FloatInput(CustomFletControl):
    def __init__(
        self,
        #
        # FloatInput-specific
        #
        default_val: float,
        tooltip: str,
        float_range: Optional[List[float]] = None,
        valid_vals: Optional[List[float]] = None,
        on_change: Optional[Callable] = None,
        description: Optional[str] = None,
        #
        # super()-specific
        #
        ref: Optional[Ref] = None,
        key: Optional[str] = None,
        left: OptionalNumber = None,
        top: OptionalNumber = None,
        right: OptionalNumber = None,
        bottom: OptionalNumber = None,
        height: OptionalNumber = None,
        width: OptionalNumber = None,
        expand: Union[None, bool, int] = None,
        col: Optional[ResponsiveNumber] = None,
        opacity: OptionalNumber = None,
        rotate: RotateValue = None,
        scale: ScaleValue = None,
        offset: OffsetValue = None,
        aspect_ratio: OptionalNumber = None,
        animate_opacity: AnimationValue = None,
        animate_size: AnimationValue = None,
        animate_position: AnimationValue = None,
        animate_rotation: AnimationValue = None,
        animate_scale: AnimationValue = None,
        animate_offset: AnimationValue = None,
        on_animation_end=None,
        visible: Optional[bool] = None,
        disabled: Optional[bool] = None,
        data: Any = None,
    ):
        super().__init__(
            tooltip=tooltip,
            ref=ref,
            key=key,
            width=width,
            height=height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            expand=expand,
            col=col,
            opacity=opacity,
            rotate=rotate,
            scale=scale,
            offset=offset,
            aspect_ratio=aspect_ratio,
            animate_opacity=animate_opacity,
            animate_size=animate_size,
            animate_position=animate_position,
            animate_rotation=animate_rotation,
            animate_scale=animate_scale,
            animate_offset=animate_offset,
            on_animation_end=on_animation_end,
            visible=visible,
            disabled=disabled,
            data=data,
        )
        if valid_vals and float_range:
            raise AssertionError(
                "Please pass a sequence of valid values or integer bounds. Not both"
            )

        assert isinstance(default_val, float) or isinstance(default_val, int)

        self.default: float = default_val
        self.__range: Optional[List[float]] = float_range
        self.__valids: Optional[List[float]] = valid_vals
        self.value: Optional[float] = float(default_val)
        self.on_change: Optional[Callable] = on_change
        self.description: Optional[str] = description

    def __float_dropdown(self) -> flt.Control:
        assert self.__valids

        self.__dropdown: flt.Dropdown = flt.Dropdown(
            options=[flt.dropdown.Option(key=str(float(_))) for _ in self.__valids],
            value=str(float(self.default)),
            border_color=BORDER_COLOR,
            expand=True,
        )

        async def __dropdown_change(e: flt.ControlEvent) -> None:
            async with self._async_lock:
                self.value = float(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        self.__dropdown.on_change = __dropdown_change

        return self.__dropdown

    def __float_bounded_textfield(self) -> flt.Control:
        async def __field_change(e: flt.ControlEvent) -> None:
            if not e.control.value:
                async with self._async_lock:
                    self.value = None
                    return

            try:
                if e.control.value == "-":
                    return
                float(e.control.value)
            except (TypeError, ValueError):
                self.__float_field.value = str(self.value)
                await self.__float_field.update_async()
                return await e.page.update_async()

            assert self.__range

            async with self._async_lock:
                if float(e.control.value) > float(max(self.__range)):
                    e.control.value = float(max(self.__range))
                    await e.control.update_async()
                elif float(e.control.value) < float(min(self.__range)):
                    e.control.value = int(min(self.__range))
                    await e.control.update_async()
                else:
                    self.value = float(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        async def __field_submit(e: flt.ControlEvent) -> None:
            e.control.value = float(e.control.value)
            await e.control.update_async()
            return await e.page.update_async()

        self.__float_field: flt.TextField = flt.TextField(
            value=str(self.value), border_color=BORDER_COLOR, expand=True
        )
        self.__float_field.on_change = __field_change
        self.__float_field.on_submit = __field_submit

        return self.__float_field

    def __float_unbounded_textfield(self) -> flt.Control:
        async def __field_change(e: flt.ControlEvent) -> None:
            if not e.control.value:
                async with self._async_lock:
                    self.value = None
                    return

            try:
                if e.control.value == "-":  # allow negative numbers
                    return
                float(e.control.value)
            except (TypeError, ValueError):
                self.__float_field.value = str(self.value)
                await self.__float_field.update_async()
                return await e.page.update_async()

            async with self._async_lock:
                self.value = float(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        async def __field_submit(e: flt.ControlEvent) -> None:
            e.control.value = float(e.control.value)
            await e.control.update_async()
            return await e.page.update_async()

        self.__float_field: flt.TextField = flt.TextField(
            value=str(self.value), border_color=BORDER_COLOR, expand=True
        )
        self.__float_field.on_change = __field_change
        self.__float_field.on_submit = __field_submit

        return self.__float_field

    def build(self) -> flt.Control:
        if self.__range:
            view = self.__float_bounded_textfield()
        elif self.__valids:
            view = self.__float_dropdown()
        else:
            view = self.__float_unbounded_textfield()

        # view = flt.Row(view)

        view = flt.Tooltip(message=self.tooltip, content=view)
        if self.description:
            view = flt.Column(
                controls=[flt.Text(selectable=True, value=self.description), view]
            )

        self.view = flt.Container(
            expand=True,
            content=view,
            # bgcolor=flt.colors.AMBER,
            # border_radius=flt.border_radius.all(5),
        )
        return self.view

    def did_mount(self) -> None:
        async def __resize_float_text_field(e: flt.ControlEvent) -> None:
            if self.width:
                self.__float_field.width = self.width * 0.99
            return await self.__float_field.update_async()

        async def __resize_float_dropdown(e: flt.ControlEvent) -> None:
            if self.width:
                self.__dropdown.width = self.width * 0.99
            return await self.__dropdown.update_async()

        if self.__valids:
            GlobalResizeHandler.add_resize_handler(
                event_id=self.uuid,
                handler=__resize_float_dropdown,
            )
        else:
            GlobalResizeHandler.add_resize_handler(
                event_id=self.uuid, handler=__resize_float_text_field
            )
        return super().did_mount()

    def will_unmount(self) -> None:
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        return super().will_unmount()


class BoolInput(CustomFletControl):
    def __init__(
        self,
        #
        # BoolInput-specific
        #
        default_val: bool,
        tooltip: str,
        on_change: Optional[Callable] = None,
        description: Optional[str] = None,
        #
        # super()-specific
        #
        ref: Optional[Ref] = None,
        key: Optional[str] = None,
        left: OptionalNumber = None,
        top: OptionalNumber = None,
        right: OptionalNumber = None,
        bottom: OptionalNumber = None,
        height: OptionalNumber = None,
        width: OptionalNumber = None,
        expand: Union[None, bool, int] = None,
        col: Optional[ResponsiveNumber] = None,
        opacity: OptionalNumber = None,
        rotate: RotateValue = None,
        scale: ScaleValue = None,
        offset: OffsetValue = None,
        aspect_ratio: OptionalNumber = None,
        animate_opacity: AnimationValue = None,
        animate_size: AnimationValue = None,
        animate_position: AnimationValue = None,
        animate_rotation: AnimationValue = None,
        animate_scale: AnimationValue = None,
        animate_offset: AnimationValue = None,
        on_animation_end=None,
        visible: Optional[bool] = None,
        disabled: Optional[bool] = None,
        data: Any = None,
    ):
        super().__init__(
            tooltip=tooltip,
            ref=ref,
            key=key,
            width=width,
            height=height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            expand=expand,
            col=col,
            opacity=opacity,
            rotate=rotate,
            scale=scale,
            offset=offset,
            aspect_ratio=aspect_ratio,
            animate_opacity=animate_opacity,
            animate_size=animate_size,
            animate_position=animate_position,
            animate_rotation=animate_rotation,
            animate_scale=animate_scale,
            animate_offset=animate_offset,
            on_animation_end=on_animation_end,
            visible=visible,
            disabled=disabled,
            data=data,
        )
        assert default_val is not None and isinstance(default_val, bool)
        self.default: int = default_val
        self.value: Optional[bool] = default_val
        self.on_change: Optional[Callable] = on_change
        self.description: Optional[str] = description

    def build(self) -> flt.Control:
        self.__check: flt.Checkbox = flt.Checkbox()

        async def __update_val(e: flt.ControlEvent) -> None:
            async with self._async_lock:
                self.value = bool(e.control.value)
            if self.on_change:
                await execute_handler(self.on_change, e)
            await self.update_async()
            await e.page.update_async()

        self.__check.on_change = __update_val
        view = self.__check

        # view = flt.Row([view])
        view = flt.Tooltip(message=self.tooltip, content=view)

        if self.description:
            view = flt.Row(
                controls=[flt.Text(selectable=True, value=self.description), view]
            )

        self.view = flt.Container(
            expand=True,
            content=view,
            # bgcolor=flt.colors.AMBER,
            # border_radius=flt.border_radius.all(5),
        )
        return self.view

    def did_mount(self):
        return super().did_mount()

    def will_unmount(self):
        return super().will_unmount()


class StringInput(CustomFletControl):
    def __init__(
        self,
        #
        # StringInput-specific
        #
        default_val: str,
        tooltip: str,
        valid_vals: Optional[List[int]] = None,
        on_change: Optional[Callable] = None,
        description: Optional[str] = None,
        #
        # super()-specific
        #
        ref: Optional[Ref] = None,
        key: Optional[str] = None,
        left: OptionalNumber = None,
        top: OptionalNumber = None,
        right: OptionalNumber = None,
        bottom: OptionalNumber = None,
        height: OptionalNumber = None,
        width: OptionalNumber = None,
        expand: Union[None, bool, int] = None,
        col: Optional[ResponsiveNumber] = None,
        opacity: OptionalNumber = None,
        rotate: RotateValue = None,
        scale: ScaleValue = None,
        offset: OffsetValue = None,
        aspect_ratio: OptionalNumber = None,
        animate_opacity: AnimationValue = None,
        animate_size: AnimationValue = None,
        animate_position: AnimationValue = None,
        animate_rotation: AnimationValue = None,
        animate_scale: AnimationValue = None,
        animate_offset: AnimationValue = None,
        on_animation_end=None,
        visible: Optional[bool] = None,
        disabled: Optional[bool] = None,
        data: Any = None,
    ):
        super().__init__(
            tooltip=tooltip,
            ref=ref,
            key=key,
            width=width,
            height=height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            expand=expand,
            col=col,
            opacity=opacity,
            rotate=rotate,
            scale=scale,
            offset=offset,
            aspect_ratio=aspect_ratio,
            animate_opacity=animate_opacity,
            animate_size=animate_size,
            animate_position=animate_position,
            animate_rotation=animate_rotation,
            animate_scale=animate_scale,
            animate_offset=animate_offset,
            on_animation_end=on_animation_end,
            visible=visible,
            disabled=disabled,
            data=data,
        )
        if default_val:
            assert isinstance(default_val, str)

        self.default: str = default_val
        self.__valids: Optional[List[int]] = valid_vals

        if self.__valids and default_val:
            assert default_val in self.__valids

        self.value: Optional[str] = default_val
        self.on_change: Optional[Callable] = on_change
        self.description: Optional[str] = description

    def __init_dropdown(self) -> flt.Control:
        assert self.__valids
        self.__dropdown: flt.Dropdown = flt.Dropdown(
            options=[flt.dropdown.Option(key=_) for _ in self.__valids if _],  # type: ignore
            value=self.value,
            border_color=BORDER_COLOR,
            expand=True,
        )

        async def _dropdown_change(e: flt.ControlEvent) -> None:
            async with self._async_lock:
                self.value = e.control.value
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        self.__dropdown.on_change = _dropdown_change
        return self.__dropdown

    def __init_field(self) -> flt.Control:
        self.__field: flt.TextField = flt.TextField(
            value=self.value, border_color=BORDER_COLOR, expand=True
        )

        async def __field_change(e: flt.ControlEvent) -> None:
            async with self._async_lock:
                if not e.control.value:
                    self.value = None
                else:
                    self.value = e.control.value
            if self.on_change:
                await execute_handler(self.on_change, e)
            await e.page.update_async()

        async def __field_submit(e: flt.ControlEvent) -> None:
            await e.control.update_async()
            return await e.page.update_async()

        self.__field: flt.TextField = flt.TextField(
            value=self.value, border_color=BORDER_COLOR, expand=True
        )
        self.__field.on_change = __field_change
        self.__field.on_submit = __field_submit
        return self.__field

    def build(self) -> flt.Control:
        view = self.__init_dropdown() if self.__valids else self.__init_field()

        # view = flt.Row([view])
        view = flt.Tooltip(
            message=self.tooltip,
            content=view,
        )

        if self.description:
            view = flt.Column(
                controls=[flt.Text(selectable=True, value=self.description), view],
                expand=True,
            )

        self.view = flt.Container(
            expand=True,
            content=view,
        )
        return self.view

    def did_mount(self) -> None:
        async def __resize_str_field(e: flt.ControlEvent) -> None:
            if self.width:
                self.__field.width = self.width * 0.99
            return await self.__field.update_async()

        async def __resize_str_dropdown(e: flt.ControlEvent) -> None:
            if self.width:
                self.__dropdown.width = self.width * 0.99
            return await self.__dropdown.update_async()

        GlobalResizeHandler.add_resize_handler(
            event_id=self.uuid,
            handler=__resize_str_field if not self.__valids else __resize_str_dropdown,
        )
        return super().did_mount()

    def will_unmount(self) -> None:
        GlobalResizeHandler.remove_resize_handler(event_id=self.uuid)
        return super().will_unmount()
