# import asyncio as _asyncio
import importlib as _importlib
import typing as _tp

import flet as _flt
from flet import NavigationRailDestination as _NavigationRailDestination
from flet import icons as _icons

import rslm.gui._resize_handler as __GlobalResizeHandler

runs_as_webapp: bool = False
UPLOAD_DIR: str = "./.uploads"

""" DEFINE ROUTES HERE

    A route is a string which is appended to the end of the URL of the WebApp.
    Please make sure that the strings are unique.
"""
APP_ROOT_ROUTE: str = "/"
__MONITOR_ROUTE: str = APP_ROOT_ROUTE + "monitor"
__PLAYLIST_CREATOR_ROUTE: str = APP_ROOT_ROUTE + "playlist_creator"


# MAP ROUTES TO THEIR PYTHON MODULE
ALL_ROUTES: _tp.Dict[str, str] = {
    APP_ROOT_ROUTE: "rslm.gui.routes.root",
    __MONITOR_ROUTE: "rslm.gui.routes.monitor",
    __PLAYLIST_CREATOR_ROUTE: "rslm.gui.routes.playlist_creator",
}
"""
    This dictionary maps routes to their respective Python modules in which implements the WebApp
    View of that route.

    The modules need to implement the following function that returns the Flutter view as a flet.View instance.
    Furthermore, contents of this View need to be a flt.Row. This Row-element then contains all other  elements.
    .. code-block:: python
        import flet
        create_view(page: flet.Page, route: str) -> flet.View:
            view: flet.View = flet.View(
                route=route,
                controls=[
                    flet.Row(
                        expand=True,  # Make Row span whole page
                        controls=[
                            ...
                            # your flutter components go here.
                        ]
                    )
                ],
            )
        return view
"""


# default icon if no icon is provided for each destination
DEFAULT_ICON = _icons.ERROR_OUTLINE

# Map which icon to use for each destination
ROUTE_NAVIGATIONRAIL_DESTINATION_ICONS: _tp.Dict[str, str] = {
    APP_ROOT_ROUTE: _icons.MAPS_HOME_WORK_OUTLINED,
    __MONITOR_ROUTE: _icons.TERMINAL_OUTLINED,
    __PLAYLIST_CREATOR_ROUTE: _icons.EDIT_OUTLINED,
}


async def change_route(
    e: _tp.Optional[_flt.RouteChangeEvent] = None,
    new_route: _tp.Optional[str] = None,
    page: _tp.Optional[_flt.Page] = None,
) -> None:
    if e:
        _new_route = e.route
        _page: _flt.Page = e.page
    else:
        assert (
            new_route and page
        ), "when changing route manually, a route (str) and the flet.Page needs to be passed"
        _new_route: str = new_route
        _page: _flt.Page = page

    assert hasattr(_page, "ROUTE_TO_VIEWS")
    ROUTE_TO_VIEWS: _tp.Dict[str, _flt.View] = getattr(_page, "ROUTE_TO_VIEWS")
    assert _new_route in ROUTE_TO_VIEWS
    assert ROUTE_TO_VIEWS[_new_route]
    _page.views.clear()
    _page.views.append(
        ROUTE_TO_VIEWS[_new_route]
    )  # add it to top. top views are the one rendered
    await _page.go_async(_new_route)
    # await _asyncio.sleep(0.3)
    if hasattr(ROUTE_TO_VIEWS[_new_route], "async_rebuild_player"):
        # rebuild any playlist player elements if they are registered in the view
        await getattr(ROUTE_TO_VIEWS[_new_route], "async_rebuild_player")()
    try:
        # update selected navigationrail target if it exists.
        assert isinstance(
            ROUTE_TO_VIEWS[_new_route].controls[0].controls[0], _flt.NavigationRail
        )
        ROUTE_TO_VIEWS[_new_route].controls[0].controls[0].selected_index = list(  # type: ignore
            ROUTE_TO_VIEWS
        ).index(
            _new_route
        )
    except (IndexError, AssertionError, ValueError, KeyError):
        pass

    _page.update()


async def register_routes(page: _flt.Page) -> None:
    rail = create_navigation_rail(page, select_idx=None)
    ROUTE_TO_VIEWS: _tp.Dict[str, _flt.View] = {}
    for route, module_fqdn in ALL_ROUTES.items():
        module = _importlib.import_module(module_fqdn)
        assert hasattr(
            module, "create_view"
        ), "Page views must implement the'create_view' method."
        view: _flt.View = module.create_view(page, route)
        assert (
            view.controls and len(view.controls) == 1
        ), "Views need to contain exactly one control"
        assert isinstance(
            view.controls[0], _flt.Row
        ), f"Expecting that views contain a Row. But got: {type(view.controls[0])}"
        if rail not in view.controls[0].controls:
            # adding a the navigation rail to the left side of the view
            view.controls[0].controls.insert(0, rail)
        # registering view globally
        ROUTE_TO_VIEWS[route] = view

    page.views.insert(0, ROUTE_TO_VIEWS[APP_ROOT_ROUTE])
    setattr(page, "ROUTE_TO_VIEWS", ROUTE_TO_VIEWS)


async def exit_view(e: _tp.Union[_flt.ViewPopEvent, _flt.ControlEvent]) -> None:
    """This Callable is to be used to handle View changes."""
    _page: _flt.Page = e.page
    popped: _flt.View = _page.views.pop()
    if popped.route in ALL_ROUTES:
        """Views which are essentials parts of the app and are not dynamically created by the app
        user are not to be deleted"""
        _page.views.append(popped)

    if _page.views:
        top_view = _page.views[-1]
        if not top_view.route:
            top_view.route = "/error"
        await _page.go_async(route=top_view.route)
        await top_view.update_async()
    await _page.update_async()


async def __on_rail_change(e: _flt.ControlEvent) -> None:
    """This function is called by flet on NavigationRail press events. It handles the route change
    and facilitates the rendering of the correct App View
    """
    assert isinstance(e.control, _flt.NavigationRail)
    assert e.control.destinations
    assert e.control.selected_index is not None
    assert hasattr(e.page, "ROUTE_NAVIGATIONRAIL_DESTINATIONS")

    ROUTE_NAVIGATIONRAIL_DESTINATIONS: _tp.Dict[_NavigationRailDestination, str] = (
        getattr(e.page, "ROUTE_NAVIGATIONRAIL_DESTINATIONS")
    )
    selected_destination: _flt.NavigationRailDestination = e.control.destinations[
        e.control.selected_index
    ]

    await e.page.go_async(ROUTE_NAVIGATIONRAIL_DESTINATIONS[selected_destination])
    await __GlobalResizeHandler.global_resize_handler(e)
    return await e.page.update_async()


def create_navigation_rail(
    page: _flt.Page, select_idx: _tp.Optional[int] = 0
) -> _flt.NavigationRail:
    expert_mode_indicator: _flt.Control = _flt.Text(
        selectable=True, value="Expert\nMode" if page.expert_mode else "Normal\nMode"  # type: ignore
    )

    async def __expert_mode_state_change(e: _flt.ControlEvent):
        assert hasattr(e.page, "expert_mode")
        e.page.expert_mode = not e.page.expert_mode
        expert_mode_indicator.value = (
            "Expert\nMode" if e.page.expert_mode else "Normal\nMode"
        )
        await e.page.update_async()

    expert_mode_switch: _flt.Control = _flt.Switch(
        label_position=_flt.LabelPosition.RIGHT,
        value=page.expert_mode,  # type: ignore
    )
    expert_mode_switch.on_change = __expert_mode_state_change

    destinations: _tp.List[_NavigationRailDestination] = []
    ROUTE_NAVIGATIONRAIL_DESTINATIONS: _tp.Dict[_NavigationRailDestination, str] = {}
    for route in ALL_ROUTES:
        try:
            icon = ROUTE_NAVIGATIONRAIL_DESTINATION_ICONS[route]
        except KeyError:
            icon = DEFAULT_ICON

        destination: _NavigationRailDestination = _NavigationRailDestination(
            icon=icon,
            selected_icon=icon,
            # label=route,
        )
        ROUTE_NAVIGATIONRAIL_DESTINATIONS[destination] = route
        destinations.append(destination)

    setattr(
        page, "ROUTE_NAVIGATIONRAIL_DESTINATIONS", ROUTE_NAVIGATIONRAIL_DESTINATIONS
    )

    rail: _flt.NavigationRail = _flt.NavigationRail(
        selected_index=select_idx,
        label_type=_flt.NavigationRailLabelType.ALL,
        min_width=10,
        extended=False,
        min_extended_width=10,
        trailing=_flt.Column(controls=[expert_mode_switch, expert_mode_indicator]),
        destinations=destinations,
    )

    rail.on_change = __on_rail_change

    return rail
