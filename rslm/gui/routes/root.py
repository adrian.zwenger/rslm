import uuid as _uuid

import flet as _flt

import rslm.gui._resize_handler as __GlobalResizeHandler
from rslm.gui.backend import LED_BACKEND
from rslm.gui.components._health_monitor import HealthMonitor
from rslm.gui.components._player import PlayerControls
from rslm.gui.components._playlist_viewer import Playlist


def create_view(page: _flt.Page, route: str) -> _flt.View:
    assert LED_BACKEND
    playlist = Playlist(
        group=str(_uuid.uuid4()), edit_mode=False, title="Currently Playing"
    )
    playlist_controller: PlayerControls = PlayerControls(playlist=playlist)
    health_monitor: HealthMonitor = HealthMonitor()

    button_container = _flt.Container(
        content=playlist_controller,
        height=70,
    )

    playlist_container = _flt.Container(
        content=playlist,
        height=page.height - button_container.height - 10,  # type: ignore
        # expand=False
    )

    right_side: _flt.Control = _flt.Column(
        height=page.height,
        width=page.width * 0.46,
        # expand=False,
        controls=[
            playlist_container,
            button_container,
        ],
    )

    left_side: _flt.Control = _flt.Column(
        height=page.height,
        width=page.width * 0.46,
        scroll=_flt.ScrollMode.ADAPTIVE,
        controls=[
            health_monitor,
        ],
    )

    async def page_resize(e: _flt.ControlEvent) -> None:
        if e.page.route == "/":
            left_side.height = right_side.height = e.page.height
            left_side.width = e.page.width * 0.46
            right_side.width = e.page.width * 0.46

            playlist_container.height = e.page.height - button_container.height - 10
            # button_container.height = e.page.height * 0.1

            await playlist_container.update_async()
            await button_container.update_async()
            await left_side.update_async()
            await right_side.update_async()
        return

    __GlobalResizeHandler.add_resize_handler(
        event_id="main_page_resizer", handler=page_resize
    )

    content_pane = _flt.Column(
        expand=True,
        spacing=0,
        alignment=_flt.MainAxisAlignment.CENTER,
        controls=[
            _flt.Row(
                spacing=0,
                expand=True,
                controls=[
                    left_side,
                    right_side,
                ],
            ),
        ],
    )
    root_view: _flt.View = _flt.View(
        route=route,
        controls=[
            _flt.Row(
                expand=True,
                controls=[content_pane],
            )
        ],
    )
    setattr(root_view, "async_rebuild_player", playlist_controller.rebuild)
    return root_view
