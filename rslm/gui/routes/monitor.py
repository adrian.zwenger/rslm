import asyncio as _asyncio
import time as _time
import typing as _tp

import flet as _flt

import rslm.gui._resize_handler as __GlobalResizeHandler
from rslm.gui.backend import LED_BACKEND


class MsgPersist:
    def __init__(self) -> None:
        self.data = []

    def add(self, msg):
        self.data.append(msg)

    def pop(self, index):
        self.data.pop(index)

    def set(self, data):
        self.data = data

    def get(self):
        return self.data


MSG_PERSIST: MsgPersist = MsgPersist()


class _SubmitCmd(_flt.UserControl):
    def __init__(self) -> None:
        super().__init__()
        self.txt_field: _flt.Ref[_flt.TextField] = _flt.Ref()
        self.output: _flt.Ref[_flt.Text] = _flt.Ref()

    def build(self) -> _flt.Control:
        input_field: _flt.TextField = _flt.TextField(label="Command")
        self.txt_field.current = input_field
        self.output.current = _flt.Text(
            selectable=True,
        )
        submit_btn: _flt.FilledButton = _flt.FilledButton(
            "Execute", on_click=self.__execute
        )
        self.view = _flt.Column(
            expand=True,
            alignment=_flt.MainAxisAlignment.START,
            controls=[
                input_field,
                submit_btn,
                _flt.Column(
                    scroll=_flt.ScrollMode.ADAPTIVE, controls=[self.output.current]
                ),
            ],
        )
        return self.view

    async def __execute(self, e: _flt.ControlEvent) -> None:
        assert hasattr(e.page, "expert_mode")
        assert LED_BACKEND

        if not getattr(e.page, "expert_mode"):
            dlg = _flt.AlertDialog(
                title=_flt.Text(
                    selectable=True,
                    value="Enable Expert Mode to execute commands directly.",
                )
            )
            dlg.open = True
            e.page.dialog = dlg
            return await e.page.update_async()

        time: str = _time.ctime()
        cmd_str: _tp.Optional[str] = self.txt_field.current.value
        if not cmd_str:
            dlg = _flt.AlertDialog(
                title=_flt.Text(selectable=True, value="No command entered.")
            )
            dlg.open = True
            e.page.dialog = dlg
            return await e.page.update_async()

        result: str = await LED_BACKEND.execute_interrupting_cmd(cmd_str)
        self.output.current.value = f"{time} >> {cmd_str}\n{result}"
        await self.output.current.update_async()


class _Terminal(_flt.UserControl):
    def __init__(self) -> None:
        super().__init__()
        self.is_mounted: _asyncio.Event = _asyncio.Event()

    def build(self):
        global MSG_PERSIST
        self.view: _flt.Column = _flt.Column(
            expand=True,
            scroll=_flt.ScrollMode.ADAPTIVE,
            alignment=_flt.MainAxisAlignment.START,
            auto_scroll=True,
            controls=[_flt.Text(selectable=True, value=_) for _ in MSG_PERSIST.get()],
        )
        return self.view

    def did_mount(self):
        self.is_mounted.set()
        assert LED_BACKEND
        assert self.page

        def add_message(msg: str) -> None:
            global MSG_PERSIST
            if len(MSG_PERSIST.get()) >= 100:
                MSG_PERSIST.pop(0)
                self.view.controls.pop(0)
            MSG_PERSIST.add(msg)
            self.view.controls.append(_flt.Text(selectable=True, value=msg))
            return

        LED_BACKEND.cmd_executor.set_log_handler_callable(add_message)
        # _asyncio.create_task(self.__update_loop())
        self.page.run_task(self.__update_loop)
        return super().did_mount()

    def will_unmount(self):
        assert LED_BACKEND
        self.is_mounted.clear()
        LED_BACKEND.cmd_executor.reset_log_handler_callable()
        return super().will_unmount()

    async def __update_loop(self, update_delay: float = 0.3) -> None:
        while self.is_mounted.is_set():
            await self.view.update_async()
            await _asyncio.sleep(update_delay)


def create_view(page: _flt.Page, route: str) -> _flt.View:

    terminal_content: _Terminal = _Terminal()
    # terminal_content.width = page.width * 0.4

    left_side: _flt.Control = _flt.Column(
        # expand=True,
        height=page.height,
        width=page.width * 0.4,
        controls=[
            _flt.Markdown("# Recently Executed:"),
            _flt.Container(
                expand=True,
                content=terminal_content,
            ),
        ],
        # scroll=_flt.ScrollMode.ADAPTIVE,
        alignment=_flt.MainAxisAlignment.START,
        horizontal_alignment=_flt.CrossAxisAlignment.START,
    )

    right_side: _flt.Control = _flt.Column(
        width=page.width * 0.4,
        # expand=False,
        controls=[_flt.Markdown("# Execute Commands Manually:"), _SubmitCmd()],
    )

    content_pane = _flt.Column(
        expand=True,
        spacing=0,
        alignment=_flt.MainAxisAlignment.CENTER,
        controls=[
            _flt.Row(
                expand=True,
                spacing=0,
                alignment=_flt.MainAxisAlignment.START,
                vertical_alignment=_flt.CrossAxisAlignment.START,
                # scroll=_flt.ScrollMode.ADAPTIVE,
                controls=[left_side, right_side],
            )
        ],
    )

    view: _flt.View = _flt.View(
        route=route,
        controls=[
            _flt.Row(
                expand=True,
                # vertical_alignment=_flt.CrossAxisAlignment.START,
                controls=[content_pane],
            )
        ],
    )

    assert LED_BACKEND

    async def page_resize(e: _flt.ControlEvent) -> None:
        if e.page.route == view.route:
            left_side.height = right_side.height = e.page.height
            left_side.width = e.page.width * 0.4
            right_side.width = e.page.width * 0.4

            await left_side.update_async()
            await right_side.update_async()
            # await view.update_async()
            # await page.update_async()

        return

    __GlobalResizeHandler.add_resize_handler(
        event_id="monitor_page_resizer", handler=page_resize
    )

    return view
