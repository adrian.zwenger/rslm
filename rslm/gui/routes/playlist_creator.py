import xml.etree.ElementTree as ET
from xml.dom import minidom

import flet as _flt

import rslm.gui._resize_handler as __GlobalResizeHandler
import rslm.gui.routes as ROUTES
from rslm.gui.backend import LED_BACKEND
from rslm.gui.components._playlist_loader import PlaylistPicker
from rslm.gui.components._playlist_viewer import Playlist
from rslm.led_command_lib._serialization import serialize_generic_command_program


def create_view(page: _flt.Page, route: str) -> _flt.View:
    pick_files_dialog = PlaylistPicker()
    playlist_viewer = Playlist()

    async def __on_click_play(e: _flt.ControlEvent):
        if LED_BACKEND:
            playlist = playlist_viewer.get_playlist()
            if not playlist:
                e.page.snack_bar = _flt.SnackBar(
                    content=_flt.Text(selectable=True, value="The playlist is empty."),
                    bgcolor="red",
                )
                e.page.snack_bar.open = True
                e.page.update()
                return

            await LED_BACKEND.set_playlist(
                session_id=e.page.session_id, playlist=playlist  # type: ignore
            )

            # await ROUTES.change_route(new_route=ROUTES.APP_ROOT_ROUTE, page=e.page)
            async def _():
                # await ROUTES.change_route(new_route=ROUTES.APP_ROOT_ROUTE, page=e.page)
                await e.page.go_async(ROUTES.APP_ROOT_ROUTE)

            e.page.run_task(_)

            # moved into change_route to guarantee same async loop
            # top_view: _flt.View = e.page.views[-1]
            # if hasattr(top_view, "async_rebuild_player"):
            #     e.page.run_task(getattr(top_view, "async_rebuild_player"))

    async def __clear_playlist(e: _flt.ControlEvent):
        await playlist_viewer.reset()

    async def __on_click_save(e: _flt.ControlEvent):
        playlist = playlist_viewer.get_playlist()
        val: str = ""
        if playlist:
            _ = serialize_generic_command_program(playlist)
            xmlstr = minidom.parseString(ET.tostring(_.getroot())).toprettyxml(
                indent="   "
            )
            val = xmlstr

        await e.page.set_clipboard_async(val)
        e.page.snack_bar = _flt.SnackBar(
            content=_flt.Text(selectable=True, value="Playlist copied to clipboard."),
            bgcolor="green",
        )
        e.page.snack_bar.open = True
        await e.page.update_async()

    playlist_container = _flt.Container(
        content=playlist_viewer,
        width=page.width * 0.9,
        height=page.height * 0.9,
    )

    button_container = _flt.Container(
        content=_flt.Row(
            scroll=_flt.ScrollMode.ADAPTIVE,
            # wrap=True,
            controls=[
                pick_files_dialog,
                _flt.ElevatedButton(
                    text="Add Command to Playlist.",
                    icon=_flt.icons.ADD,
                    on_click=playlist_viewer._open_add_track_dialog,
                ),
                _flt.ElevatedButton(
                    text="Copy Playlist to Clipboard.",
                    icon=_flt.icons.SAVE,
                    on_click=__on_click_save,
                ),
                _flt.ElevatedButton(
                    text="Start the Playlist.",
                    icon=_flt.icons.PLAY_ARROW,
                    on_click=__on_click_play,
                ),
                _flt.ElevatedButton(
                    text="Clear the Playlist.",
                    icon=_flt.icons.DELETE,
                    on_click=__clear_playlist,
                ),
            ],
        ),
        width=page.width * 0.9,
        height=page.height * 0.1,
    )

    async def __playlist_chosen(e: _flt.ControlEvent):
        if pick_files_dialog.playlist:
            # await playlist_viewer.reset(e)
            await playlist_viewer.add_cmds(pick_files_dialog.playlist)
            await playlist_viewer.track_column.update_async()

    pick_files_dialog.on_change = __playlist_chosen

    content = _flt.Row(
        expand=True,
        alignment=_flt.MainAxisAlignment.START,
        controls=[
            _flt.Row(
                expand=True,
                alignment=_flt.MainAxisAlignment.CENTER,
                controls=[
                    _flt.Column(
                        # expand=True,
                        controls=[button_container, playlist_container],
                        alignment=_flt.MainAxisAlignment.START,
                    ),
                ],
            )
        ],
    )
    view: _flt.View = _flt.View(
        route=route,
        controls=[content],
    )

    async def page_resize(e: _flt.ControlEvent) -> None:
        if e.page.route == view.route:
            playlist_container.width = e.page.width * 0.9
            playlist_container.height = e.page.height * 0.9

            button_container.width = e.page.width * 0.9
            button_container.height = e.page.height * 0.1
            await playlist_container.update_async()
            await button_container.update_async()
        return

    __GlobalResizeHandler.add_resize_handler(
        event_id="playground_page_resizer", handler=page_resize
    )

    return view
